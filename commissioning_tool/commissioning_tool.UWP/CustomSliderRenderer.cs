﻿using commissioning_tool.CustomControls;
using commissioning_tool.UWP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;
using GradientStop = Windows.UI.Xaml.Media.GradientStop;
using GradientStopCollection = Windows.UI.Xaml.Media.GradientStopCollection;
using LinearGradientBrush = Windows.UI.Xaml.Media.LinearGradientBrush;

[assembly: ExportRenderer(typeof(CustomSlider), typeof(CustomSliderRenderer))]
namespace commissioning_tool.UWP
{

    public class CustomSliderRenderer : SliderRenderer
    {

        public CustomSliderRenderer()
        {
            //MaxColor = ExportRendererAttribute.
            
            //Control.Background = new LinearGradientBrush() { GradientStops = new GradientStopCollection() { new GradientStop() { Color = Windows.UI.Colors.Red, Offset = 0.0 }, new GradientStop() { Color = Windows.UI.Colors.Blue, Offset = 1 } } };
        }

        public Color MinColor { get; set; } = Color.Red;
        public Color MaxColor { get; set; } = Color.Blue;
        public Color CenterColor { get; private set; }
        public bool UseCenterColor { get; private set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Slider> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null) return;

            var customSlider = e.NewElement as CustomSlider;
            MinColor =  customSlider.MinColor;
            MaxColor = customSlider.MaxColor;
            CenterColor = customSlider.CenterColor;
            UseCenterColor = customSlider.UseCenterColor;
            if (Control != null)
            {
                
                var cntrl = Control as FormsSlider;


                if (UseCenterColor == false)
                {
                    cntrl.Background = new LinearGradientBrush() { GradientStops = new GradientStopCollection() { new GradientStop() { Color = MinColor.ToWindowsColor(), Offset = 0.0 }, new GradientStop() { Color = MaxColor.ToWindowsColor(), Offset = 1 } } };
                }
                else
                {
                    cntrl.Background = new LinearGradientBrush() { GradientStops = new GradientStopCollection() { new GradientStop() { Color = MinColor.ToWindowsColor(), Offset = 0.0 }, new GradientStop() { Color = CenterColor.ToWindowsColor(), Offset = .5 }, new GradientStop() { Color = MaxColor.ToWindowsColor(), Offset = 1 } } };

                }
                Windows.UI.Xaml.Style sliderStyle = (Windows.UI.Xaml.Style)Windows.UI.Xaml.Application.Current.Resources["styledSlider"];
                Control.Style = sliderStyle;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var mySlider = sender as CustomSlider;
            MinColor = mySlider.MinColor;
            MaxColor = mySlider.MaxColor;
            CenterColor = mySlider.CenterColor;
            UseCenterColor = mySlider.UseCenterColor;
            if (Control != null)
            {

                var cntrl = Control as FormsSlider;


                if (UseCenterColor == false)
                {
                    cntrl.Background = new LinearGradientBrush() { GradientStops = new GradientStopCollection() { new GradientStop() { Color = MinColor.ToWindowsColor(), Offset = 0.0 }, new GradientStop() { Color = MaxColor.ToWindowsColor(), Offset = 1 } } };
                }
                else
                {
                    cntrl.Background = new LinearGradientBrush() { GradientStops = new GradientStopCollection() { new GradientStop() { Color = MinColor.ToWindowsColor(), Offset = 0.0 }, new GradientStop() { Color = CenterColor.ToWindowsColor(), Offset = .5 }, new GradientStop() { Color = MaxColor.ToWindowsColor(), Offset = 1 } } };

                }
                Windows.UI.Xaml.Style sliderStyle = (Windows.UI.Xaml.Style)Windows.UI.Xaml.Application.Current.Resources["styledSlider"];
                Control.Style = sliderStyle;
            }

        }
    }
}
