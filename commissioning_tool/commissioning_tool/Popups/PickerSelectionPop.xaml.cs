﻿using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PickerSelectionPop : PopupPage
    {
        public PickerSelectionPop()
        {
            InitializeComponent();
        }
    }
}