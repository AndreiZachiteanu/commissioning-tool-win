﻿using commissioning_tool.Enums;
using commissioning_tool.Models;
using commissioning_tool.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupPageActionPicker : PopupPage
    {
        #region Fields
        private PopupPageActionPickerViewModel vm;

        #endregion

        #region Constructor
        public PopupPageActionPicker(ScheduleActions actionList, Actions action)
        {
            InitializeComponent();
            vm = BindingContext as PopupPageActionPickerViewModel;
            vm.OriginalACtion = action;
            vm.actionList = actionList;
            vm.Title = action.name;
            switch (action.type)
            {
                case LogicalTypeEnum.TASK:
                    vm.Type = "TASK";
                    break;
                case LogicalTypeEnum.SCENE:
                    vm.Type = "SCENE";
                    break;
            }
        }
        #endregion

        #region Overrides

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (vm.currentAction != null)
            {
                vm.currentAction.selected = false;
            }
        }

        protected override bool OnBackgroundClicked()
        {
            return false;
        }
        #endregion
    }
}