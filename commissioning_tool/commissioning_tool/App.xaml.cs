﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using commissioning_tool.Views;
using System.Globalization;
using System.Threading;
using commissioning_tool.Assets;
using Xamarin.CommunityToolkit.Helpers;
using commissioning_tool.Assets.Dictionary;
using System.Reflection;
using commissioning_tool.Assets.Languages;
using System.Linq;
using commissioning_tool.ViewModels;

namespace commissioning_tool
{

    public partial class App : Application
    {

        //public static Page MainPage = new Page();
        public App()
        {
            //var resource = AppResources.ResourceManager;
            //var x = resource.GetResourceSet();

            
            InitializeComponent();
            // AppResources.Culture = CultureInfo.GetCultureInfo("en");
            //Set default language
            //LanguageList.CurrentLanguage = 
            //var lang = Resources.Configuration.Locale;

            var lang = LanguageList.Languages.Where(w => w.LanCode.Equals("en")).FirstOrDefault();
            lang.SelectedLanguage = true;
            LanguageList.CurrentLanguage = lang;
            MainPage = new NavigationPage(new MainPage());


        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
