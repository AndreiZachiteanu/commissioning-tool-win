﻿using commissioning_tool.Converters;
using commissioning_tool.Models;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.PhysicalDevices
{
    public class Sensor : Device
    {
        #region Fields
        public string _id;
        public Dictionary<string, int[]> _triggerEvents = new Dictionary<string, int[]>();

        #endregion

        #region Properties        
        public iM_NPolicyParameter SensorRate { get; set; } = new iM_NPolicyParameter { enabled = true };

        [JsonProperty("id")]
        public  string id{ get { return _id; }
    set { _id = value; ID = value; } }
        public override string ID { get; set; }
            
            
            /*id 
            */
        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("drawingReference")]
        public string drawingReference { get; set; }

        [JsonProperty("connected")]
        public string connected { get; set; }

        [JsonProperty("commissioned")]
        public string commissioned { get; set; }

        [JsonProperty("identified")]
        public bool identified { get; set; }

        [JsonProperty("discovered")]
        public string discovered { get; set; }

        [JsonProperty("lastSeen")]
        public string lastSeen { get; set; }

        [JsonProperty("protocol")]
        public string protocol { get; set; }

        [JsonProperty("sensorType")]
        public string sensorType { get; set; }

        [JsonProperty("triggerEvents")]
        [JsonConverter(typeof(TriggerJsonConverter))]
        public List<TriggerJson> triggerEvents {get;set;}

        [JsonProperty("triggerStates")]
        public object triggerStates { get; set; }


        #endregion


        public Sensor()
        {
            devType = Enums.DeviceType.sensor;
            
        }     

    }

}
