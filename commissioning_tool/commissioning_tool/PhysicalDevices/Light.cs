﻿using commissioning_tool.Connections;
using commissioning_tool.Converters;
using commissioning_tool.Enums;
using commissioning_tool.Models;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.PhysicalDevices
{
    public class Light : Device
    {
        #region Fields

        private int __RValue = 0;
        private int __GValue = 0;
        private int __BValue = 0;
        private int __xValue = 0;
        private int __SCIntensity;
        public int __CCTTemperature = 0;
        public int __CCTIntensity = 0;
        private int __RGBIntensity = 255;
        private double __intensity = 0;
        public override string ID {get;set;}
        #endregion
        public double _intensity 
        {
            get 
            { 
                return __intensity; 
            }
            set 
            {
                __intensity = value;
                OnPropertyChanged("intensity");
            } 
        }
        private LightType _type = LightType.PRIMITIVE;
        public event EventHandler TypeChanged;
        public int _RValue
        { 
            get 
            { 
                return __RValue;
            } 
            set {
                __RValue = value;
                OnPropertyChanged("RValue");
            } 
        }
        public int _GValue
        {
            get
            {
                return __GValue;
            }
            set
            {
                __GValue = value;
                OnPropertyChanged("GValue");
            }
        }
        public int _BValue
        {
            get
            {
                return __BValue;
            }
            set
            {
                __BValue = value;
                OnPropertyChanged("BValue");
            }
        }
        public int _xValue
        {
            get
            {
                return __xValue;
            }
            set
            {
                __xValue = value;
                OnPropertyChanged("xValue");
            }
        }
        
        public int _CCTIntensity
        {
            get
            {
                return __CCTIntensity;
            }
            set
            {
                __CCTIntensity = value;
                //OnPropertyChanged("CCTIntensity");
               // OnPropertyChanged("CCTTemperature");
                Debug.WriteLine("intenstiy changed");
            }
        }
        public int _CCTTemperature
        {
            get
            {
                return __CCTTemperature;
            }
            set
            {
                Debug.WriteLine("temp changed");
                __CCTTemperature = value;
               // OnPropertyChanged("CCTTemperature");
               // OnPropertyChanged("CCTIntensity");
            }
        }
        
        public int _RGBIntensity
        {
            get
            {
                return __RGBIntensity;
            }
            set
            {
                __RGBIntensity = value;
               // OnPropertyChanged("RGBIntensity");
            }
        }
        

        #region Properties
        public LightType type { get { return _type; } set { _type = value; TypeCh(EventArgs.Empty); } } 
        public int subUni { get; set; }
        public bool is16Bit { get; set; }
        public int net { get; set; }
        public string defName { get; set; }
        public string custName { get; set; }
        public List<int> channels { get; set; } = new List<int>();
        public int intensity { 
            get { return (int)(__intensity); } 
            set 
            { 
                __intensity = value;
                ChangeLightLevel();
            } 
        }

        public int IntensityLabel
        {
            get
            {
                return (int)((double)intensity / 2.55);
            }
            set
            {
                _intensity = value;
            }
        }
        public int current { get; set; }
        public int voltage { get; set; }
        private int _colour;
        public int colour { get { return _colour; } set { _colour = value; ChangeLightLevel(); } }


        [JsonProperty("address")]
        [JsonConverter(typeof(LightDMXConverter))]
        public LightDMXAddress dmxAddress { get; set; } = new LightDMXAddress();
        public string controlledBy { get; set; }
        public int universe { get; internal set; }
        public int warmTemp { get; set; }
        public int coolTemp { get; set; }
        public string xChannelName { get; set; }

        public int CCTIntensity 
        { 
            get 
            {
                return _CCTIntensity; 
            } 
            set 
            {

                Debug.WriteLine("Slider changed intensity to " + value);
                Debug.WriteLine("Temperature is " + __CCTTemperature);
                __CCTIntensity = value; 
                ChangeLightLevel(); 
            } 
        }
        public int CCTTemperature 
        { 
            get 
            { 
                return _CCTTemperature;
            } 
            set 
            {
                Debug.WriteLine("Slider changed temperature to " + value);
                Debug.WriteLine("Intensity is " + __CCTIntensity);
                __CCTTemperature = value; 
                ChangeLightLevel(); 
            } 
        }
        public int RGBIntensity 
        { 
            get 
            {

                return (int)(Color.FromRgb(RValue, GValue, BValue).Luminosity * 100);
            } 
            set 
            { 
                
                var color = Color.FromRgb(__RValue, __GValue, __BValue).WithLuminosity(value * 0.01);
                __RValue = (int)(color.R * 255);
                __GValue = (int)(color.G * 255);
                __BValue = (int)(color.B * 255);
                ChangeLightLevel(); 
            } 
        } 
        public int RValue 
        { get 
            { 
                return __RValue; 
            } 
            set 
            { 
                __RValue = value; 
                ChangeLightLevel(); 
            } 
        }
        public int GValue 
        { 
            get 
            { 
                return __GValue; 
            } 
            set 
            { 
                __GValue = value; 
                ChangeLightLevel(); 
            } 
        }
        public int BValue 
        { 
            get 
            { 
                return __BValue; 
            } 
            set 
            { 
                __BValue = value; 
                ChangeLightLevel(); 
            } 
        }
        public int xValue 
        { 
            get 
            { 
                return __xValue; 
            } 
            set 
            { 
                __xValue = value; 
                ChangeLightLevel(); 
            } 
        }

        public Color RGB
        {
            get
            {


                var color = Color.FromRgb(RValue, GValue, BValue);
                
                return color;
                
                // var returnedColor = color.WithLuminosity(color.Luminosity * (((double)RGBIntensity) / 255));

                //return returnedColor ;
            }
            set
            {
                
                RGB = value;
                OnPropertyChanged();
            }


        }

        public Color xChannelColour
        {
            get
            {
                var returnedColor = Color.Black.WithLuminosity(((double)xValue / 255));                
                return returnedColor;
            }

        }
        public Color RGBX { get; set; }
        public int RGBXIntensity { get; set; }
        #endregion


        #region Methods

        protected virtual void TypeCh(EventArgs e)
        {
            EventHandler handler = TypeChanged;
            handler?.Invoke(this, e);
        }
        public void ChangeLightLevel()
        {
            iMune_SSHComs.GetInstance().ChangeLightLevel(this);
        }

        public void PublicPropertyChanged(string property)
        {
            OnPropertyChanged(property);
        }

        #endregion
    }


}
