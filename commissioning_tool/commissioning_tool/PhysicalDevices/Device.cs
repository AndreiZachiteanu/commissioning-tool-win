﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace commissioning_tool.PhysicalDevices
{
    abstract public class Device: INotifyPropertyChanged
    {
        #region Properties
        public bool selected { get; set; }
        public bool showProps { get; set; }
        public DeviceType devType { get; set; }
        abstract public string ID { get; set; }
        public bool showDevPolProps { get; internal set; }
        #endregion



        #region Methods
        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
