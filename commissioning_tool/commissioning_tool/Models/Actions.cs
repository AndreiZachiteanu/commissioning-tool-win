﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace commissioning_tool.Models
{
    public class Actions : INotifyPropertyChanged
    {
        
        #region Fields
        public string _name = "";
        private Actions _instance; 
        public string _id = "";
        #endregion
                public string name { 
            get { 

                return _name; } 
            set { 
                _name = value;
                OnPropertyChanged();
            } 
        }
        public bool selected { get; set; }
        public string id {
            get {
                return _id;
            } set {
                _id = value;
                OnPropertyChanged();
            } 
        }
        public LogicalTypeEnum type { get; set; }

        #region Properties

        public Actions instance { get{ return _instance == null ? _instance = new Actions() : _instance; } set => _instance = value; }


        #endregion

        public Actions()
        {
            instance = this;
        }


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;
            /*
            if (propertyName.Equals("instance"))
            {
                
                //if (instance == null) instance = new Actions();
                name = instance.name;
                id = instance.id;
                type = instance.type;
                
                OnPropertyChanged("name");
                OnPropertyChanged("id");
                OnPropertyChanged("type");
                
            }
            */
            if (propertyName.Equals("id"))
            {

                // Debug.WriteLine(type);
                var item = ScheduleObject.actionList.mergedList.Where(w => w.id == id).FirstOrDefault();
                if (item != null)
                {
                    name = item.name;
                }
            }
            



            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
