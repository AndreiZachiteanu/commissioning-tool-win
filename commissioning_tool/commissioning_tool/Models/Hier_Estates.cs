﻿using commissioning_tool.Assets;
using commissioning_tool.Converters;
using commissioning_tool.Enums;
using commissioning_tool.PhysicalDevices;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class Hier_Estates : Hierarchy
    {
        #region Fields
        private HierarchyItemList itemList;
        #endregion
        #region Properties
        public override HierarchyMenuItem HierType { get; } = HierarchyMenuItem.ESTATE;
        public override HierarchyMenuItem HierChildType { get; } = HierarchyMenuItem.CAMPUS;
        public override string Icon { get; } = FontAwesomeIcons.Flag;
        [JsonProperty("id")]
        public override string Id { get; set; }

        [JsonProperty("name")]
        public override string Name { get; set; }

        [JsonProperty("campuses")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public override HierarchyItemList ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                itemList.Parent = this;
            }
        }




        #endregion
    }

    public class Floor : Hierarchy
    {
        #region Fields
        private HierarchyItemList itemList;
        #endregion
        public override HierarchyMenuItem HierType { get; } = HierarchyMenuItem.FLOOR;
        public override HierarchyMenuItem HierChildType { get; } = HierarchyMenuItem.ZONE;
        public override string Icon { get; } = FontAwesomeIcons.Columns;
        [JsonProperty("id")]
        public override string Id { get; set; }

        [JsonProperty("name")]
        public override string Name { get; set; }

        [JsonProperty("zones")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public override HierarchyItemList ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                itemList.Parent = this;
            }
        }

        [JsonProperty("devices")]
        public object Devices { get; set; }

        [JsonProperty("gateways")]
        public object Gateways { get; set; }

        [JsonProperty("availableOutputs")]
        public object AvailableOutputs { get; set; }
    }
    public class Campus : Hierarchy
    {
        #region Fields
        private HierarchyItemList itemList;
        #endregion
        public override HierarchyMenuItem HierChildType { get; } = HierarchyMenuItem.BUILDING;
        public override string Icon { get; } = FontAwesomeIcons.City;
        public override HierarchyMenuItem HierType { get; } = HierarchyMenuItem.CAMPUS;
        [JsonProperty("id")]
        public override string Id { get; set; }

        [JsonProperty("name")]
        public override string Name { get; set; }

        [JsonProperty("buildings")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public override HierarchyItemList ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                itemList.Parent = this;
            }
        }

    }

    public class Building : Hierarchy
    {
        #region Fields
        private HierarchyItemList itemList;
        #endregion
        public override HierarchyMenuItem HierType { get; } = HierarchyMenuItem.BUILDING;
        public override HierarchyMenuItem HierChildType { get; } = HierarchyMenuItem.FLOOR;
        public override string Icon { get; } = FontAwesomeIcons.Building;
        [JsonProperty("id")]
        public override string Id { get; set; }

        [JsonProperty("name")]
        public override string Name { get; set; }

        [JsonProperty("floors")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public override HierarchyItemList ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                itemList.Parent = this;
            }
        }
    }
    public class Zone : Hierarchy
    {
        #region Fields
        private HierarchyItemList itemList;
        #endregion
        public override HierarchyMenuItem HierChildType { get; } = HierarchyMenuItem.DEVICES;
        public override HierarchyMenuItem HierType { get; } = HierarchyMenuItem.ZONE;
        public override string Icon { get; } = FontAwesomeIcons.VectorSquare;
        [JsonProperty("id")]
        public override string Id { get; set; }

        [JsonProperty("name")]
        public override string Name { get; set; }




        public ObservableCollection<SchedulesM> Schedules { get; set; } = new ObservableCollection<SchedulesM>();
        [JsonProperty("lights")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public ObservableCollection<Light> Lights { get; set; } = new ObservableCollection<Light>();

        [JsonProperty("lightGroups")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public ObservableCollection<iM_LightGroup> LightGroups { get; set; } = new ObservableCollection<iM_LightGroup>();

        [JsonProperty("policies")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public ObservableCollection<iM_newPolicyM> Policies { get; set; } = new ObservableCollection<iM_newPolicyM>();
        [JsonProperty("scenes")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public ObservableCollection<iM_SceneModel> Scenes { get; set; } = new ObservableCollection<iM_SceneModel>();
        [JsonProperty("tasks")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public ObservableCollection<iM_TaskModel> Tasks { get; set; } = new ObservableCollection<iM_TaskModel>();
        [JsonProperty("taskGroups")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public ObservableCollection<iM_TaskGroupModel> TaskGroups { get; set; } = new ObservableCollection<iM_TaskGroupModel>();

        [JsonProperty("sensors")]
        [JsonConverter(typeof(Hier_ItemListConverter))]
        public ObservableCollection<Sensor> Sensors { get; set; } = new ObservableCollection<Sensor>();
        public override HierarchyItemList ItemList { get; set; }
    }
}
