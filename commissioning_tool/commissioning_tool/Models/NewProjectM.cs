﻿using commissioning_tool.Objects;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Models
{
    public class NewProjectM : BaseViewModel
    {
        #region Properties
        public Project project { get; set; } = new Project();
        public string selectedTab { get; set; } = "contacts";
        public ObservableCollection<DeviceM> devices { get; set; } = new ObservableCollection<DeviceM>();

        #endregion

        #region Fields
        public INavigation Navigation;
        #endregion
    }
}
