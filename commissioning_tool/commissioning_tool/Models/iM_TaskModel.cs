﻿using commissioning_tool.Converters;
using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_TaskModel : iM_LogicalModel
    {

        private LogicalTypeEnum _type = LogicalTypeEnum.TASK;
        public override LogicalTypeEnum type { get => _type; set { _type = value; TypeHasChanged(); } }
        public override event EventHandler TypeChanged;

        protected override void TypeHasChanged()
        {
            EventHandler handler = TypeChanged;
            EventMessage eventMessage = new EventMessage();
            eventMessage.Message = type.ToString();
            handler?.Invoke(this, eventMessage);
        }

        public iM_TaskModel()
        {
            Phases.CollectionChanged += Phases_CollectionChanged;

        }

        private void Phases_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var count = 0;
            foreach (var item in (ObservableCollection<TaskPhase>)sender)
            {
                item.Name = "Phase " + count++;
            }
        }

        [JsonProperty("date")]
        public object Date { get; set; }
        [JsonProperty("id")]
        public override string Id { get; set; }


        [JsonProperty("lightsListIDs")]
        public List<string> LightsListIDs { get; set; }

        [JsonProperty("PhaseList")]
        [JsonConverter(typeof(TaskPhaseListConverterJson))]
        public ObservableCollection<TaskPhase> Phases { get; set; } = new ObservableCollection<TaskPhase>();

        [JsonProperty("cycled")]
        [JsonConverter(typeof(BoolPropertyConverter))]
        public bool Cycled { get; set; }

        [JsonProperty("pingpong")]
        [JsonConverter(typeof(BoolPropertyConverter))]
        public bool Pingpong { get; set; }

        [JsonProperty("runningNow")]
        [JsonConverter(typeof(BoolPropertyConverter))]
        public bool RunningNow { get; set; }

        [JsonProperty("sensorDriven")]
        [JsonConverter(typeof(BoolPropertyConverter))]
        public bool SensorDriven { get; set; }

        [JsonProperty("startTriggers")]
        public List<object> StartTriggers { get; set; }

        [JsonProperty("stopTriggers")]
        public List<object> StopTriggers { get; set; }

        [JsonProperty("privledgedUserIDs")]
        public List<string> PrivledgedUserIDs { get; set; }

        [JsonProperty("cycleDuration")]
        public int CycleDuration { get; set; }

        [JsonProperty("cycleCounter")]
        public int CycleCounter { get; set; }

        [JsonProperty("taskExecStarted")]
        public object TaskExecStarted { get; set; }

        [JsonProperty("triggers")]
        public List<object> Triggers { get; set; }

        [JsonProperty("manuallyExcluded")]
        public object ManuallyExcluded { get; set; }

        [JsonProperty("exitPhase")]
        public object ExitPhase { get; set; }



        [JsonProperty("activeLights")]
        public List<string> ActiveLights { get; set; }

        [JsonProperty("statusShort")]
        public string StatusShort { get; set; }

        [JsonProperty("restricted")]
        public bool Restricted { get; set; }
    }
    // Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Duration : BaseViewModel
    {
        [JsonProperty("time")]
        public int Time { get; set; }

        [JsonProperty("hours")]
        public int Hours { get; set; }

        [JsonProperty("minutes")]
        public int Minutes { get; set; }

        [JsonProperty("seconds")]
        public int Seconds { get; set; }
    }

    public class TaskPhase : BaseViewModel
    {
        public string Name
        {
            get;
            set;
        }
        public string LightType
        {
            get;
            set;
        } = "PRIMITIVE";
        public LightValues LightValues { get; set; } = new LightValues();

        [JsonProperty("duration")]
        public Duration Duration { get; set; } = new Duration();

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("transitionStyle")]
        public string TransitionStyle { get; set; } = "LIN";

        [JsonProperty("phaseExecStarted")]
        public long? PhaseExecStarted { get; set; }
    }





}
