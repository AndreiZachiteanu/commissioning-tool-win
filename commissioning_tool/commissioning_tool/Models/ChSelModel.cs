﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class ChSelModel : BaseViewModel
    {
        #region Properties
        public int channelNo { get; set; }
        public bool prim { get; set; }
        public bool warm { get; set; }
        public bool cool { get; set; }
        public bool red { get; internal set; }
        public bool blue { get; internal set; }
        public bool green { get; internal set; }
        public bool xCh { get; internal set; }
        #endregion

    }
}
