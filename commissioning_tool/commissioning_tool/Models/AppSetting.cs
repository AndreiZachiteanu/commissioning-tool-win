﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class AppSetting : BaseViewModel
    {
        #region Properties
            public string Setting { get; set; }
            public string SettingText { get; set; }
        #endregion
    }
}
