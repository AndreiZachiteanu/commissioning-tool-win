﻿using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_ActivationDayM : BaseViewModel
    {
        #region Parameters
        public bool use { get; set; }
        public ObservableCollection<iM_StartEndTimesM> times { get; set; } = new ObservableCollection<iM_StartEndTimesM>();
        public DayEnum DayName { get; internal set; }
        public bool selected { get; internal set; }
        #endregion
    }
}
