﻿using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace commissioning_tool.Models
{
    public class iM_SequenceM : BaseViewModel
    {
        #region Properties
        public SequenceType sequenceType { get; set; }

        public LightValues lightValues { get; set; }
        #endregion
    }

    public class LightValues : BaseViewModel
    {
        #region Fields

        #endregion


        #region Properties

        public iM_NPolicyParameter transition { get; set; } = new iM_NPolicyParameter { text="LIN" };
        public iM_NPolicyParameter duration { get; set; } = new iM_NPolicyParameter();
        public iM_NPolicyParameter delay { get; set; } = new iM_NPolicyParameter();

        public bool enabled { get; set; }
        public int SCIntensity { get; set; }
        public int SCIntensityLabel { get { return (int)((double)SCIntensity / 2.55); } }

        public int CCTIntensity { get; set; }

        public int CCTIntensityLabel { get { return (int)((double)CCTIntensity / 2.55); } }
        public int CCTTemperature { get; set; }
        public int RGBIntensityX { get; set; } = 255;
        public int RValue { get; set; }
        public int GValue { get; set; }
        public int BValue { get; set; }

        public int RValueX { get; set; }
        public int GValueX { get; set; }
        public int BValueX { get; set; }
        public int xValue { get; set; }

        public Color RGB { get {
                return Color.FromRgb(RValue, GValue, BValue); 
                //var color = Color.FromRgb(RValue, GValue, BValue);
                //return color.WithLuminosity(color.Luminosity * (((double)RGBIntensity) /255));
            } }

        public Color xChannelColour
        {
            get
            {
                return Color.Black.WithLuminosity(((double)xValue / 255));
            }

        }
        public int RGBIntensity { get; set; } = 255;
        public Color RGBX { get {

                return Color.FromRgb(RValueX, GValueX, BValueX);
                //return color.WithLuminosity(color.Luminosity * (((double)RGBIntensityX) / 255));
            } }
        public int RGBXIntensity { get; set; }
        #endregion
    }
}
