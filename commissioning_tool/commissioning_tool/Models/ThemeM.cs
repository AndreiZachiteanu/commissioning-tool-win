﻿using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Models
{
    public class ThemeM : BaseViewModel
    {
        #region Properties
        public ResourceDictionary Theme { get; set; }
        public ThemeEnum ThemeName { get; internal set; }

        

        #endregion
    }
}
