﻿using System;
using System.Collections.Generic;
using System.Text;
using commissioning_tool.ViewModels;
using Xamarin.Forms;

namespace commissioning_tool.Models
{
    public class LoginM : BaseViewModel
    {
        #region Properties
        public string username { get; set; }
        public string password { get; set; }
        #endregion
        #region Fields
        public INavigation Navigation;
        #endregion
    }
}
