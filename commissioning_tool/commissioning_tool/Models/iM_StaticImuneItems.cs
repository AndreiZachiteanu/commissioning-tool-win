﻿using commissioning_tool.Connections;
using commissioning_tool.PhysicalDevices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public static class iM_StaticImuneItems
    {
        public static ImuneItems Instance { get; set; } = new ImuneItems();
    }


    public class ImuneItems
    {
        #region Fields
        private iMune_SSHComs iMComs = iMune_SSHComs.GetInstance();
        #endregion

        #region Properties
        public ObservableCollection<Device> LightList { get; set; }
        public ObservableCollection<Device> SensorList { get; set; }
        public ObservableCollection<iM_LightGroup> LightGroups { get;set;}
        public ObservableCollection<iM_TaskModel> TaskList { get; set; }
        public ObservableCollection<iM_TaskGroupModel> TaskGroups { get; set; }
        public ObservableCollection<iM_SceneModel> Scenes { get; set; }
        public ObservableCollection<iM_newPolicyM> Policies { get; set; }
        public ObservableCollection<SchedulesM> ScheduleList { get; set; }
        public bool iMuneCommissioningMode { get; private set; }

        #endregion

        #region Methods
        public void GetCommissioningMode()
        {
            iMuneCommissioningMode = iMComs.GetCommissioningMode();
        }

        public void GetLightGroups()
        {
            LightGroups = iMComs.GetGroups();
        }

        public void GetLights()
        {
            LightList = (ObservableCollection<Device>)iMComs.GetLights();
        }

        public void GetSensors()
        {
            SensorList = iMComs.GetSensors();
        }

        public void GetPolicies()
        {
            Policies = iMComs.GetPolicies();
        }

        public void GetTasks()
        {
            TaskList = iMComs.ListTasks();
        }

        public void GetTaskGroups()
        {
            TaskGroups = iMComs.ListTaskGroups();
        }

        public void GetScenes()
        {
            Scenes = iMComs.ListScenes();
        }
        
        public void GetSchedules()
        {
            ScheduleList = iMComs.GetSchedules();
        }
        #endregion
    }
}
