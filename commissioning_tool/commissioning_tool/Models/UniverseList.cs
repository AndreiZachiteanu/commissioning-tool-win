﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class UniverseList : BaseViewModel
    {
        #region Properties
        public int universe { get; set; }
        public bool selected { get; set; }
        #endregion
    }
}
