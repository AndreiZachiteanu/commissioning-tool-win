﻿using commissioning_tool.Objects;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Models
{
    public class iM_LoginM : BaseViewModel
    {
        #region Properties
        public ObservableCollection<iMuneM> iMune_List { get; set; } = new ObservableCollection<iMuneM>();
        public iMuneM selectedImune { get; set; } = new iMuneM();
        public string username { get; set; }
        public string keyName { get; set; }
        public string key { get; set; }
        public bool ipSelected { get; set; } = true;
        public bool usernameSet { get; set; } = true;
        public bool keyUploaded { get; set; } = true;
        #endregion

        #region Fields
        public INavigation Navigation;
        #endregion
    }
}
