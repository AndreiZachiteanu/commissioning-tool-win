﻿using commissioning_tool.Converters;
using commissioning_tool.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Linq;
using Xamarin.Forms.Internals;
//using Xamarin.Forms.Internals;

namespace commissioning_tool.Models
{
    public class ScheduleActions :  INotifyPropertyChanged
    {
        #region Fields
        private ObservableCollection<Actions> _mergedList = new ObservableCollection<Actions>();
        private ObservableCollection<Actions> _scenes = new ObservableCollection<Actions>();
        private ObservableCollection<Actions> _tasks = new ObservableCollection<Actions>();
        #endregion

        #region Properties
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(ActionTypeConverter))]        
        public LogicalTypeEnum type { get; set; }

        [JsonProperty("control")]
        public string control { get; set; }

        [JsonProperty("scenes")]
        [JsonConverter(typeof(ActionConverter))]
        public ObservableCollection<Actions> scenes
        { 
            get 
            { 
                return _scenes; 
            } 
            set 
            { 
                _scenes = value;
                scenes.ForEach((i) => { if (!mergedList.Contains(i))mergedList.Add(i); });
                OnPropertyChanged();
            } 
        }
        [JsonProperty("tasks")]
        [JsonConverter(typeof(ActionConverter))]
        public ObservableCollection<Actions> tasks 
        { 
            get 
            { 
                return _tasks; 
            } 
            set 
            { 
                _tasks = value;
                OnPropertyChanged(); 
            } 
        }
        public ObservableCollection<Actions> mergedList {
            get
            {             
                
                    

                return _mergedList;
            }
            set
           {
                _mergedList = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Constructors
        public ScheduleActions()
        {
            /*
            _scenes.CollectionChanged += ListCollection_Changed;
            _tasks.CollectionChanged += ListCollection_Changed;
            
            scenes.CollectionChanged += ListCollection_Changed;
            tasks.CollectionChanged += ListCollection_Changed;
            */

            _mergedList.CollectionChanged += ListCollection_Changed;
        }

        private void ListCollection_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("mergedList");
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
