﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class Hier_MenuItemModel
    {
        #region Properties
        public string Icon { get; set; }
        public string Name { get; set; }
        public List<object> ListItems { get; set; }
        #endregion
    }
}
