﻿using commissioning_tool.Enums;
using commissioning_tool.PhysicalDevices;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class NewLogicalItem : BaseViewModel
    {
        #region Properties
        public LogicalItemEnum Type { get; set; }
        public iM_TaskModel Task { get; set; } = new iM_TaskModel();
        public Light Light { get; set; } = new Light();
        #endregion

    }
}
