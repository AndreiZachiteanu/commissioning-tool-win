﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_ActivationParamM : ObservableCollection<iM_ActivationDayM>
    {

        public bool enabled { get; set; }
        public iM_ActivationParamM()
        {
            Add(new iM_ActivationDayM
            {
                DayName = Enums.DayEnum.MONDAY
            }) ;
            Add(new iM_ActivationDayM
            {
                DayName = Enums.DayEnum.TUESDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = Enums.DayEnum.WEDNESDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = Enums.DayEnum.THURSDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = Enums.DayEnum.FRIDAY

            });
            Add(new iM_ActivationDayM
            {
                DayName = Enums.DayEnum.SATURDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = Enums.DayEnum.SUNDAY
            });
        }
    }
}
