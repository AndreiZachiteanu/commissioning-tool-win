﻿using commissioning_tool.Converters;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class SchedulesM : BaseViewModel
    {

        #region Properties
        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("enabled")]
        public bool enabled { get; set; }

        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("actions")]
        [JsonConverter(typeof(ActionListConverter))]
        public ObservableCollection<ScheduleActions> actions { get; set; } = new ObservableCollection<ScheduleActions>();

        [JsonProperty("scheduleTime")]
        public ScheduleTime scheduleTime { get; set; }
        public bool selected { get; internal set; }
        public bool editName { get; set; }

        #endregion
        
    }
}
