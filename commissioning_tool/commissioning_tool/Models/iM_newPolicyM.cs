﻿using commissioning_tool.Converters;
using commissioning_tool.Enums;
using commissioning_tool.PhysicalDevices;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_newPolicyM : BaseViewModel
    {
        #region Fields
        private PolicyType _type;
        public event EventHandler ParameterChanged;
        #endregion

        #region Properties
        public bool showProps { get; set; }
        public bool selected { get; set; }
        public bool trigger { get; set; }
        [JsonProperty("override")]
        public string override_ { get; set; }

        [JsonProperty("id")]
        public string ID
        {
            get;
            set;
        }
        private string _name = "";
        [JsonProperty("name")]
        public string name 
        {
            get { return _name; }
            set { 
                _name = value; 
            } 
        }
        [JsonProperty("running")]
        public bool Running { get; set; }

        [JsonProperty("triggered")]
        public bool Triggered { get; set; }

        [JsonProperty("lastRan")]
        public string LastRan { get; set; }

        [JsonProperty("lastTrigger")]
        public string LastTrigger { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }
        [JsonProperty("enabled")]
        public bool Enabled { get; set; } = true;
        [JsonProperty("type")]
        [JsonConverter(typeof(PolTypeJsonConverter))]


        public PolicyType type
        {
            get 
            {
                
                //ParameterUsage(); 
                return _type;  } 
            set 
            {
                
                _type = value;
                ParamCh(EventArgs.Empty);
                //ParameterUsage();
            } }
        [JsonProperty("priority")]
        public int priority { get; set; } = 1;


        [JsonProperty("metadata")]
        public Metadata metadata { get; set; }

        [JsonProperty("on")]
        [JsonConverter(typeof(LightValueJsonConverter))]
        public LightValues OnValues { get; set; } = new LightValues();

        [JsonProperty("off")]
        [JsonConverter(typeof(LightValueJsonConverter))]
        public LightValues OffValues { get; set; } = new LightValues();

        [JsonProperty("restriction")]
        public Object Restriction { get; set; }
        [JsonProperty("exclused")]
        public Object Excluded { get; set; }

        [JsonProperty("sensors")]
        public string[] sensorIDs { get; set; }
        public ObservableCollection<Device> NewDevices { get; set; } = new ObservableCollection<Device>();

        [JsonProperty("lights")]
        public string[] lightIDs { get; set; }



        [JsonProperty("interim")]
        [JsonConverter(typeof(LightValueJsonConverter))]
        public LightValues InterimValues { get; set; } = new LightValues();

        [JsonProperty("offDelay")]
        [JsonConverter(typeof(DelayJsonConverter))]
        public iM_NPolicyParameter Delay { get; set; } = new iM_NPolicyParameter();

        [JsonProperty("multiOn")]
        [JsonConverter(typeof(MultiParamJsonConverter))]
        public iM_NPolicyParameter MultiModeOn { get; set; } = new iM_NPolicyParameter();
        
        [JsonProperty("multiOff")]
        [JsonConverter(typeof(MultiParamJsonConverter))]
        public iM_NPolicyParameter MultiModeOff { get; set; } = new iM_NPolicyParameter();       

        public iM_NPolicyParameter IntensityCapMax { get; set; } = new iM_NPolicyParameter();
        public iM_NPolicyParameter IntensityCapMin { get; set; } = new iM_NPolicyParameter();
        public iM_NPolicyParameter Scene { get; set; } = new iM_NPolicyParameter();
        public iM_ActivationParamM ActivationParam { get; set; } = new iM_ActivationParamM();
        public bool PolicyTypeChangeable { get; set; } = true;


        #endregion



        #region Method
        public void ParameterUsage()
        {
            OnValues.enabled = false;
            OffValues.enabled = false;
            Delay.enabled = false;
            ActivationParam.enabled = true;
            //OffDelay.enabled = false;
            InterimValues.enabled = false;
            MultiModeOn.enabled = false;
            MultiModeOff.enabled = false;
            Scene.enabled = false;
            IntensityCapMax.enabled = false;
            IntensityCapMin.enabled = false;
            switch (_type)
            {
                case PolicyType.TOGGLE:
                case PolicyType.SWITCHED:
                    OnValues.enabled = true;                    
                    OffValues.enabled = true;
                    break;
                case PolicyType.TIMER:
                    InterimValues.enabled = true;
                    Delay.enabled = true;
                    Delay.name = "Off";
                    //OffDelay.enabled = true;
                    OnValues.enabled = true;
                    OffValues.enabled = true;
                    break;
                case PolicyType.PRESENCE:
                case PolicyType.ABSENCE:
                    Delay.name = "Off";
                    Delay.enabled = true;
                    OnValues.enabled = true;
                    OffValues.enabled = true;
                    MultiModeOn.enabled = true;
                    MultiModeOff.enabled = true;
                    //OffDelay.enabled = true;
                    InterimValues.enabled = true;
                    break;
                case PolicyType.SCENESELECT:
                    Delay.enabled = true;
                    Delay.name = "Reset";
                    Scene.enabled = true;
                    break;
                case PolicyType.RAISEDIM:
                case PolicyType.STEPDIM:
                    IntensityCapMax.enabled = true;
                    IntensityCapMin.enabled = true;
                    Delay.enabled = true;
                    Delay.name = "Reset";

                    break;
                case PolicyType.EMERGENCY:
                    ActivationParam.enabled = false;
                    break;
            }
        }
        #endregion

        protected virtual void ParamCh(EventArgs e)
        {
            EventHandler handler = ParameterChanged;
            handler?.Invoke(this, e);
        }
    }


}
