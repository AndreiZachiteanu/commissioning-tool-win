﻿using commissioning_tool.Converters;
using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_SceneModel : iM_LogicalModel
    {
        private LogicalTypeEnum _type = LogicalTypeEnum.SCENE;
        public override event EventHandler TypeChanged;

        protected override void TypeHasChanged()
        {
            EventHandler handler = TypeChanged;
            EventMessage eventMessage = new EventMessage();
            eventMessage.Message = type.ToString();
            handler?.Invoke(this, eventMessage);
        }
        [JsonProperty("duration")]
        [JsonConverter(typeof(SceneDurationJsonConverter))]
        public Duration Duration { get; set; } = new Duration();
        public override LogicalTypeEnum type { get => _type; set { _type = value; TypeHasChanged(); } }
        [JsonProperty("phases")]
        [JsonConverter(typeof(ScenePhaseListConverterJson))]
        public ObservableCollection<ScenePhase> Phases { get; set; } = new ObservableCollection<ScenePhase>();
        [JsonProperty("lastRan")]
        public string LastRan { get; set; }
        [JsonProperty("id")]
                public override string Id { get; set; }
    }


    

    public class ScenePhase : BaseViewModel
    {
        public string LightID { get; set; }
        public string TransitionStyle { get; set; } = "LIN";
        public LightValues LightValue { get; set; } = new LightValues();
        public LightType LightType { get; internal set; }
        public string Name { get; internal set; }
    }
}
