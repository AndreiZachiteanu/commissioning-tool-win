﻿using commissioning_tool.Converters;
using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    abstract public class iM_LogicalModel : BaseViewModel
    {
        abstract public event EventHandler TypeChanged;
        abstract protected void TypeHasChanged();
        public bool selected { get; set; }
        public bool showProps { get; set; }
        abstract public LogicalTypeEnum type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        abstract public string Id { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("metadata")]
        public object Metadata { get; set; }
        [JsonProperty("enabled")]
        [JsonConverter(typeof(BoolPropertyConverter))]
        public bool Enabled { get; set; }
        [JsonProperty("priority")]
        public int Priority { get; set; } = 1;
        [JsonProperty("excluded")]
        public object Excluded { get; set; }
        [JsonProperty("sensors")]
        public object Sensors { get; set; }
        [JsonProperty("userAuthorID")]
        public string UserAuthorID { get; set; }
        [JsonProperty("restriction")]
        public object Restriction { get; set; }

        [JsonProperty("running")]
        [JsonConverter(typeof(BoolPropertyConverter))]
        public bool Running { get; set; }

        [JsonProperty("paused")]
        [JsonConverter(typeof(BoolPropertyConverter))]
        public bool Paused { get; set; }

        [JsonProperty("authorisedUsers")]
        public List<string> AuthorisedUsers { get; set; }



    }
}
