﻿using commissioning_tool.Converters;
using commissioning_tool.PhysicalDevices;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_LightGroup : BaseViewModel
    {
        #region Properties
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("defName")]
        public string groupName { get; set; } 
        [JsonProperty("order")]
        public string Order { get; set; }
        [JsonProperty("numLights")]
        public string NumberOfLights { get; set; }
        //This will probably crush non-hierarchy mode. Make sure you link getlights to iM_StaticImuneItems.Instance
        [JsonProperty("lightIds")]
        [JsonConverter(typeof(LightIdJsonToGroup))]
       // public ObservableCollection<Device> groupLights { get; set; } = new ObservableCollection<Device>();
        public ObservableCollection<Light> groupLights { get; set; } = new ObservableCollection<Light>();
        #endregion
    }
}
