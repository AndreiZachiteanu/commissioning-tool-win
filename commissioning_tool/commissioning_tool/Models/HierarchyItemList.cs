﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class HierarchyItemList
    {
        #region Fields
        private Hierarchy parent;
        #endregion

        #region Properties

        public string ContainerName { get; set; }  
        public string Icon { get; set; }
        public ObservableCollection<Hierarchy> ItemList { get; set; } = new ObservableCollection<Hierarchy>();
        public Hierarchy Parent
        {
            get { return parent; }
            set
            {
                parent = value;
                if (ItemList != null)
                {
                    foreach (var item in ItemList)
                    {
                        item.Parent = value;
                    }
                }
            }
        }
        #endregion
    }
}
