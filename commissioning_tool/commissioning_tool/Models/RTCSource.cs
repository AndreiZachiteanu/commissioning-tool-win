﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class RTCSource : BaseViewModel
    {
        #region Properties
        public string source { get; set; }
        #endregion
    }
}
