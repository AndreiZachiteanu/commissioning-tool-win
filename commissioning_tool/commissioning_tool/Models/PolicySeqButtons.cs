﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class PolicySeqButtons : ObservableCollection<iM_PolSeqM>
    {
        public PolicySeqButtons()
        {
            Add(new iM_PolSeqM
            {
                cID = "onSeq",
                Title = "ON"
            });
            Add(new iM_PolSeqM
            {
                cID = "offSeq",
                Title = "OFF",
            });
            Add(new iM_PolSeqM
            {
                cID = "interimSeq",
                Title = "INTERIM"
            });
            Add(new iM_PolSeqM
            {
                cID = "parameter",
                Title = "Parameter"
            });

            Add(new iM_PolSeqM
            {
                cID = "activation",
                Title = "Activation"
            });
        }
    }
}
