﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_CommissioningDetails
    {
        #region Properties
        [JsonProperty("commissioned")]
        public bool Commissioned { get; set; }

        [JsonProperty("commissioning")]
        public bool Commissioning { get; set; }

        [JsonProperty("commissioningModeEntered")]
        public Object CommissioningModeEntered { get; set; }
        #endregion
    }
}
