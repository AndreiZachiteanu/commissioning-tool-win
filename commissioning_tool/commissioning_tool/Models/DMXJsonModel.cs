﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class DMXJsonModel
    {
        [JsonProperty("chCourse")]
        public int[] Addresses { get; set; }
        [JsonProperty("chFine")]
        public int[] FineAddresses { get; set; }
    }
}
