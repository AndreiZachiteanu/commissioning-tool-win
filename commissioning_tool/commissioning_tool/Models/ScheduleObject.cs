﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public static class ScheduleObject
    {
        public static ScheduleActions actionList { get; set; } = new ScheduleActions();
        public static SchedulesM currentSchedule { get; set; } = new SchedulesM();
    }
}
