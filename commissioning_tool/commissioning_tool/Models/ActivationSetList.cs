﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class ActivationSetList : ObservableCollection<iM_ActivationDayM>
    {
        public ActivationSetList()
        {
            Add(new iM_ActivationDayM
            {
                DayName = DayEnum.MONDAY
            }); 
            Add(new iM_ActivationDayM
            {
                DayName = DayEnum.TUESDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = DayEnum.WEDNESDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = DayEnum.THURSDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = DayEnum.FRIDAY

            });
            Add(new iM_ActivationDayM
            {
                DayName = DayEnum.SATURDAY
            });
            Add(new iM_ActivationDayM
            {
                DayName = DayEnum.SUNDAY
            });
        }
    }
}
