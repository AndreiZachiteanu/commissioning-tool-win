﻿using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Models
{
    public class LightDMXAddress : BaseViewModel
    {
        #region Properties
        public ObservableCollection<DMXAddress> primAddresses { get; set; } = new ObservableCollection<DMXAddress>();
        public ObservableCollection<DMXAddress> warmAddresses { get; set; } = new ObservableCollection<DMXAddress>();
        public ObservableCollection<DMXAddress> coolAddresses { get; set; } = new ObservableCollection<DMXAddress>();
        public ObservableCollection<DMXAddress> redAddresses { get; set; } = new ObservableCollection<DMXAddress>();
        public ObservableCollection<DMXAddress> greenAddresses { get; set; } = new ObservableCollection<DMXAddress>();
        public ObservableCollection<DMXAddress> blueAddresses { get; set; } = new ObservableCollection<DMXAddress>();
        public ObservableCollection<DMXAddress> whiteAddresses { get; set; } = new ObservableCollection<DMXAddress>();
        #endregion
    }

    public class DMXAddress : BaseViewModel
    {
        #region Properties
        public string oldValue { get; set; }
        public string newValue { get; set; }
        #endregion
    }
}


