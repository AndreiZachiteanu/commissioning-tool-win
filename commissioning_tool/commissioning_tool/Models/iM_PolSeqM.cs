﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_PolSeqM : BaseViewModel
    {
        #region Parameters
        public string cID { get; set; }
        public bool used { get; set; }
        public bool selected { get; set; }
        public bool isEnabled { get; set; } = true;
        #endregion
    }
}
