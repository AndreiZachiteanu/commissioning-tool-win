﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_StartEndTimesM : BaseViewModel
    {
        #region Parameters
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }

        #endregion
    }
}
