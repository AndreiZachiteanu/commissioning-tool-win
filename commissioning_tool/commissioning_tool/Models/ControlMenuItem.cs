﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class ControlMenuItem
    {
        public string Icon { get; set; }
        public string Name { get; set; }
        public ControlMenuItemType Type { get; set; }
    }
}
