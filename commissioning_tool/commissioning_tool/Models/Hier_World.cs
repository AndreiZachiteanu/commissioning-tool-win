﻿using commissioning_tool.Assets;
using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class Hier_World : Hierarchy
    {
        #region Fields
        private HierarchyItemList itemList = new HierarchyItemList();

        #endregion

        #region Properties
        public override string Icon { get; } = FontAwesomeIcons.GlobeAfrica;
        public override string Name { get; set; } = "World";
        public override string Id { get; set; }
        public override HierarchyMenuItem HierType { get; } = HierarchyMenuItem.WORLD;
        public override HierarchyMenuItem HierChildType { get; } = HierarchyMenuItem.ESTATE;
        public override HierarchyItemList ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                itemList.Parent = this;
            }
        }
        #endregion
    }
}
