﻿using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace commissioning_tool.Models
{
    public class iM_NPolicyParameter : BaseViewModel
    {
        #region Fields
        private string _intValue;
        #endregion


        #region Parameters
        public string text { get; set; }
        public bool enabled { get; set; }
        public string name { get; set; }

        public int value { get; set; }
        public int rate 
        { 
            get
            {
                return (int)(Convert.ToDouble(value) / 2.55);
            } 
        }

        
        public string intValue
        {
            get
            {
                return _intValue;
            }
            set
            {
                _intValue = Regex.Replace(value, @"[^0-9]", String.Empty);
            }
        }

        public string MultiModeTimeout { get; set; }

        [JsonProperty("requires")]
        public string Requires { get; set; }


        [JsonProperty("delay")]
        public string Delay { get; set; }
        #endregion
    }



}
