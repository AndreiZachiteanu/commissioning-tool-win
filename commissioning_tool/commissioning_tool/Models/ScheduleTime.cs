﻿using commissioning_tool.Converters;
using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class ScheduleTime : BaseViewModel
    {
        #region Property
        [JsonProperty("time")]
        [JsonConverter(typeof(TimeConverter))]
        public TimeSpan Time { get; set; }

        [JsonProperty("days")]
        [JsonConverter(typeof(DaysConverter))]
        public ActivationSetList Days { get; set; } = new ActivationSetList();

        [JsonProperty("datesOfMonth")]
        public List<object> DatesOfMonth { get; set; }

        [JsonProperty("months")]
        public List<object> Months { get; set; }
        #endregion
    }
}
