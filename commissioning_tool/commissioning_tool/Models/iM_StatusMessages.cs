﻿using commissioning_tool.Assets;
using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_StatusMessages : BaseViewModel
    {

        #region Properties
        public int numberOfErrors { get; set; }
        public string LastStatusMessage { get; set; }
        public ObservableCollection<StatusMessage> StatusMessageList { get; set; } = new ObservableCollection<StatusMessage>();
        #endregion

        #region Constructor

        #endregion

        #region Methods
        public void AddMessage(StatusMessage message)
        {
            message.count = StatusMessageList.Count + 1;
            if (StatusMessageList.Count > 0)
            {
                StatusMessageList.Insert(0, message);
            }
            else
            {
                StatusMessageList.Add(message);
            }
            LastStatusMessage = message.message;
        }
        #endregion

    }

    public class StatusMessage : BaseViewModel
    {
        #region Fields
        private string _message;

        #endregion

        #region Properties
        public int count { get; set; }
        public string message { get { return _message; } set { _message = value; } }
        public bool received { get; set; }
        public string dateTimeReceived { get; set; } = DateTime.Now.ToString("[HH:mm:ss - dd.MM.yy]");
        
        #endregion


    }

    public class Uni_OutList : BaseViewModel
    {
        public ObservableCollection<UniverseList> Universe { get; set; } = new ObservableCollection<UniverseList>();//= Enumerable.Range(1, 100).ToList();
        public OutputChannelList OutChannels { get; set; } = new OutputChannelList(); // = Enumerable.Range(1, 512).ToList();
        public Uni_OutList()
        {
            for(int i = 0; i <= 100;i++)
            {
                Universe.Add(new UniverseList { universe = i });
            }
        }

    }
    public class OutputChannelList : ObservableCollection<ChSelModel>
    {
        public OutputChannelList()
        {
            for (int i = 1; i <= 512; i++)
            {
                Add(new ChSelModel { channelNo = i });
            }
        }
    }

    public class StaticInformation : BaseViewModel
    {
        public ObservableCollection<int> Priority { get; set; } 
        public ObservableCollection<ControlMenuItem> ControlMenuList { get; set; }
        public List<string> PolicyTypes { get; set; }
        public List<PolicySequence> PolicySequences { get; set; } = new List<PolicySequence>();
        public List<string> TransitionTypes { get; set; }
        public StaticInformation()
        {
           Priority = new ObservableCollection<int>(Enumerable.Range(1, 100));
            ControlMenuList = new ObservableCollection<ControlMenuItem>
            {
                new ControlMenuItem
                {
                    Icon = FontAwesomeIcons.Lightbulb,
                    Name = "Light",
                    Type = Enums.ControlMenuItemType.LIGHT
                },
                new ControlMenuItem
                {
                    Icon = FontAwesomeIcons.Cubes,
                    Name = "Light Group",
                    Type = Enums.ControlMenuItemType.LIGHTGROUP
                },
                new ControlMenuItem
                {
                    Icon = FontAwesomeIcons.Rss,
                    Name = "Sensor",
                    Type = Enums.ControlMenuItemType.SENSOR
                },
                new ControlMenuItem
                {
                    Icon = FontAwesomeIcons.ProjectDiagram,
                    Name = "Policy",
                    Type = Enums.ControlMenuItemType.POLICY
                },
                new ControlMenuItem
                {
                    Icon = FontAwesomeIcons.Tasks,
                    Name = "Logical Task",
                    Type = Enums.ControlMenuItemType.LOGICALTASK
                },
                new ControlMenuItem
                {
                    Icon = FontAwesomeIcons.LayerGroup,
                    Name = "Logical Task Group",
                    Type = Enums.ControlMenuItemType.LOGICALTASKGROUP
                },
                new ControlMenuItem
                {
                    Icon = FontAwesomeIcons.Film,
                    Name = "Logical Scene",
                    Type = Enums.ControlMenuItemType.LOGICALSCENE
                }

            };
            PolicyTypes = Enum.GetValues(typeof(PolicyType)).Cast<PolicyType>().Select(v => v.ToString()).ToList();
            var sequenceTypes = Enum.GetValues(typeof(PolicySequenceType)).Cast<PolicySequenceType>().Select(v => v).ToList();
            for (int i = 0; i < sequenceTypes.Count; i++)
            {
                PolicySequences.Add(new PolicySequence { Type = sequenceTypes[i] });
            }     
            TransitionTypes = Enum.GetValues(typeof(TransitionType)).Cast<TransitionType>().Select(v => v.ToString()).ToList();
        }
    }

    

    public class iM_StaticStatus : BaseViewModel
    {
        #region Fields
        public static iM_StatusMessages instance;
        public static Uni_OutList ch_instance;
        public static StaticInformation StaticInfo;
        #endregion

        #region Properties
        public static iM_StatusMessages Instance
        {
            get { return instance; }
        }
        #endregion

        #region Constructor
        static iM_StaticStatus()
        {
            instance = new iM_StatusMessages();
            ch_instance = new Uni_OutList();
            StaticInfo = new StaticInformation();
        }



        public static Uni_OutList CH_Instance
        {
            get { return ch_instance; }
            set { ch_instance = value; }
        }

        //Check if Toggable Sequence Buttons can be enabled or not
        internal static bool CheckIfEnabled(PolicySequenceType type, PolicyType selectedType)
        {
            switch (type)
            {
                case PolicySequenceType.OnSequence:
                case PolicySequenceType.OffSequence:
                    switch (selectedType)
                    {
                        case PolicyType.ABSENCE:
                        case PolicyType.PRESENCE:
                        case PolicyType.SWITCHED:
                        case PolicyType.TOGGLE:
                        case PolicyType.TIMER:
                            return true;
                        default:
                            return false;
                    }
                case PolicySequenceType.InterimSequence:
                    switch (selectedType)
                    {
                        case PolicyType.ABSENCE:
                        case PolicyType.PRESENCE:
                        case PolicyType.TIMER:
                            return true;
                        default:
                            return false;

                    }
                case PolicySequenceType.PolicyActivation:

                    switch (selectedType)
                    {
                        case PolicyType.EMERGENCY:
                            return false;
                        default:
                            return true;                                
                    }
                case PolicySequenceType.PolicyParameter:
                    return true;
                default:
                    return false;
                    
            }
        }

        #endregion

    }
}
