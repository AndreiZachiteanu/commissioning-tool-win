﻿using commissioning_tool.ContentViews;
using commissioning_tool.CustomControls;
using commissioning_tool.Enums;
using commissioning_tool.PhysicalDevices;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Device = commissioning_tool.PhysicalDevices.Device;

namespace commissioning_tool.Models
{

    public class iM_DeviceM : BaseViewModel
    {

        #region Fields
        private iM_newPolicyM _newPolicy = new iM_newPolicyM();
        private LogicalTypeEnum _CurrentNewLogicalType = LogicalTypeEnum.TASK;
        #endregion


        #region Properties
        public Device currentSensor { get; set; }
        public ContentView parameterContent { get; set; }
        public bool polProp_Expanded { get; set; }
        public ObservableCollection<Device> lightList { get; set; }
        public ObservableCollection<Device> sensorList { get; set; }
        public ObservableCollection<Device> currentDeviceList { get; set; } 
        public ContentView SpecificLightValueCV { get; set; }
        public iM_LightGroup currentSelectedLightGroup { get; set; } = new iM_LightGroup();
        
        
        /*This affects All Lights View
         * 
         * { 
            get 
            { 
                return _currentSelectedLightGroup; 
            }
            set 
            {
                _currentSelectedLightGroup = value;
                if (_currentSelectedLightGroup != null)
                {
                    currentDeviceList = _currentSelectedLightGroup.groupLights;
                }
            }
        } 
        */
        public bool rs_Expanded_devices { get; set; }
        public bool rs_Expanded_lightsGr { get; set; }
        public bool rs_Expanded_policies { get; set; }
        public bool rs_Expanded_logicals { get; set; }
        //public bool pol_Expanded { get; set; }
        public ContentView expanderContent { get; set; } = new ContentView();
        public ContentView intensityContent { get; set; } = new ContentView();
        public ContentView upperContent { get; set; } = new ContentView();
        public ContentView lowerContent { get; set; } = new ContentView();
        public ContentView createLPanel { get; set; } = new ContentView();
        public iM_LightGroup selectedGroup { get; set; } = new iM_LightGroup(); 
        public Light newLight { get; set; } = new Light();
        public iM_newPolicyM newPolicy
        {
            get
            {
               
                return _newPolicy;
            }
            set
            {
               
                _newPolicy = value;
                
                OnPropertyChanged();
            }
        }
        public ObservableCollection<iM_LightGroup> lightGroups { get; set; } 
        public Device currentDevice { get; set; }
        public ObservableCollection<UniverseList> universeList { get; set; } 
        public List<string> l_typeList { get; set; } 
        public List<string> policyTypes { get; set; }
        public ObservableCollection<ChSelModel> ChSelModelList { get; set; } 
        public ObservableCollection<string> lv_grList { get; set; }
        public ObservableCollection<iM_newPolicyM> policyList { get; internal set; }
        public iM_newPolicyM currentPolicy { get; internal set; }
        public ContentView CrNewPolSplitCV_UP { get; set; }
        public ContentView CrNewPolSplitCV_BOT { get; set; }
        public LightValues currentLightValues { get; set; } = new LightValues();
        public ObservableCollection<NewLogicalItem> NewLogicalItemsList { get; set; } = new ObservableCollection<NewLogicalItem>();
        public Actions testingAction { get; set; }
        public ScheduleActions actionLists { get; set; } = new ScheduleActions();
        public SchedulesM currentSchedule { get; set; } = new SchedulesM();
        public ObservableCollection<Actions> currentTasks { get; set; } = new ObservableCollection<Actions>();
        public ObservableCollection<RTCSource> RTCSourceList { get; set; } = new ObservableCollection<RTCSource>();
        public ObservableCollection<string> DBScenes { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<SchedulesM> ScheduleList { get; set; } = new ObservableCollection<SchedulesM>();
        public List<ObservableCollection<string>> FullListDataName { get; set; } = new List<ObservableCollection<string>>();
        public SceneCapture_LightValues SC_LightValues { get; set; } = new SceneCapture_LightValues();
        public SceneCapture_RTCSetting RTCSetting { get; set; } = new SceneCapture_RTCSetting();
        public ActivationSetList activationList { get; set; } = new ActivationSetList();
        public iM_ActivationDayM currentActivDay { get; set; } = new iM_ActivationDayM();
        public List<string> pickerControlType { get; set; } = Enum.GetValues(typeof(LogicalTypeEnum)).Cast<LogicalTypeEnum>().Select(v => v.ToString()).ToList();
        public ObservableCollection<Device> policyItemsList { get; set; } = new ObservableCollection<Device>();
        public List<int> Priority { get; set; } = Enumerable.Range(1, 100).ToList();
        public List<string> seqTypes { get; set; }
        public List<string> Transition { get; set; }
        public iM_PolSeqM currentSeqToggleButton { get; set; } = new iM_PolSeqM();
        public CustomToggleBtn currentLightToggleButton { get; set; }
        public CustomToggleBtn currentSideMenuToggleButton { get; set; }
        public CustomToggleBtn currentPolicyDevSideTglBtn { get; set; }
        public CustomToggleBtn currentNewPolicySideTglBtn { get; set; }
        public bool iMuneCommissioningMode { get; set; }
        public string seqIDToggled { get; set; }
        public PolicySeqButtons polSeqToggle { get; set; } = new PolicySeqButtons();
        public Device currentPolDevice { get; set; }
        public string currentDMXChColour { get; internal set; }
        public CustomToggleBtn currentLogicalDevSideTglBtn { get; set; }
        public ContentView CrNewLogicalSplitCV_UP { get; internal set; }
        public bool AddFromIdentified { get; internal set; }
        public ObservableCollection<iM_TaskModel> TaskList { get; internal set; }

        public ObservableCollection<iM_SceneModel> SceneList { get; internal set; }

        public ObservableCollection<iM_TaskGroupModel> TaskGroupList { get; set; }
        public CustomToggleBtn currentNewLogicalSideTglBtn { get; internal set; }
        public ContentView CrNewLogicalSplitCV_BOT { get; internal set; }
        public List<LogicalTypeEnum> LogicalTypeList { get; set; }
        public iM_LogicalModel NewLogical { get; set; } = new iM_TaskModel();
        public LogicalTypeEnum CurrentNewLogicalType
        {
            get { return _CurrentNewLogicalType; }
            set
            {
                _CurrentNewLogicalType = value;
                switch (value)
                {
                    case LogicalTypeEnum.TASK:
                        NewLogical = new iM_TaskModel();
                        break;
                    case LogicalTypeEnum.TASK_GROUP:
                        NewLogical = new iM_TaskGroupModel();
                        break;
                    case LogicalTypeEnum.SCENE:
                        NewLogical = new iM_SceneModel();
                        break;
                }

            }
        }



        #endregion


        #region Fields
        public iM_LightGroupSplitCV lightGroupSplitCV = new iM_LightGroupSplitCV();
       
        public iM_LightGrCV lightGroupCV = new iM_LightGrCV();
        public SelChPrimCV selectChPrimCV = new SelChPrimCV();
        public SelChCCTCV selectChCCTCV = new SelChCCTCV();
        public SelChRGBCV selectChRGBCV = new SelChRGBCV();
        public SelChRGBXCV selectChRGBXCV = new SelChRGBXCV();
        public SelUniCV selectUniCV = new SelUniCV();
        public UniverseList currentUniverse;
        public CreateLightCV createLightCV = new CreateLightCV();
        public LightListCV lightListCV = new LightListCV();
        #endregion

        #region Constructor
        public iM_DeviceM()
        {
            //CrNewPolSplitCV_BOT = new ContentView();// { BindingContext = this };

            //newPolicy.ParameterChanged += NewPolicy_ParameterChanged;
            newLight.TypeChanged += NewLight_TypeChanged;
            lightList = new ObservableCollection<Device>();
            sensorList = new ObservableCollection<Device>();
            currentDeviceList = new ObservableCollection<Device>();
            ChSelModelList = new ObservableCollection<ChSelModel>();
            universeList = new ObservableCollection<UniverseList>();
            lightGroups = new ObservableCollection<iM_LightGroup>();
            CrNewPolSplitCV_UP = new ContentView { BindingContext = this };
            l_typeList = Enum.GetValues(typeof(LightType)).Cast<LightType>().Select(v => v.ToString()).ToList();
            policyTypes = Enum.GetValues(typeof(PolicyType)).Cast<PolicyType>().Select(v => v.ToString()).ToList();
            seqTypes = Enum.GetValues(typeof(SequenceType)).Cast<SequenceType>().Select(v => v.ToString()).ToList();
            Transition = Enum.GetValues(typeof(TransitionType)).Cast<TransitionType>().Select(v => v.ToString()).ToList();
            LogicalTypeList = Enum.GetValues(typeof(LogicalTypeEnum)).Cast<LogicalTypeEnum>().ToList();

            NewLogical.TypeChanged += NewLogical_TypeChanged;

            for (int i =1; i<= 100; i++)
            {
                universeList.Add(new UniverseList { universe = i});
            }

            //universeList[0].selected = true;
            for (int i = 1; i <= 512; i++)
            {
                ChSelModelList.Add(new ChSelModel
                {
                    channelNo = i
                });
            }            
        }

        private void NewLogical_TypeChanged(object sender, EventArgs e)
        {
            EventMessage mess = e as EventMessage;

            switch (mess.Message) 
            {
                case "TASK":
                    NewLogical = new iM_TaskModel();
                    CrNewLogicalSplitCV_UP = new ContentView();
                    NewLogicalItemsList = new ObservableCollection<NewLogicalItem>();
                    
                    break;
                case "TASK_GROUP":
                    NewLogical = new iM_TaskGroupModel();
                    CrNewLogicalSplitCV_UP = new ContentView();
                    NewLogicalItemsList = new ObservableCollection<NewLogicalItem>();
                    break;
                case "SCENE":
                    NewLogical = new iM_SceneModel();
                    CrNewLogicalSplitCV_UP = new ContentView();
                    NewLogicalItemsList = new ObservableCollection<NewLogicalItem>();
                    break;
            }

            NewLogical.TypeChanged += NewLogical_TypeChanged;
        }

        private void NewLight_TypeChanged(object sender, EventArgs e)
        {
            for(int i = 0; i< ChSelModelList.Count; i++)
            {
                ChSelModelList[i].prim = false;
                ChSelModelList[i].cool = false;
                ChSelModelList[i].warm = false;
                ChSelModelList[i].red = false;
                ChSelModelList[i].blue = false;
                ChSelModelList[i].green = false;
            }
            switch (newLight.type)
            {
                case LightType.PRIMITIVE:
                    createLPanel.Content = selectChPrimCV;
                    break;
                case LightType.CCT:
                    createLPanel.Content = selectChCCTCV;
                    break;
                case LightType.RGB:
                    createLPanel.Content = selectChRGBCV;
                    break;
                case LightType.RGBX:
                    createLPanel.Content = selectChRGBXCV;
                    break;
                default:
                    createLPanel.Content = null;
                    break;

            }
        }

        public void NewPolicy_ParameterChanged(object sender, EventArgs e)
        {
            var sen = sender as iM_newPolicyM;
            var polSec = new PolicySeqButtons();
            newPolicy.Delay.enabled = false;
            newPolicy.MultiModeOn.enabled = false;
            newPolicy.MultiModeOff.enabled = false;
            for (int i = 0; i< polSec.Count; i++)
            {
                switch (polSec[i].cID)
                {
                    case "onSeq":
                        // = sen.OnValues.enabled;
                        if (!sen.OnValues.enabled) polSec[i].used = false;

                        break;
                    case "offSeq":
                        if (!sen.OffValues.enabled) polSec[i].used = false;
                        break;
                    case "interimSeq":
                        if (!sen.InterimValues.enabled) polSec[i].used = false;
                        polSec[i].isEnabled = CheckIfTypeEnabled(sen.type, "interimSeq");
                        break;
                    case "activation":
                        polSec[i].isEnabled = CheckIfTypeEnabled(sen.type, "activation");// sen.ActivationParam.enabled;
                        if (!sen.ActivationParam.enabled) polSec[i].used = false;
                        break;
                }
            }

            polSeqToggle = polSec;
            CheckIfTypeEnabled(sen.type);
            //OnPropertyChanged("newPolicy");
        }

        private bool CheckIfTypeEnabled(PolicyType type, string v = null)
        {
            switch (type)
            {
                case PolicyType.TOGGLE:
                case PolicyType.SWITCHED:


                    if(v == "interimSeq")
                    {
                        return false;
                    }

                    //OnValues.enabled = true;
                    //OffValues.enabled = true;
                    break;
                case PolicyType.TIMER:
                    //InterimValues.enabled = true;
                    //Delay.enabled = true;
                    //Delay.name = "Off";
                    //OffDelay.enabled = true;
                    //OnValues.enabled = true;
                    //OffValues.enabled = true;
                    newPolicy.Delay.enabled = true;
                    break;
                case PolicyType.PRESENCE:
                case PolicyType.ABSENCE:
                    //Delay.name = "Off";
                    //Delay.enabled = true;
                    //OnValues.enabled = true;
                    //OffValues.enabled = true;
                    //MultiModeOn.enabled = true;
                    //MultiModeOff.enabled = true;
                    ////OffDelay.enabled = true;
                    //InterimValues.enabled = true;
                    newPolicy.MultiModeOn.enabled = true;
                    newPolicy.MultiModeOff.enabled = true;
                    newPolicy.Delay.enabled = true;
                    break;
                case PolicyType.SCENESELECT:
                    //Delay.enabled = true;
                    //Delay.name = "Reset";
                    //Scene.enabled = true;
                    break;
                case PolicyType.RAISEDIM:
                case PolicyType.STEPDIM:
                    if (v == "interimSeq")
                    {
                        return false;
                    }
                    //IntensityCapMax.enabled = true;
                    //IntensityCapMin.enabled = true;
                    //Delay.enabled = true;
                    //Delay.name = "Reset";

                    break;
                case PolicyType.EMERGENCY:
                    //ActivationParam.enabled = false;
                    return false;                                    
            }
            return true;
        }
        #endregion


    }
}
