﻿using commissioning_tool.Converters;
using commissioning_tool.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class Values
    {
        [JsonProperty("CCT")]
        public string CCT { get; set; }

        [JsonProperty("RGB")]
        public string RGB { get; set; }

        [JsonProperty("PRIMITIVE")]
        public string PRIMITIVE { get; set; }
    }

    public class On
    {
        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("transition")]
        public string Transition { get; set; }

        [JsonProperty("values")]
        public Values Values { get; set; }
    }
    public class Interim
    {
        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("transition")]
        public string Transition { get; set; }

        [JsonProperty("values")]
        public Values Values { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("delay")]
        public string Delay { get; set; }
    }

    public class Off
    {
        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("transition")]
        public string Transition { get; set; }

        [JsonProperty("values")]
        public Values Values { get; set; }
    }

    public class Sequence
    {
        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("transition")]
        public string Transition { get; set; }

        [JsonProperty("values")]
        public Values Values { get; set; }
    }

    public class iM_PolicyDetailsM
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("priority")]
        public int Priority { get; set; }

        [JsonProperty("excluded")]
        public object Excluded { get; set; }

        [JsonProperty("restriction")]
        public object Restriction { get; set; }

        [JsonProperty("sensors")]
        public List<object> Sensors { get; set; }

        [JsonProperty("authorisedUsers")]
        public List<string> AuthorisedUsers { get; set; }

        [JsonProperty("running")]
        public bool Running { get; set; }

        [JsonProperty("start")]
        public object Start { get; set; }

        [JsonProperty("stop")]
        public object Stop { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(PolTypeJsonConverter))]
        public PolicyType type { get; set; }

        [JsonProperty("lights")]
        public List<string> Lights { get; set; }

        [JsonProperty("triggered")]
        public bool Triggered { get; set; }

        [JsonProperty("lastRan")]
        public string LastRan { get; set; }

        [JsonProperty("lastTrigger")]
        public string LastTrigger { get; set; }

        [JsonProperty("on")]
        public On On { get; set; }

        [JsonProperty("off")]
        public Off Off { get; set; }

        [JsonProperty("interim")]
        public Interim Interim { get; set; }

        [JsonProperty("override")]
        public object Override { get; set; }

        [JsonProperty("sequence")]
        public Sequence Sequence { get; set; }

        [JsonProperty("controlType")]
        public string ControlType { get; set; }

        [JsonProperty("activeLights")]
        public List<string> ActiveLights { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("restricted")]
        public bool Restricted { get; set; }
    }
}
