﻿using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public abstract class Hierarchy : BaseViewModel
    {
        #region Properties
        public abstract HierarchyMenuItem HierType { get; }
        public abstract HierarchyMenuItem HierChildType { get; }
        public abstract string Icon { get; }
        public abstract string Id { get; set; }
        public abstract string Name { get; set; }
        public abstract HierarchyItemList ItemList { get; set; } 
        public Hierarchy Parent { get; internal set; }
        #endregion
    }
}
