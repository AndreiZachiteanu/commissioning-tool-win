﻿using commissioning_tool.Objects;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Models
{
    public class ProjectsM : BaseViewModel
    {
        #region Properties
        public ObservableCollection<Project> projects { get; set; } = new ObservableCollection<Project>();
        #endregion

        #region Fields
        public INavigation Navigation;
        #endregion
    }

    
}
