﻿using commissioning_tool.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_TaskGroupModel : iM_LogicalModel
    {
        private LogicalTypeEnum _type = LogicalTypeEnum.TASK_GROUP;
        public override event EventHandler TypeChanged;

        protected override void TypeHasChanged()
        {
            EventHandler handler = TypeChanged;
            EventMessage eventMessage = new EventMessage();
            eventMessage.Message = type.ToString();
            handler?.Invoke(this, eventMessage);
        }

        public override LogicalTypeEnum type { get => _type; set { _type = value; TypeHasChanged(); } }

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 

        [JsonProperty("_id")]
        public override string Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("taskIds")]
        public List<string> TaskIds { get; set; }

        [JsonProperty("userIds")]
        public List<string> UserIds { get; set; }

        
    }
}
