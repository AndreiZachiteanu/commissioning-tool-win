﻿using commissioning_tool.Enums;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Models
{
    public class PolicySequence : BaseViewModel
    {
        public PolicySequenceType Type { get; set; }
        public bool Selected { get; set; }
        public bool IsEnabled { get; set; }
        public bool Used { get; set; }

        public override string ToString()
        {
            return Type.ToString();
        }

    }
}
