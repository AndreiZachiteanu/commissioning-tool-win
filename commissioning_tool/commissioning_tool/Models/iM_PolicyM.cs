﻿using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Models
{
    public class iM_PolicyM : BaseViewModel
    {
        #region Properties
        public bool showProps { get; set; }
        public bool selected { get; set; }
        public bool trigger { get; set; }

        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("enabled")]
        public bool enabled { get; set; }

        [JsonProperty("priority")]
        public int priority { get; set; }

        [JsonProperty("sensors")]
        public string[] sensors { get; set; }

        [JsonProperty("authorizedUsers")]
        public string authorizedUsers { get; set; }

        [JsonProperty("running")]
        public bool running { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("lights")]
        public string[] lights { get; set; }

        [JsonProperty("triggered")]
        public bool triggered { get; set; }

        [JsonProperty("lastRan")]
        public string lastRan { get; set; }

        [JsonProperty("lastTrigger")]
        public string lastTrigger { get; set; }

        [JsonProperty("override")]
        public string override_ { get; set; }

        [JsonProperty("multiOn")]
        public string multiOn { get; set; }

        [JsonProperty("multiOff")]
        public string multiOff { get; set; }

        [JsonProperty("offDelay")]
        public string offDelay { get; set; }

        [JsonProperty("on")]
        public PolicyLValues on { get; set; }

        [JsonProperty("off")]
        public PolicyLValues off { get; set; }

        [JsonProperty("interim")]
        public PolicyInterim interim { get; set; }

        [JsonProperty("metadata")]
        public Metadata metadata { get; set; }

        #endregion
    }

    public class Metadata
    {
        #region Properties
        [JsonProperty("createdBy")]
        public string createdBy { get; set; }

        [JsonProperty("dateCreated")]
        public string dateCreated { get; set; }

        [JsonProperty("lastSavedBy")]
        public string lastSavedBy { get; set; }

        [JsonProperty("dateLastSaved")]
        public string dateLastSaved { get; set; }

        [JsonProperty("numberEdits")]
        public int numberEdits { get; set; }

        #endregion
    }

    public class PolicyONOFF 
    {
        #region Properties
        [JsonProperty("duration")]
        public string duration { get; set; }

        [JsonProperty("transition")]
        public string transition { get; set; }

        [JsonProperty("values")]
        public PolicyLValues values { get; set; }
        #endregion
    }

    public class PolicyInterim : PolicyONOFF
    {
        #region Properties

        [JsonProperty("active")]
        public bool active { get; set; }

        [JsonProperty("delay")]
        public string delay { get; set; }
        #endregion
    }

    public class PolicyLValues
    {
        #region Properties
        [JsonProperty("CCT")]
        public string CCT { get; set; }

        [JsonProperty("PRIMITIVE")]
        public string PRIMITIVE { get; set; }

        [JsonProperty("RGBX")]
        public string RGBX { get; set; }

        [JsonProperty("RGB")]
        public string RGB { get; set; }
        #endregion
    }
}
