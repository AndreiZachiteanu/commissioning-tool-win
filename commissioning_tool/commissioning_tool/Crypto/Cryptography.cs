﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace commissioning_tool.Crypto
{
    class Cryptography
    {
        #region Attributes

        private static Cryptography instance;

        private string key = "MAKV2SPBNI99212";
        private RijndaelManaged rijndael;

        #endregion

        #region Properties

        public static Cryptography Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cryptography();
                }

                return instance;
            }
        }

        #endregion

        #region Constructors

        private Cryptography()
        {
            rijndael = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128
            };
        }

        #endregion

        #region Methods

        public string encrypt(string message)
        {
            return encrypt(message, key);
        }

        public string encrypt(string message, string key)
        {
            prepRijndael(key);
            return doEncryption(message).Replace("=", "~").Replace("+", "-").Replace("/", "_");
        }

        private string doEncryption(string message)
        {
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            byte[] encryptedBytes = rijndael.CreateEncryptor()
                .TransformFinalBlock(messageBytes, 0, messageBytes.Length);

            return Convert.ToBase64String(encryptedBytes);
        }

        public string decrypt(string message)
        {
            return decrypt(message, key);
        }

        public string decrypt(string message, string key)
        {
            prepRijndael(key);
            return doDecryption(message.Replace("~", "=").Replace("-", "+").Replace("_", "/"));
        }

        private string doDecryption(string message)
        {
            byte[] messageBytes = new byte[] { new byte()};
            try
            {
                byte[] encryptedBytes = Convert.FromBase64String(message);
                messageBytes = rijndael.CreateDecryptor()
                    .TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
            }
            catch(Exception ex) { }
            return Encoding.UTF8.GetString(messageBytes);
        }

        private void prepRijndael(string key)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(key);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));

            rijndael.Key = keyBytes;
            rijndael.IV = keyBytes;
        }


        #endregion

    }
}
