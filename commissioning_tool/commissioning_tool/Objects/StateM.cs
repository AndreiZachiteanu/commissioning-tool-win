﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace commissioning_tool.Objects
{
    public class StateM : BaseViewModel
    {
        // Client socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 256;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }
}
