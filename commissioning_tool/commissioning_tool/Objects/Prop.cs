﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Objects
{
    public class Prop
    {
        #region Properties
        public string propName { get; set; }
        public string propValue { get; set; }
        #endregion
    }
}
