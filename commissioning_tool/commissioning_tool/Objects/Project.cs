﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Objects
{
    public class Project: BaseViewModel
    {
        #region Properties
        public string projectName { get; set; }
        public ObservableCollection<ImageSource> floorMaps { get; set; } = new ObservableCollection<ImageSource>();
        public bool isAddNew { get; set; }
        #endregion
    }
}
