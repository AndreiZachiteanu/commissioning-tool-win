﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Objects
{
    public class DeviceM : BaseViewModel
    {
        #region Properties
        public string type { get; set; }
        public string model { get; set; }
        public ImageSource icon { get; set; }
        public string prefix { get; set; }
        public ObservableCollection<Prop> props { get; set; } = new ObservableCollection<Prop>();
        public bool usingDev { get; set; }
        #endregion
    }
}
