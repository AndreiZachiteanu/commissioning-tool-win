﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Objects
{
    public class iMuneM : BaseViewModel
    {
        #region Properties
        public string name { get; set; }
        public string IP { get; set; }
        public bool isDHCP { get; set; }
        public string Version { get; set; }
        #endregion
    }
}
