﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class NewProjectVM : BaseViewModel
    {
        #region Properties
        public NewProjectM newProjectM { get; set; }
        #endregion

        #region Constructor
        public NewProjectVM()
        {
            newProjectM = new NewProjectM();

            newProjectM.devices.Add(new Objects.DeviceM()
            {

            });
        }
        #endregion

        #region Commands
        public Command AddMap_Command => new Command(async() => 
        {
            try
            {
                var file = await FilePicker.PickAsync();
                if (file != null)
                {
                    newProjectM.project.floorMaps.Add(ImageSource.FromFile(file.FullPath));
                }
            }catch(Exception ex)
            {
                Debug.WriteLine("file uploaded " + ex);
            }
        });

        public Command TabChanged_Command => new Command((param) => 
        {
            newProjectM.selectedTab = (string)param;
        });
        #endregion
    }
}
