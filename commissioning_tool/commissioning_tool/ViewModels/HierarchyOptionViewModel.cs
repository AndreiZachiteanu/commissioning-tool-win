﻿using commissioning_tool.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class HierarchyOptionViewModel : BaseViewModel
    {
        #region Properties
        public INavigation Navigation { get; set; }
        #endregion

        #region Commands
        public Command HierarchyOption_Command => new Command((param) => 
        {
            var parameter = (string)param;
            switch (parameter)
            {
                case "wHierarchy":
                    Navigation.PushAsync(new iM_Hierarchy());
                    break;
                case "woHierarchy":
                    Navigation.PushAsync(new iM_DevicePage());
                    break;
            }
        });
        #endregion
    }
}
