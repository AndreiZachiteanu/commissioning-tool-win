﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;
using System.Linq;
using Rg.Plugins.Popup.Services;
using commissioning_tool.Models;
using commissioning_tool.Enums;
using commissioning_tool.Connections;
using commissioning_tool.ContentViews;
using commissioning_tool.Popups;

namespace commissioning_tool.ViewModels
{
    public class PopupPageRTCSetViewModel : BaseViewModel
    {

        #region Fields
        private iMune_SSHComs iM_Coms = iMune_SSHComs.GetInstance();
        #endregion

        #region Properties
        public Actions testingAction { get; set; }
        public ScheduleActions actionLists { get; set; } = new ScheduleActions();
        public SchedulesM currentSchedule { get; set; } = new SchedulesM();
        public ObservableCollection<Actions> currentTasks { get; set; } = new ObservableCollection<Actions>();
        public ObservableCollection<RTCSource> RTCSourceList { get; set; } = new ObservableCollection<RTCSource>();
        public ObservableCollection<string> DBScenes { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<SchedulesM> ScheduleList { get; set; } = new ObservableCollection<SchedulesM>();
        public List<ObservableCollection<string>> FullListDataName { get; set; } = new List<ObservableCollection<string>>();
        public SceneCapture_LightValues SC_LightValues { get; set; } = new SceneCapture_LightValues();
        public SceneCapture_RTCSetting RTCSetting { get; set; } = new SceneCapture_RTCSetting();
        public ActivationSetList activationList { get; set; } = new ActivationSetList();
        public iM_ActivationDayM currentActivDay { get; set; } = new iM_ActivationDayM();
        public List<string> pickerControlType { get; set; } = Enum.GetValues(typeof(LogicalTypeEnum)).Cast<LogicalTypeEnum>().Select(v => v.ToString()).ToList();
        #endregion

        #region Constructor
        public PopupPageRTCSetViewModel()
        {

            //DBScenes = commandProcesser.getControlNames(Enums.ControlType.SCENE);


            actionLists = iM_Coms.GetActions();
            ScheduleObject.actionList = actionLists;
           // ScheduleList = iM_Coms.GetSchedulerList();


            var mergedList = new ObservableCollection<Actions>();
            if (ScheduleList != null)
            {
                for (int i = 0; i < ScheduleList.Count; i++)
                {
                    var scheduleActions = ScheduleList[i].actions;
                    for (int y = 0; y < scheduleActions.Count; y++)
                    {
                        if (scheduleActions[y].tasks != null)
                        {
                            for (int z = 0; z < scheduleActions[y].tasks.Count; z++)
                            {
                                var action = actionLists.tasks.Where(w => w.id == scheduleActions[y].tasks[z].id).FirstOrDefault();
                                if (action != null)
                                {
                                    scheduleActions[y].tasks[z] = action;
                                }

                            }
                        }
                        if (scheduleActions[y].scenes != null)
                        {
                            for (int z = 0; z < scheduleActions[y].scenes.Count; z++)
                            {
                                var action = actionLists.scenes.Where(w => w.id == scheduleActions[y].scenes[z].id).FirstOrDefault();
                                if (action != null)
                                {
                                    scheduleActions[y].scenes[z] = action;


                                }
                            }
                        }
                        for (int w = 0; w < scheduleActions[y].tasks.Count; w++)
                        {
                            scheduleActions[y].mergedList.Add(scheduleActions[y].tasks[w]);
                        }
                        for (int w = 0; w < scheduleActions[y].scenes.Count; w++)
                        {
                            scheduleActions[y].mergedList.Add(scheduleActions[y].scenes[w]);
                        }
                    }


                }

            }
            else
            {
                ScheduleList = new ObservableCollection<SchedulesM>();
            }


        }
        #endregion

        #region Methods



        #endregion

        #region Commands
        public Command EditName_Command => new Command((param) => {
            var schedule = param as SchedulesM;
            schedule.editName = !schedule.editName;
        });
        public Command DeleteScheduler_Command => new Command(async(param) => {


            var scheduler = (SchedulesM)param;

            bool answer = await Application.Current.MainPage.DisplayAlert("Are you sure?", "You are about to delete a Scheduler: \n" + scheduler.name, "Yes", "No");
           
            if (answer)
            {
                var result = iM_Coms.DeleteSchedule(scheduler.id);   //.SendCommand("delete_schedule " + scheduler.id);
                if (result.Succesfull)
                {
                    ScheduleList.Remove((SchedulesM)param);
                }
            }
        });

        public Command ChangedActionType_Command => new Command(async (param) => {
            //var x = param;

            await PopupNavigation.Instance.PushAsync(new PopupPageActionPicker(actionLists, (Actions)param));
        });
        public Command AddSource_Command => new Command((param) => {

            if (currentSchedule.actions != null)
            {
                for (int i = 0; i < currentSchedule.actions.Count; i++)
                {
                    if (currentSchedule.actions[i].control == (string)param)
                    {
                        if (actionLists.mergedList.Count != 0)
                        {
                            if (actionLists.tasks.Count > 0)
                            {
                                currentSchedule.actions[i].mergedList.Add(new Actions { name = actionLists.tasks[0].name, id = actionLists.tasks[0].id, type = actionLists.tasks[0].type });// new Actions { type = ControlType.TASK });
                            }
                            else if(actionLists.scenes.Count > 0)
                            {
                                currentSchedule.actions[i].mergedList.Add(new Actions { name = actionLists.scenes[0].name, id = actionLists.scenes[0].id, type = actionLists.scenes[0].type });
                            }
                        }
                    }
                }
            }


        });

        public Command DeleteTimeActivationSetting_Command => new Command((param) => {
            var sender = param as iM_StartEndTimesM;

            currentActivDay.times.Remove(sender);
        });

        public Command AddTimesToActivationSetting_Command => new Command(() => {

            currentActivDay.times.Add(new iM_StartEndTimesM());
        });

        public Command ActivationDaySel_Command => new Command((param) => {
            var sender = param as iM_ActivationDayM;

            currentActivDay.selected = false;

            sender.selected = true;
            currentActivDay = sender;
        });

        public Command DeleteRTCSource_Command => new Command((param) => {

            var action = (Actions)param;



            switch (action.type)
            {
                case Enums.LogicalTypeEnum.TASK:
                    for (int i = 0; i < currentSchedule.actions.Count; i++)
                    {
                        currentSchedule.actions[i].tasks.Remove(action);
                    }
                    break;
                case Enums.LogicalTypeEnum.SCENE:
                    for (int i = 0; i < currentSchedule.actions.Count; i++)
                    {
                        currentSchedule.actions[i].scenes.Remove(action);
                    }
                    break;
            }
            for (int i = 0; i < currentSchedule.actions.Count; i++)
            {
                currentSchedule.actions[i].mergedList.Remove(action);
            }

            //currentSchedule.actions;


        });

        public Command EnableSheduler_Command => new Command(async(param) => {

            var enab = (string)param;
            var ReturnMessage = iM_Coms.EnableSchedule(currentSchedule.id, enab); //Ssh.GetInstance().SendCommand("set_schedule_enabled " + currentSchedule.id + " " + enab);

            if (ReturnMessage.Succesfull)
            {
                if (enab.Equals("1"))
                {
                    currentSchedule.enabled = true;
                }
                else
                {
                    currentSchedule.enabled = false;
                }
            }

        });

        public Command ExecuteScheduler_Command => new Command(async() => {


            if (String.IsNullOrEmpty(currentSchedule.id))
            {
                await App.Current.MainPage.DisplayAlert("Alert!!",
                    "Scheduler doesn't exist! Click \"SAVE CHANGES\" before attempting to use this function", "OK");
            }
            else
            {
                var ReturnMessage = iM_Coms.ExecuteSchedule(currentSchedule.id); // Ssh.GetInstance().SendCommand("execute_schedule_now " + currentSchedule.id);
            }
        });

        public Command SaveScheduler_Command => new Command(async(param) => {


            var responseString = "";


            if (String.IsNullOrEmpty(currentSchedule.id))
            {
                var ReturnMessage = iM_Coms.CreateSchedule(currentSchedule.name); // Ssh.GetInstance().SendCommand("create_schedule " + currentSchedule.name);

                if (ReturnMessage.Succesfull)
                {
                    currentSchedule.id = ReturnMessage.Result.Split(':')[1];
                    currentSchedule.enabled = true;
                    responseString = "Created Schedule: " + currentSchedule.name + "\n";
                }
            }
            var messageToSend = "";

            var nameChanged = iM_Coms.SetScheduleName(currentSchedule.id, currentSchedule.name);// Ssh.GetInstance().SendCommand("set_schedule_name " + currentSchedule.id + " " + currentSchedule.name);

            for (int i = 0; i< currentSchedule.actions.Count; i++)
            {
                currentSchedule.actions[i].tasks = new ObservableCollection<Actions>();
                currentSchedule.actions[i].scenes = new ObservableCollection<Actions>();
                for (int y = 0; y < currentSchedule.actions[i].mergedList.Count; y++)
                {
                    switch (currentSchedule.actions[i].mergedList[y].type)
                    {
                        case LogicalTypeEnum.TASK:
                            currentSchedule.actions[i].tasks.Add(currentSchedule.actions[i].mergedList[y]);
                            break;
                        case LogicalTypeEnum.SCENE:
                            currentSchedule.actions[i].scenes.Add(currentSchedule.actions[i].mergedList[y]);
                            break;
                    }
                }

                switch (currentSchedule.actions[i].control)
                {
                    case "START":
                        if(currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~start";

                            for(int y = 0; y< currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (currentSchedule.actions[i].scenes.Count > 0)
                        {
                            messageToSend += " scene_control~start";

                            for (int y = 0; y < currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                    case "STOP":
                        if (currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~stop";

                            for (int y = 0; y < currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (currentSchedule.actions[i].scenes.Count > 0)
                        {
                            messageToSend += " scene_control~stop";

                            for (int y = 0; y < currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                    case "PAUSE":
                        if (currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~pause";

                            for (int y = 0; y < currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (currentSchedule.actions[i].scenes.Count > 0)
                        {
                            messageToSend += " scene_control~pause";

                            for (int y = 0; y < currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                    case "RESUME":
                        if (currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~resume";

                            for (int y = 0; y < currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " scene_control~resume";

                            for (int y = 0; y < currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                }

            }

            messageToSend = messageToSend.StartsWith("~") ? messageToSend.Substring(1) : messageToSend;

            var ReturnMessage2 = iM_Coms.SetScheduleAction(currentSchedule.id, messageToSend); //Ssh.GetInstance().SendCommand("set_schedule_actions " + currentSchedule.id+ " "+ messageToSend);
            var ReturnMessage3 = iM_Coms.SetScheduleTime(currentSchedule.id, currentSchedule.scheduleTime.Time);//Ssh.GetInstance().SendCommand("set_schedule_time " + currentSchedule.id +" "+ currentSchedule.scheduleTime.Time.ToString(@"hh\:mm\:ss"));

            var days = "";

            for(int i = 0; i< currentSchedule.scheduleTime.Days.Count; i++)
            {
                var day = currentSchedule.scheduleTime.Days[i];
                if (day.use)
                {
                    days += " " + ((int)(day.DayName)).ToString();
                }
            }

            var ReturnMessage4 = iM_Coms.SetScheduleDays(currentSchedule.id, days);//Ssh.GetInstance().SendCommand("set_schedule_days " + currentSchedule.id + days);
            //await PopupNavigation.Instance.PushAsync(new ValidationPopup("iMune Response", nameChanged +"\n"+ ReturnMessage2 + "\n" + ReturnMessage3 +"\n" + ReturnMessage4));

        });

        public Command CloseSchedulerPP_Command => new Command(async() => 
        {
            await PopupNavigation.Instance.PopAllAsync();
        });

        public Command SaveSchedulerPP_Command => new Command(async () => 
        {
            await PopupNavigation.Instance.PopAllAsync();
        });

        public Command AddScheduler_Command => new Command(() => {

            var defaultList = new ObservableCollection<ScheduleActions>
            {
                new ScheduleActions{control = "START"},
                new ScheduleActions{control = "STOP"},
                new ScheduleActions{control = "PAUSE"},
                new ScheduleActions{control = "RESUME"},
            };
            ScheduleList.Add(new SchedulesM { name = "No Name", scheduleTime = new ScheduleTime(), actions = defaultList });
        });

        public Command SelectedSchedule_Command => new Command((param) =>
        {
            if (currentSchedule != null)
            {
                currentSchedule.selected = false;
            }

            currentSchedule = (SchedulesM)param;
            currentSchedule.selected = true;
        });


        #endregion
    }
}
