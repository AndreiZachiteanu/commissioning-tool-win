﻿using commissioning_tool.Assets.Themes;
using commissioning_tool.Enums;
using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class SettingsPopViewModel : BaseViewModel
    {
        #region Fields
        public static ThemeM _ColourTheme = new ThemeM { Theme = new BlueWhiteTheme(), ThemeName = ThemeEnum.BlueWhite };
        public static ThemeM _SizeTheme = new ThemeM { Theme = new MediumSizeTheme(), ThemeName = ThemeEnum.Medium };
        #endregion

        #region Properties
        public AppSetting CurrentSetting { get; set; }
        public ThemeM ColourTheme { get { return _ColourTheme; } set { _ColourTheme = value; OnPropertyChanged(); } } 
        public ThemeM SizeTheme { get { return _SizeTheme; } set { _SizeTheme = value; OnPropertyChanged();} }
        public ObservableCollection<ThemeM> ColourThemeList { get; set; } = new ObservableCollection<ThemeM>();
        public ObservableCollection<ThemeM> SizeThemeList { get; set; } = new ObservableCollection<ThemeM>();
        public ObservableCollection<AppSetting> SettingsList { get; set; } = new ObservableCollection<AppSetting>();
        #endregion

        #region Methods


        private void ChangeTheme()
        {
            /*
            switch (_CurrentTheme.Theme)
            {
                case ThemeEnum.BlueWhite:
                    Colours = new BlueWhiteTheme();
                    break;
                case ThemeEnum.Dark:
                default:
                    Colours = new DarkTheme();
                    //mergedDictionaries.Add(new SmallSizeTheme());
                    break;
            }
            */
            ICollection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
            if (mergedDictionaries != null)
            {
                mergedDictionaries.Clear();

                var resDictionary = new ResourceDictionary();
                switch (ColourTheme.ThemeName) {
                    case ThemeEnum.BlueWhite:
                        resDictionary = new BlueWhiteTheme();
                        switch (SizeTheme.ThemeName)
                        {
                            case ThemeEnum.Large:
                                resDictionary.MergedDictionaries.Add(new LargeSizeTheme());
                                break;
                            case ThemeEnum.Medium:
                                resDictionary.MergedDictionaries.Add(new MediumSizeTheme());
                                break;

                            case ThemeEnum.Small:
                                resDictionary.MergedDictionaries.Add(new SmallSizeTheme());
                                break;
                        }
                        
                        break;
                    case ThemeEnum.Dark:
                        resDictionary = new DarkTheme();
                        switch (SizeTheme.ThemeName)
                        {
                            case ThemeEnum.Large:
                                resDictionary.MergedDictionaries.Add(new LargeSizeTheme());
                                break;
                            case ThemeEnum.Medium:
                                resDictionary.MergedDictionaries.Add(new MediumSizeTheme());
                                break;

                            case ThemeEnum.Small:
                                resDictionary.MergedDictionaries.Add(new SmallSizeTheme());
                                break;
                        }

                        break;
                }
                
                mergedDictionaries.Add(resDictionary);    
            }
            
        }



        #endregion

        #region Commands
        public Command ChangeTheme_Command => new Command((param) =>         
        {
            var sender = param as ThemeM;

            switch (sender.ThemeName) 
            {
                case ThemeEnum.BlueWhite:
                case ThemeEnum.Dark:
                    ColourTheme = sender;
                    break;
                case ThemeEnum.Large:
                case ThemeEnum.Medium:
                case ThemeEnum.Small:
                    SizeTheme = sender;
                    break;
            }
            ICollection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
            if (mergedDictionaries != null)
            {
                mergedDictionaries.Clear();

                var resDictionary = new ResourceDictionary();
                switch (ColourTheme.ThemeName)
                {
                    case ThemeEnum.BlueWhite:
                        resDictionary = new BlueWhiteTheme();
                        switch (SizeTheme.ThemeName)
                        {
                            case ThemeEnum.Large:
                                resDictionary.MergedDictionaries.Add(new LargeSizeTheme());
                                break;
                            case ThemeEnum.Medium:
                                resDictionary.MergedDictionaries.Add(new MediumSizeTheme());
                                break;

                            case ThemeEnum.Small:
                                resDictionary.MergedDictionaries.Add(new SmallSizeTheme());
                                break;
                        }

                        break;
                    case ThemeEnum.Dark:
                        resDictionary = new DarkTheme();
                        switch (SizeTheme.ThemeName)
                        {
                            case ThemeEnum.Large:
                                resDictionary.MergedDictionaries.Add(new LargeSizeTheme());
                                break;
                            case ThemeEnum.Medium:
                                resDictionary.MergedDictionaries.Add(new MediumSizeTheme());
                                break;

                            case ThemeEnum.Small:
                                resDictionary.MergedDictionaries.Add(new SmallSizeTheme());
                                break;
                        }

                        break;
                }

                mergedDictionaries.Add(resDictionary);
            }
        });
        #endregion


        #region Constructor
        public SettingsPopViewModel()
        {
            ColourThemeList.Add(new ThemeM
            {
                ThemeName = Enums.ThemeEnum.BlueWhite,
                Theme = new BlueWhiteTheme()
            }); ;
            ColourThemeList.Add(new ThemeM
            {
                ThemeName = Enums.ThemeEnum.Dark,
                Theme = new DarkTheme()
            });

            SizeThemeList.Add(new ThemeM
            {
                ThemeName = ThemeEnum.Large,
                Theme = new LargeSizeTheme()
            });

            SizeThemeList.Add(new ThemeM
            {
                ThemeName = ThemeEnum.Medium,
                Theme = new MediumSizeTheme()
            });
            SizeThemeList.Add(new ThemeM
            {
                ThemeName = ThemeEnum.Small,
                Theme = new SmallSizeTheme()
            });

            /*
            ICollection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
            ColourTheme = new ThemeM();
            SizeTheme = new ThemeM();
            ColourTheme.Theme = mergedDictionaries.Last();
            SizeTheme.Theme = ColourTheme.Theme.MergedDictionaries.Last();
            */
            SettingsList.Add(new AppSetting {
                SettingText = LocalizationResourceManager.Current.GetValue("iM_SettingPopLanguage"),
                Setting = "Language"
            });
            SettingsList.Add(new AppSetting
            {
                SettingText = LocalizationResourceManager.Current.GetValue("iM_SettingPopTheme"),
                Setting = "Theme"
            });
            SettingsList.Add(new AppSetting
            {
                SettingText = LocalizationResourceManager.Current.GetValue("iM_SettingPopSize"),
                Setting = "Size"
            });



        }
        #endregion
    }
}
