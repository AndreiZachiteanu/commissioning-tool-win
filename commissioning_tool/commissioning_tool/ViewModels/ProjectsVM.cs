﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Text;
using commissioning_tool.Objects;
using Xamarin.Forms;
using commissioning_tool.Views;

namespace commissioning_tool.ViewModels
{
    public class ProjectsVM : BaseViewModel
    {
        #region Properties
        public ProjectsM projectsM { get; set; }
        #endregion

        #region Constructors
        public ProjectsVM()
        {
            projectsM = new ProjectsM();
            projectsM.projects.Add(new Project() { projectName = "Some project" });

            projectsM.projects.Add(new Project() { projectName = "Some other project" });

            projectsM.projects.Add(new Project() { projectName = "Some other other project" });


            projectsM.projects.Add(new Project() { projectName = "ADD Project", isAddNew = true });
        }
        #endregion


        #region Commands
        public Command Project_Command => new Command(async(item) => 
        {
            var project = item as Project;
            if (project.isAddNew)
            {
                await projectsM.Navigation.PushAsync(new NewProjectPage());
            }

            //await projectsM.Navigation.PushAsync(new NewProjectPage());

        });
        #endregion
    }
}
