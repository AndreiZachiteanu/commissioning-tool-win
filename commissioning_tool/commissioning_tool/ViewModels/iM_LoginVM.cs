﻿using commissioning_tool.Connections;
using commissioning_tool.Models;
using commissioning_tool.Objects;
using commissioning_tool.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class iM_LoginVM : BaseViewModel
    {
        #region Fields

        public iMuneDiscovery iMDiscovery;


        #endregion

        #region Properties


        public iM_LoginM imLoginM { get; set; } = new iM_LoginM();
        #endregion

        #region Constructor
        public iM_LoginVM()
        {
            imLoginM = new iM_LoginM();
            
            iMDiscovery = new iMuneDiscovery(11000, imLoginM.iMune_List);

            iMDiscovery.LookforImune();
            
        }
        #endregion

        #region Commands
        public Command Login_Command => new Command(async() =>
        {
            Ssh sClient = Ssh.GetInstance();

            if (sClient.IsConnected())
            {
                sClient.Disconnect();
            }

                imLoginM.ipSelected = !String.IsNullOrEmpty(imLoginM.selectedImune.IP);

            imLoginM.usernameSet = !String.IsNullOrEmpty(imLoginM.username);
            imLoginM.keyUploaded = !String.IsNullOrEmpty(imLoginM.key);


            if (imLoginM.ipSelected
            && imLoginM.usernameSet
            && imLoginM.keyUploaded)
            {


                iMune_MQTT client = iMune_MQTT.GetInstance(imLoginM.selectedImune.IP);


                sClient.Connect(imLoginM.selectedImune.IP, imLoginM.username, imLoginM.key);
                if (sClient.IsConnected())
                {
                    iM_StaticStatus.instance.AddMessage(new StatusMessage
                    {
                        message = "Client is connected | ",
                        received = true,

                    });
                    //await imLoginM.Navigation.PushAsync(new iM_DevicePage());
                    await imLoginM.Navigation.PushAsync(new HierarchyOption());
                }
                Debug.WriteLine("The client may be connected " + client.IsConnected);

                

            }
        });

        public Command Refresh_Command => new Command(() =>
        {


                iMDiscovery.LookforImune();
            
        });

        public Command UploadKey_Command => new Command(async () =>
        {
            


            
            try
            {
                var file = await FilePicker.PickAsync();

                if (file != null)
                {
                    var stream = await file.OpenReadAsync();
                    using (StreamReader reader = new StreamReader(stream))
                    {
                       imLoginM.key = reader.ReadToEnd();
                    }

                    imLoginM.keyName = file.FileName;
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("File Upload", "null file", "Ok");
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("File Upload", ex.ToString(), "Ok");
                //Debug.WriteLine("file uploaded " + ex);
            }
        });
        #endregion
    }
}
