﻿using commissioning_tool.Models;
using commissioning_tool.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class LoginVM : BaseViewModel
    {
        #region Properties
        public LoginM loginM { get; set; }
        #endregion


        #region Constructor
        public LoginVM()
        {
            loginM = new LoginM();
        }
        #endregion


        #region Commands
        public Command Login_Command => new Command(async() =>
        {
            await loginM.Navigation.PushAsync(new ProjectsPage());
        });
        #endregion

    }
}
