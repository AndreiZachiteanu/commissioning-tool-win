﻿using commissioning_tool.Models;
using commissioning_tool.ViewModels;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class PopupPageActionPickerViewModel : BaseViewModel
    {
        #region Fields
        private string _Type;
        #endregion` 

        #region Properties
        public string Type { get { return _Type; } set { _Type = value; switch (value)
                {
                    case "TASK":
                        Actions = actionList.tasks;
                        break;
                    case "SCENE":
                        Actions = actionList.scenes;
                        break;
                }
            } }
        public string Title { get; set; }
        public Actions OriginalACtion { get; set; }
        public Actions currentAction { get; set; }
        public ObservableCollection<Actions> Actions { get; set; } = new ObservableCollection<Actions>();
        public ScheduleActions actionList { get; set; } = new ScheduleActions();

        #endregion

        #region Constructors
        public PopupPageActionPickerViewModel()
        {

        }
        #endregion

        #region Commands
        public Command SetCurrentAction_Command => new Command((param) => {

            if (currentAction != null)
            {
                currentAction.selected = false;
            }

            currentAction = (Actions)param;
            currentAction.selected = true;
        });
        public Command Save_Command => new Command(async() =>
        {
            currentAction.selected = false;
            OriginalACtion.type = currentAction.type;
            OriginalACtion.name = currentAction.name;
            OriginalACtion.id = currentAction.id;
            await PopupNavigation.Instance.PopAsync();
        });

        public Command Cancel_Command => new Command(async() => 
        {
            if (currentAction != null)
            {
                currentAction.selected = false;
            }
            await PopupNavigation.Instance.PopAsync();
        });
        #endregion
    }
}
