﻿using commissioning_tool.Models;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class PickerSelectionPopViewModel : BaseViewModel
    {
        #region Properties
        public ObservableCollection<string> ListOfItems { get; set; } = new ObservableCollection<string>();
        
        public BaseViewModel ObjectToChange { get; set; }

        public string WhatToChange { get; set; }
        #endregion

        #region Commands
        public Command SelectItem_Command => new Command(async(param) => 
        {
            if (ObjectToChange.GetType() == typeof(TaskPhase))
            {
                switch (WhatToChange)
                {
                    case "Light Type":
                        ((TaskPhase)ObjectToChange).LightType = (string)param;
                        break;
                    case "Transition Style":
                        ((TaskPhase)ObjectToChange).TransitionStyle = (string)param;
                        break;
                }
                
            }

            await PopupNavigation.Instance.PopAsync();
            //ObjectToChange.GetType
        });
        #endregion
    }
}
