﻿using commissioning_tool.Connections;
using commissioning_tool.ContentViews;
using commissioning_tool.Enums;
using commissioning_tool.Models;
using Device = commissioning_tool.PhysicalDevices.Device;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;
using System.Linq;
using commissioning_tool.PhysicalDevices;
using Newtonsoft.Json.Linq;
using Xamarin.Essentials;
using DeviceType = commissioning_tool.Enums.DeviceType;
using MQTTnet;
using commissioning_tool.Crypto;
using System.Diagnostics;
using commissioning_tool.Converters;
using commissioning_tool.CustomControls;
using commissioning_tool.Popups;
using Rg.Plugins.Popup.Services;

namespace commissioning_tool.ViewModels
{
    public class iM_DeviceVM
    {
        #region Properties
        public iM_DeviceM iM_DevM { get; set; }
        public string groupName { get; set; }
        public ContentView expanderContent { get; set; } = new ContentView();
        public ContentView upperContent { get; set; } = new ContentView();

        #endregion

        #region Fields
        private iMune_SSHComs iMComs;
        private iMune_MQTT client;
        #endregion

        #region Constructors
        public iM_DeviceVM()
        {

            iM_DevM = new iM_DeviceM();
            iMComs = iMune_SSHComs.GetInstance();

            //RefreshLights();
            //RefreshSensors();
            //RefreshPolicies();
            GetCommissioningMode();
            client = iMune_MQTT.GetInstance();

            //client.RegisterApplicationMessageReceivedHandler(MqttMessages.GetInstance());
            SubscribeTopics();
            //MqttIdentifySensors(iM_DevM.sensorList);
            MqttMessages.GetInstance().SensorIdentified += IM_DeviceVM_SensorIdentified;
            MqttMessages.GetInstance().CommissionModeChanged += IM_DeviceVM_CommissionModeChanged;
            MqttMessages.GetInstance().LogicalActStateChanged += IM_DeviceVM_LogicalActStateChanged;
            MqttMessages.GetInstance().LightValueChanged += IM_DeviceVM_LightValueChanged;
            iMune_MQTT.client.ReconnectTrigger += MQTT_Reconnected;

        }

        private void MQTT_Reconnected(object sender, EventArgs e)
        {
            SubscribeTopics();
        }

        private void IM_DeviceVM_LightValueChanged(object sender, EventArgs e)
        {

            var message = e as EventMessage;
            var light = iM_DevM.lightList.Where(w => w.ID == message.Id).FirstOrDefault() as Light;

            if (light != null)
            {
                var stringToLight = LightValueTypeConverter.ConvertStringToLight(message.Message);
                var values = stringToLight.LightValues;

                switch (stringToLight.Type)
                {
                    case LightType.PRIMITIVE:
                        light._intensity = values.SCIntensity;

                        break;
                    case LightType.CCT:
                        light._CCTIntensity = values.CCTIntensity;
                        light._CCTTemperature = values.CCTTemperature;
                        break;
                        /*
                    case LightType.RGB:
                        light._RGBIntensity = values.RGBIntensity;
                        light._RValue = values.RValue;
                        light._GValue = values.GValue;
                        light._BValue = values.BValue;
                        break;
                        
                    case LightType.RGBX:
                        light._RGBIntensity = values.RGBIntensity;
                        light._RValue = values.RValueX;
                        light._GValue = values.GValueX;
                        light._BValue = values.BValueX;
                        light._xValue = values.xValue;
                        break;
                        */
                }
            }
        }

        private void IM_DeviceVM_LogicalActStateChanged(object sender, EventArgs e)
        {
            var message = (EventMessage)e;
            iM_LogicalModel task; // = new iM_LogicalModel();
            switch (message.LogicalType)
            {
                case LogicalTypeEnum.TASK:

                    task = iM_DevM.TaskList.Where(w => w.Id == message.Id).FirstOrDefault();
                    if (task != null)
                    {
                        task.Status = message.Message;
                    }
                    break;
                case LogicalTypeEnum.TASK_GROUP:
                    task = iM_DevM.TaskGroupList.Where(w => w.Id == message.Id).FirstOrDefault();
                    if (task != null)
                    {
                        task.Status = message.Message;
                    }
                    break;
                case LogicalTypeEnum.SCENE:
                    task = iM_DevM.SceneList.Where(w => w.Id == message.Id).FirstOrDefault();
                    if (task != null)
                    {
                        task.Status = message.Message;
                    }
                    break;
            }
        }

        private void GetCommissioningMode()
        {
            iM_DevM.iMuneCommissioningMode = iMComs.GetCommissioningMode();
        }

        private void IM_DeviceVM_CommissionModeChanged(object sender, EventArgs e)
        {
            MainThread.BeginInvokeOnMainThread(() => { iM_DevM.iMuneCommissioningMode = Convert.ToBoolean(((EventMessage)e).Message); });
            
        }

        private void IM_DeviceVM_SensorIdentified(object sender, EventArgs e)
        {

            var topic = ((MqttApplicationMessageReceivedEventArgs)e).ApplicationMessage.Topic.Split('/');
            var sensorID = topic[topic.Length - 1];
            string decodedMessage = Encoding.ASCII.GetString(((MqttApplicationMessageReceivedEventArgs)e).ApplicationMessage.Payload);
            var sensor = iM_DevM.sensorList.Where(w => ((Sensor)w).ID == sensorID).FirstOrDefault();
            if (sensor != null)
            {
                var identifiedState = decodedMessage.Split(':');
                if (identifiedState.Length == 2)
                {
                    ((Sensor)sensor).identified = Convert.ToBoolean(identifiedState[1].Trim());
                }
                if (iM_DevM.AddFromIdentified)
                {
                    MainThread.BeginInvokeOnMainThread(() =>
                    {
                        if (!iM_DevM.policyItemsList.Contains(sensor))
                        {
                            iM_DevM.policyItemsList.Add(sensor);
                        }
                    });
                }
            }
        }





        #endregion

        #region Methods

        private void SubscribeTopics()
        {
            client.Subscribe("sensor/status/#");
            client.Subscribe("imune/light/#");
            client.Subscribe("imune/status/commissioning");
        }
        public void RefreshLights()
        {
            MainThread.BeginInvokeOnMainThread(() =>
            {
                GetLights();
                iM_DevM.lightGroups = new ObservableCollection<iM_LightGroup>();
                iM_DevM.lightGroups = iMComs.GetGroups();
            });
        }

        public void RefreshSensors()
        {
            GetSensors();
        }

        public void RefreshPolicies()
        {
            GetPolicies();
        }

        private void GetPolicies()
        {
            iM_DevM.policyList = new ObservableCollection<iM_newPolicyM>();
            iM_DevM.policyList = iMComs.GetPolicies();
        }

        public void GetSensors()
        {
            iM_DevM.sensorList = new ObservableCollection<Device>();
            iM_DevM.sensorList = (ObservableCollection<Device>)iMComs.GetSensors();
        }
        private void GetLights()
        {
            iM_DevM.lightList = new ObservableCollection<Device>();
            iM_DevM.lightList = (ObservableCollection<Device>)iMComs.GetLights();
            for (int i = 0; i < iM_DevM.lightList.Count; i++)
            {
                var light = iMComs.GetLightValue(iM_DevM.lightList[i].ID);
                var values = light.values;
                var currentLight = ((Light)iM_DevM.lightList[i]);
                switch (light.type)
                {
                    case LightType.PRIMITIVE:
                        currentLight._intensity = values.SCIntensity;
                        break;

                    case LightType.CCT:
                        currentLight._CCTTemperature = values.CCTTemperature;
                        currentLight._CCTIntensity = values.CCTIntensity;


                        break;

                    case LightType.RGB:
                        currentLight._RValue = values.RValue;
                        currentLight._GValue = values.GValue;
                        currentLight._BValue = values.BValue;
                        break;

                    case LightType.RGBX:
                        currentLight._RValue = values.RValue;
                        currentLight._GValue = values.GValue;
                        currentLight._BValue = values.BValue;
                        currentLight._xValue = values.xValue;
                        break;
                }

            }
        }
        #endregion

        #region Commands
        public Command ShowLights_Command => new Command((param) =>
        {
            RefreshLights();

            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;
            iM_DevM.currentDeviceList = new ObservableCollection<Device>();
            iM_DevM.expanderContent = new ContentView();
            iM_DevM.upperContent = new ContentView();


            iM_DevM.expanderContent.Content = new LightProps { BindingContext = this };

            iM_DevM.upperContent = new LightListCV { BindingContext = this };
            iM_DevM.currentDeviceList = iM_DevM.lightList;
        });

        public Command ShowSensors_Command => new Command((param) =>
        {
            RefreshSensors();
            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;

            iM_DevM.currentDeviceList = new ObservableCollection<Device>();

            iM_DevM.expanderContent = new ContentView();
            iM_DevM.upperContent = new ContentView();
            iM_DevM.expanderContent = new iM_SensorProps { BindingContext = this };

            iM_DevM.upperContent = new SensorListCV { BindingContext = this };

            iM_DevM.currentDeviceList = iM_DevM.sensorList;

        });

        public Command AddDeviceToPolFromMQTT_Command => new Command(() =>
        {
            if (iM_DevM.sensorList.Count == 0)
            {
                RefreshSensors();
            }
            iM_DevM.AddFromIdentified = !iM_DevM.AddFromIdentified;
        });

        public Command StartCommissioning_Command => new Command(() =>
        {
            var response = iMComs.ChangeCommissioningMode(!iM_DevM.iMuneCommissioningMode);
        });
        public Command ShowPolicies_Command => new Command((param) =>
        {
            RefreshPolicies();
            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;
            iM_DevM.expanderContent = new ContentView();
            iM_DevM.upperContent = new ContentView();
            iM_DevM.expanderContent = new iM_PolicyProps { BindingContext = this };
            iM_DevM.upperContent = new iM_PolicyListCV { BindingContext = this };
        });
        public Command IdentifySensor_Command => new Command((param) =>
        {
            Sensor sensor = (Sensor)param;
            sensor.identified = !sensor.identified;
            iMComs.IdentifySensor(sensor.ID, sensor.identified);
        });

        public Command ShowCreateLightUI_Command => new Command(() =>
        {
            iM_DevM.expanderContent = new ContentView();
            iM_DevM.upperContent = new ContentView();

            iM_DevM.upperContent = new CreateLightCV { BindingContext = this };



        });

        public Command CreateNewGroup_Command => new Command((param) =>
        {
            iMComs.CreateGroup((string)param);
        });

        public Command TriggerSensor_CommandOn => new Command((param) =>
        {
            iMComs.TriggerSensor(((Sensor)iM_DevM.currentSensor).ID, param.ToString(), 1);
        });

        public Command TriggerSensor_CommandOff => new Command((param) =>
        {
            iMComs.TriggerSensor(((Sensor)iM_DevM.currentSensor).ID, param.ToString(), 0);
        });

        public Command DeleteGroup_Command => new Command((param) =>
        {
            var x = param;

            iMComs.DeleteGroup((iM_LightGroup)param);
        });

        public Command PolicyDeviceTap_Command => new Command((param) =>
        {
            var device = (Device)param;
            device.showDevPolProps = true;
            iM_DevM.intensityContent = new ContentView();
            if (iM_DevM.currentPolDevice != null)
            {
                if (device.ID == iM_DevM.currentPolDevice.ID)
                {
                    iM_DevM.polProp_Expanded = !iM_DevM.polProp_Expanded;
                }
            }
            else if (iM_DevM.polProp_Expanded == false)
            {
                iM_DevM.polProp_Expanded = true;
            }
            switch (device.devType)
            {
                case DeviceType.light:

                    if (iM_DevM.currentPolDevice != null)
                    {
                        if (!iM_DevM.currentPolDevice.ID.Equals(device.ID))
                        {
                            iM_DevM.currentPolDevice.showDevPolProps = false;
                        }
                    }

                    switch (((Light)device).type)
                    {
                        case LightType.PRIMITIVE:

                            iM_DevM.intensityContent.Content = new PrimLiCV();
                            break;
                        case LightType.CCT:
                            iM_DevM.intensityContent.Content = new CCTLiCV();
                            break;
                    }
                    iM_DevM.currentPolDevice = (Light)device;
                    break;
                case DeviceType.sensor:

                    if (iM_DevM.currentPolDevice != null)
                    {

                        if (!iM_DevM.currentPolDevice.ID.Equals(device.ID))
                        {
                            iM_DevM.currentPolDevice.showDevPolProps = false;
                        }
                    }
                    iM_DevM.currentPolDevice = device;

                    break;
                default:

                    break;
            }
        });

        public Command DeviceTap_Command => new Command((param) =>
        {
            iM_DevM.expanderContent = new ContentView();

            iM_DevM.intensityContent = new ContentView();
            var device = (Device)param;
            device.showProps = true;




            switch (device.devType)
            {
                case DeviceType.light:
                    iM_DevM.expanderContent.Content = new LightProps { BindingContext = this };
                    if (iM_DevM.currentDevice != null)
                    {
                        if (!iM_DevM.currentDevice.ID.Equals(device.ID))
                        {
                            iM_DevM.currentDevice.showProps = false;
                        }
                    }

                    if (iM_DevM.currentDevice != null)
                    {
                        if (device.ID == iM_DevM.currentDevice.ID)
                        {
                            iM_DevM.rs_Expanded_devices = !iM_DevM.rs_Expanded_devices;
                        }
                    }
                    else if (iM_DevM.rs_Expanded_devices == false)
                    {
                        iM_DevM.rs_Expanded_devices = true;
                    }

                    iM_DevM.currentDevice = null;


                    switch (((Light)device).type)
                    {
                        case LightType.PRIMITIVE:

                            iM_DevM.intensityContent.Content = new PrimLiCV();
                            break;
                        case LightType.CCT:
                            iM_DevM.intensityContent.Content = new CCTLiCV();
                            break;
                    }
                    iM_DevM.currentDevice = (Light)device;
                    break;
                case DeviceType.sensor:
                    iM_DevM.expanderContent.Content = new iM_SensorProps { BindingContext = this };
                    if (iM_DevM.currentSensor != null)
                    {
                        if (!((Sensor)iM_DevM.currentSensor).ID.Equals(((Sensor)device).ID))
                        {
                            iM_DevM.currentSensor.showProps = false;
                        }
                        if (device.ID == iM_DevM.currentSensor.ID)
                        {
                            iM_DevM.rs_Expanded_devices = !iM_DevM.rs_Expanded_devices;
                        }
                    }
                    else if (iM_DevM.rs_Expanded_devices == false)
                    {
                        iM_DevM.rs_Expanded_devices = true;
                    }


                    iM_DevM.currentSensor = device;

                    break;
                default:

                    break;
            }



        });

        public Command PolicyTapped_Command => new Command((param) =>
        {

            var device = (iM_newPolicyM)param;
            device.showProps = true;
            if (iM_DevM.currentPolicy != null)
            {

                if (device.ID == iM_DevM.currentPolicy.ID)
                {
                    iM_DevM.rs_Expanded_policies = !iM_DevM.rs_Expanded_policies;
                }
                if (!iM_DevM.currentPolicy.ID.Equals(device.ID))
                {
                    iM_DevM.currentPolicy.showProps = false;
                }

            }
            else if (iM_DevM.rs_Expanded_policies == false)
            {
                iM_DevM.rs_Expanded_policies = true;
            }
            iM_DevM.currentPolicy = device;
        });

        public Command ReturnToLightProps_Command => new Command(async () =>
        {
            RefreshLights();
            await iM_DevM.expanderContent.Content.TranslateTo(-120, 0, 40, Easing.CubicIn);
            await iM_DevM.expanderContent.Content.FadeTo(0, 50);
            iM_DevM.expanderContent = new LightProps() { BindingContext = this };



        });

        public Command ChangeDMXAddress_Command => new Command(() =>
        {
            iMComs.ChangeLightDMXAddresses((Light)iM_DevM.currentDevice);

            RefreshLights();
        });

        public Command IdentifyAll_Command => new Command(() =>
        {

            for (int i = 1; i <= 100; i++)
            {
                //Uncomment when done! DANGER!
                // iMComs.IdentifyLight(i.ToString(), new List<int>(Enumerable.Range(1, 512)));
            }
        });

        public Command EditLightDMXAdd_Command => new Command(async () =>
        {
            await iM_DevM.expanderContent.Content.TranslateTo(-120, 0, 40, Easing.CubicIn);
            await iM_DevM.expanderContent.Content.FadeTo(0, 50);
            iM_DevM.expanderContent = new iM_DMXChangeCV { BindingContext = this };
        });
        public Command CreateLight_Command => new Command(() =>
        {
            var id = "";
            /*
            switch (((Light)iM_DevM.newLight).type)
            {
                case LightType.PRIMITIVE:
                    var result = iMComs.CreatePRIMLight(iM_DevM.newLight,
                                        iM_DevM.ChSelModelList.Where(x => x.prim).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_DevM.universeList.Where(x => x.selected).ToList());
                    id = result.id;
                    RefreshLights();
                    break;
                case LightType.CCT:
                    var result1 = iMComs.CreateCCTLight(iM_DevM.newLight,
                                        iM_DevM.ChSelModelList.Where(x => x.warm).ToList(),
                                        iM_DevM.ChSelModelList.Where(x => x.cool).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_DevM.universeList.Where(x => x.selected).ToList());
                    id = result1.id;
                    RefreshLights();
                    break;

                case LightType.RGB:
                    var result2 = iMComs.CreateRGBLight(iM_DevM.newLight,
                                        iM_DevM.ChSelModelList.Where(x => x.red).ToList(),
                                        iM_DevM.ChSelModelList.Where(x => x.green).ToList(),
                                        iM_DevM.ChSelModelList.Where(x => x.blue).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_DevM.universeList.Where(x => x.selected).ToList());
                    id = result2.id;
                    RefreshLights();
                    break;
                case LightType.RGBX:
                    var result3 = iMComs.CreateRGBXLight(iM_DevM.newLight,
                                        iM_DevM.ChSelModelList.Where(x => x.red).ToList(),
                                        iM_DevM.ChSelModelList.Where(x => x.green).ToList(),
                                        iM_DevM.ChSelModelList.Where(x => x.blue).ToList(),
                                        iM_DevM.ChSelModelList.Where(x => x.xCh).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_DevM.universeList.Where(x => x.selected).ToList(),
                                        iM_DevM.newLight.xChannelName);
                    id = result3.id;
                    RefreshLights();
                    break;

            }
            */

            switch (((Light)iM_DevM.newLight).type)
            {
                case LightType.PRIMITIVE:
                    var result = iMComs.CreatePRIMLight(iM_DevM.newLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.prim).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList());
                    id = result.id;
                    RefreshLights();
                    break;
                case LightType.CCT:
                    var result1 = iMComs.CreateCCTLight(iM_DevM.newLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.warm).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.cool).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList());
                    id = result1.id;
                    RefreshLights();
                    break;

                case LightType.RGB:
                    var result2 = iMComs.CreateRGBLight(iM_DevM.newLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList());
                    id = result2.id;
                    RefreshLights();
                    break;
                case LightType.RGBX:
                    var result3 = iMComs.CreateRGBXLight(iM_DevM.newLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.xCh).ToList(),
                                        iM_DevM.selectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList(),
                                        iM_DevM.newLight.xChannelName);
                    id = result3.id;
                    RefreshLights();
                    break;

            }

            if (iM_DevM.selectedGroup.id != null && !id.Equals(""))
            {
                iMComs.AddLightToGroup(iM_DevM.selectedGroup.id, id);
            }
        });


        public Command ChangeSensorName_Command => new Command((param) =>
        {
            iMComs.ChangeSensorName(iM_DevM.currentSensor.ID, (string)param);
        });

        public Command IdentifyDMX_Command => new Command((param) =>
        {
            iMComs.IdentifyDMX(Convert.ToInt32(((DMXAddress)param).oldValue), ((Light)iM_DevM.currentDevice).universe);


        });
        public Command ActLight_Command => new Command((param) =>
        {
            var device = (Light)param;
            //Uncomment when done!IdentifyLightDMX_Command
            //iMComs.ChangeLightLevel(device, true);
        });

        public Command IdentifyLightNew_Command => new Command((param) =>
        {
            var x = param as Light;

            iMComs.IdentifyLight(((Light)param).ID);
        });

        public Command IdentifyLightDMX_Command => new Command(() =>
        {
            switch (iM_DevM.newLight.type)
            {
                case LightType.PRIMITIVE:
                    iMComs.IdentifyLightDMX(iM_DevM.newLight.universe.ToString(),
                            iM_StaticStatus.ch_instance.OutChannels.Where(x => x.prim).Select(x => x.channelNo).ToList());
                    break;
                case LightType.CCT:
                    var coolCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.cool).Select(x => x.channelNo).ToList();
                    var warmCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.warm).Select(x => x.channelNo).ToList();
                    coolCh.AddRange(warmCh);

                    iMComs.IdentifyLightDMX(iM_DevM.newLight.universe.ToString(), coolCh);

                    break;
                case LightType.RGB:
                    var redCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).Select(x => x.channelNo).ToList();
                    var greenCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).Select(x => x.channelNo).ToList();
                    var blueCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).Select(x => x.channelNo).ToList();
                    redCh.AddRange(greenCh);
                    redCh.AddRange(blueCh);
                    iMComs.IdentifyLightDMX(iM_DevM.newLight.universe.ToString(), redCh);

                    break;

                case LightType.RGBX:
                    var redxCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).Select(x => x.channelNo).ToList();
                    var greenxCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).Select(x => x.channelNo).ToList();
                    var bluexCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).Select(x => x.channelNo).ToList();
                    var whitexCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.xCh).Select(x => x.channelNo).ToList();
                    redxCh.AddRange(greenxCh);
                    redxCh.AddRange(bluexCh);
                    redxCh.AddRange(whitexCh);
                    iMComs.IdentifyLightDMX(iM_DevM.newLight.universe.ToString(), redxCh);

                    break;
            }

        });

        public Command ShowLightGr_Command => new Command((param) =>
        {
            RefreshLights();

            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;

            iM_DevM.currentDeviceList = new ObservableCollection<Device>();
            iM_DevM.expanderContent = new ContentView();
            iM_DevM.upperContent = new ContentView();

            iM_DevM.expanderContent = new LightProps { BindingContext = this };
            iM_DevM.upperContent = new iM_LightGroupSplitCV { BindingContext = this };
            iM_DevM.currentDeviceList = iM_DevM.lightList;

        });
        public Command LightsNewPolicy_Command => new Command((param) =>
        {
            RefreshLights();

            iM_DevM.currentPolicyDevSideTglBtn = (CustomToggleBtn)param;

            iM_DevM.currentDeviceList = new ObservableCollection<Device>();
            iM_DevM.expanderContent = new ContentView();
            iM_DevM.CrNewPolSplitCV_UP = new ContentView();


            iM_DevM.expanderContent = new LightProps { BindingContext = this };
            iM_DevM.CrNewPolSplitCV_UP = new LightListCV { BindingContext = this };
            iM_DevM.currentDeviceList = iM_DevM.lightList;
        });

        public Command GrLightsNewPolicy_Command => new Command((param) =>
        {
            RefreshLights();
            iM_DevM.currentPolicyDevSideTglBtn = (CustomToggleBtn)param;
            iM_DevM.expanderContent = new ContentView();
            iM_DevM.CrNewPolSplitCV_UP = new ContentView();
            iM_DevM.currentDeviceList = null;
            iM_DevM.CrNewPolSplitCV_UP = new iM_GroupLightsCV { BindingContext = this };

        });

        public Command DeletePolicy_Command => new Command(() =>
        {
            iMComs.DeletePolicies(iM_DevM.policyList);


        });

        public Command ActivationDaySel_Command => new Command((param) =>
        {
            var sender = param as iM_ActivationDayM;

            iM_DevM.currentActivDay.selected = false;

            sender.selected = true;
            iM_DevM.currentActivDay = sender;
        });


        public Command lightValuesCV_Command => new Command((param) =>
        {
            //iM_DevM.parameterContent = new ContentView();

            // Debug.WriteLine("something");

            var sender = param as iM_PolSeqM;

            if (iM_DevM.currentSeqToggleButton != null)
            {
                iM_DevM.currentSeqToggleButton.selected = false;
            }
            /*
             for (int i = 0; i < iM_DevM.polSeqToggle.Count; i++)
             {
                 iM_DevM.polSeqToggle[i].selected = false;
             }
            */
            sender.selected = true;
            iM_DevM.currentSeqToggleButton = sender;

            switch (sender.cID)
            {
                case "onSeq":
                    iM_DevM.currentLightValues = iM_DevM.newPolicy.OnValues;
                    iM_DevM.currentLightValues.transition.enabled = true;
                    iM_DevM.currentLightValues.duration.enabled = true;
                    iM_DevM.currentLightValues.delay.enabled = false;
                    iM_DevM.parameterContent = new iM_LightValueCV { BindingContext = this };
                    break;
                case "offSeq":
                    iM_DevM.currentLightValues = iM_DevM.newPolicy.OffValues;
                    iM_DevM.currentLightValues.transition.enabled = true;
                    iM_DevM.currentLightValues.duration.enabled = true;
                    iM_DevM.currentLightValues.delay.enabled = false;
                    iM_DevM.parameterContent = new iM_LightValueCV { BindingContext = this };
                    break;
                case "interimSeq":
                    iM_DevM.currentLightValues = iM_DevM.newPolicy.InterimValues;

                    iM_DevM.currentLightValues.transition.enabled = true;
                    iM_DevM.currentLightValues.duration.enabled = true;
                    iM_DevM.currentLightValues.delay.enabled = true;
                    iM_DevM.parameterContent = new iM_LightValueCV { BindingContext = this };
                    break;
                case "parameter":
                    iM_DevM.parameterContent = new iM_NewPolicyParam { BindingContext = this };
                    break;
                case "activation":
                    iM_DevM.parameterContent = new iM_NewPolicyActivation { BindingContext = this };
                    break;
            }

        });


        public Command AddTimesToActivationPol_Command => new Command(() =>
        {

            iM_DevM.currentActivDay.times.Add(new iM_StartEndTimesM());
        });

        public Command DeleteTimeActivationPol_Command => new Command((param) =>
        {
            var sender = param as iM_StartEndTimesM;

            iM_DevM.currentActivDay.times.Remove(sender);
        });



        public Command CreatePolicy_Command => new Command(() =>
        {

            iMComs.CreatePolicy(iM_DevM.newPolicy, iM_DevM.polSeqToggle, iM_DevM.policyItemsList);

        });

        public Command SpecLightValue_Command => new Command((param) =>
        {
            iM_DevM.SpecificLightValueCV = new ContentView();
            var currentBtn = param as CustomToggleBtn;
            iM_DevM.currentLightToggleButton = currentBtn;
            switch (currentBtn.Text)
            {
                case "SC":
                    iM_DevM.SpecificLightValueCV = new SCIntensitySliderCV { BindingContext = this };
                    break;
                case "CCT":
                    iM_DevM.SpecificLightValueCV = new CCTLightValueCV { BindingContext = this };
                    break;
                case "RGB":
                    iM_DevM.SpecificLightValueCV = new RGBLightValueCV { BindingContext = this };
                    break;
                case "RGBX":
                    iM_DevM.SpecificLightValueCV = new RGBXLightValueCV { BindingContext = this };
                    break;
            }
        });

        public Command SensorsNewPolicy_Command => new Command((param) =>
        {
            RefreshSensors();
            iM_DevM.currentPolicyDevSideTglBtn = (CustomToggleBtn)param;
            iM_DevM.currentDeviceList = new ObservableCollection<Device>();
            iM_DevM.expanderContent = new ContentView();
            iM_DevM.CrNewPolSplitCV_UP = new ContentView();
            iM_DevM.expanderContent = new iM_SensorProps { BindingContext = this };
            iM_DevM.CrNewPolSplitCV_UP = new SensorListCV() { BindingContext = this };
            iM_DevM.currentDeviceList = iM_DevM.sensorList;
        });

        public Command ShowCreatePolicyUI_Command => new Command(() =>
        {
            iM_DevM.newPolicy = new iM_newPolicyM();
            iM_DevM.newPolicy.ParameterChanged += iM_DevM.NewPolicy_ParameterChanged;
            iM_DevM.policyItemsList = new ObservableCollection<Device>();
            iM_DevM.currentLightValues = new LightValues();
            iM_DevM.expanderContent = new ContentView();

            iM_DevM.upperContent = new ContentView();
            iM_DevM.CrNewPolSplitCV_BOT = new ContentView();
            iM_DevM.CrNewPolSplitCV_BOT = new iM_PolicyItemsCV { BindingContext = this };
            iM_DevM.upperContent = new iM_CreateNewPolicyCV { BindingContext = this };
        });


        public Command AddDMXAddress_Command => new Command(() =>
        {
            switch (((Light)iM_DevM.currentDevice).type)
            {
                case LightType.PRIMITIVE:
                    ((Light)iM_DevM.currentDevice).dmxAddress.primAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    break;
                case LightType.CCT:
                    ((Light)iM_DevM.currentDevice).dmxAddress.coolAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    ((Light)iM_DevM.currentDevice).dmxAddress.warmAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    break;
                case LightType.RGB:
                    ((Light)iM_DevM.currentDevice).dmxAddress.redAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    ((Light)iM_DevM.currentDevice).dmxAddress.greenAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    ((Light)iM_DevM.currentDevice).dmxAddress.blueAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });


                    break;
                case LightType.RGBX:
                    ((Light)iM_DevM.currentDevice).dmxAddress.redAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    ((Light)iM_DevM.currentDevice).dmxAddress.greenAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    ((Light)iM_DevM.currentDevice).dmxAddress.blueAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });

                    ((Light)iM_DevM.currentDevice).dmxAddress.whiteAddresses.Add(new DMXAddress { oldValue = "0", newValue = "" });
                    break;
            }
        });

        public Command SelectChannelColour => new Command((param) =>
        {
            iM_DevM.currentDMXChColour = (string)param;
        });
        public Command DeleteDMXAddress_Command => new Command((param) =>
        {
            switch (((Light)iM_DevM.currentDevice).type)
            {
                case LightType.PRIMITIVE:
                    ((Light)iM_DevM.currentDevice).dmxAddress.primAddresses.Remove((DMXAddress)param);
                    break;

                case LightType.CCT:
                    if (((Light)iM_DevM.currentDevice).dmxAddress.warmAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.warmAddresses.Remove((DMXAddress)param);
                    }
                    if (((Light)iM_DevM.currentDevice).dmxAddress.coolAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.coolAddresses.Remove((DMXAddress)param);
                    }
                    break;
                case LightType.RGB:
                    if (((Light)iM_DevM.currentDevice).dmxAddress.redAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.redAddresses.Remove((DMXAddress)param);
                    }
                    if (((Light)iM_DevM.currentDevice).dmxAddress.blueAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.blueAddresses.Remove((DMXAddress)param);
                    }
                    if (((Light)iM_DevM.currentDevice).dmxAddress.greenAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.greenAddresses.Remove((DMXAddress)param);
                    }
                    break;
                case LightType.RGBX:
                    if (((Light)iM_DevM.currentDevice).dmxAddress.redAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.redAddresses.Remove((DMXAddress)param);
                    }
                    if (((Light)iM_DevM.currentDevice).dmxAddress.blueAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.blueAddresses.Remove((DMXAddress)param);
                    }
                    if (((Light)iM_DevM.currentDevice).dmxAddress.greenAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.greenAddresses.Remove((DMXAddress)param);
                    }
                    if (((Light)iM_DevM.currentDevice).dmxAddress.whiteAddresses.Contains((DMXAddress)param))
                    {
                        ((Light)iM_DevM.currentDevice).dmxAddress.whiteAddresses.Remove((DMXAddress)param);
                    }
                    break;
            }
        });

        public Command DeleteLight_Command => new Command(() =>
        {
            iMComs.DeleteLights(iM_DevM.lightList);
        });

        public Command MoveSelectedDown_Command => new Command(() =>
        {
            if (iM_DevM.currentSelectedLightGroup != null)
            {
                for (int i = 0; i < iM_DevM.lightList.Count; i++)
                {
                    if (iM_DevM.lightList[i].selected)
                    {
                        iMComs.AddLightToGroup(iM_DevM.currentSelectedLightGroup.id, iM_DevM.lightList[i].ID);
                    }
                }
            }

            RefreshLights();
        });
        public Command DeleteLight_Gr_Command => new Command(() =>
        {

        });

        public Command ShowPolicyItems_Command => new Command((param) =>
        {
            iM_DevM.currentNewPolicySideTglBtn = (CustomToggleBtn)param;
            iM_DevM.CrNewPolSplitCV_BOT = new ContentView();
            iM_DevM.CrNewPolSplitCV_BOT = new iM_PolicyItemsCV { BindingContext = this };
        });

        public Command ShowPolicyLogic_Command => new Command((param) =>
        {
            iM_DevM.currentNewPolicySideTglBtn = (CustomToggleBtn)param;
            iM_DevM.CrNewPolSplitCV_BOT = new ContentView();
            iM_DevM.CrNewPolSplitCV_BOT = new iM_NewPolicyLogicCV { BindingContext = this };

        });
        public Command LogicalDeviceTap_Command => new Command((param) =>
        {
            if (param.GetType() == typeof(iM_TaskModel))
            {
                var btn = (iM_TaskModel)param;
                btn.showProps = !btn.showProps;
            }
            else
            {
                var btn = (Light)param;
                btn.showProps = !btn.showProps;
            }



            if (iM_DevM.rs_Expanded_logicals == false)
            {
                iM_DevM.rs_Expanded_logicals = !iM_DevM.rs_Expanded_logicals;
            }

        });

        public Command LogicalDevStart => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            switch (btn.Status)
            {
                case "stopped":
                    iMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.START);
                    break;
                case "paused":
                    iMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.RESUME);
                    break;
                default:
                    iMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.START);
                    break;
            }
        });


        public Command LogicalDevStop => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            iMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.STOP);
        });

        public Command LogicalDevPause => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            iMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.PAUSE);
        });

        public Command LogicalDevResume => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            iMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.RESUME);
        });
        public Command DelectedSelectedFromPolicy_Command => new Command(() =>
        {

            int count = iM_DevM.policyItemsList.Count;
            for (int i = 0; i < count; i++)
            {
                iM_DevM.policyItemsList.Remove(iM_DevM.policyItemsList.FirstOrDefault(s => s.selected == true));
            }
        });

        public Command ShowTasks_Command => new Command((param) =>
        {
            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;
            iM_DevM.TaskList = iMComs.ListTasks();
            for (int i = 0; i < iM_DevM.TaskList.Count; i++)
            {
                iMune_MQTT.GetInstance().SubscribeEncryptedTopic("task/status/" + iM_DevM.TaskList[i].Id);
            }
            iM_DevM.upperContent = new ContentView();
            iM_DevM.upperContent.Content = new iM_LogicalList { BindingContext = this };
        });

        public Command ShowCreateLogicalUI_Command => new Command(() =>
        {
            iM_DevM.upperContent = new ContentView();
            iM_DevM.upperContent.Content = new iM_CreateLogical();
        });
        public Command DeleteLogical_Command => new Command(() => { });
        public Command EditPolicy_Command => new Command(() =>
        {
            var somePolicy = iMComs.GetPolicyDetails(iM_DevM.currentPolicy.ID);
            iM_DevM.currentLightValues = new LightValues();
            somePolicy.ParameterChanged += iM_DevM.NewPolicy_ParameterChanged;

            iM_DevM.policyItemsList.Clear();
            iM_DevM.upperContent = new iM_CreateNewPolicyCV();
            somePolicy.PolicyTypeChangeable = false;
            iM_DevM.newPolicy = somePolicy;
            iM_DevM.NewPolicy_ParameterChanged(somePolicy, EventArgs.Empty);




            if (iM_DevM.sensorList.Count == 0)
            {
                RefreshSensors();
            }
            if (iM_DevM.lightList.Count == 0)
            {
                RefreshLights();
            }

            for (int i = 0; i < somePolicy.lightIDs.Length; i++)
            {
                var light = iM_DevM.lightList.Where(w => w.ID == somePolicy.lightIDs[i]).FirstOrDefault();

                if (light != null)
                {
                    iM_DevM.policyItemsList.Add(light);
                }

            }

            for (int i = 0; i < somePolicy.sensorIDs.Length; i++)
            {
                var sensorSplit = somePolicy.sensorIDs[i].Split('_');
                if (sensorSplit.Length > 1)
                {
                    var id = sensorSplit[0];
                    var trigger = sensorSplit[1];

                    var sensor = iM_DevM.sensorList.Where(w => ((Sensor)w).ID == id).FirstOrDefault();
                    if (sensor != null)
                    {
                        var availableTriggers = ((Sensor)sensor).triggerEvents;

                        for (int y = 0; y < availableTriggers.Count; y++)
                        {
                            var usedTrigger = availableTriggers[y].Trigger.Where(w => w.value == Convert.ToInt32(trigger)).FirstOrDefault();

                            if (usedTrigger != null)
                            {
                                usedTrigger.used = true;
                            }
                        }
                        iM_DevM.policyItemsList.Add(sensor);
                    }
                }
            }
        });

        public Command ShowLogicalLogic_Command => new Command((param) =>
        {




            iM_DevM.currentNewLogicalSideTglBtn = (CustomToggleBtn)param;
            iM_DevM.CrNewLogicalSplitCV_BOT = new ContentView();
            iM_DevM.CrNewLogicalSplitCV_BOT.Content = new iM_CreateLogicalLogic();


        });

        public Command SelectUniverse_Command => new Command((param) =>
        {
            var uni = (UniverseList)param;
            if (iM_DevM.currentUniverse != null)
            {
                iM_DevM.currentUniverse.selected = false;
            }
            uni.selected = true;
            iM_DevM.currentUniverse = uni;
            iM_DevM.newLight.universe = uni.universe;
        });

        public Command SelectCh_Command => new Command((param) =>
        {
            switch (iM_DevM.newLight.type)
            {
                case LightType.PRIMITIVE:
                    var sender = param as ChSelModel;
                    sender.prim = !sender.prim;
                    break;
                case LightType.CCT:
                    var sender1 = param as ChSelModel;
                    switch (iM_DevM.currentDMXChColour)
                    {
                        case "warm":
                            sender1.cool = false;
                            sender1.warm = !sender1.warm;
                            break;
                        case "cool":
                            sender1.warm = false;
                            sender1.cool = !sender1.cool;
                            break;
                    }

                    break;

                case LightType.RGB:
                    var sender2 = param as ChSelModel;
                    switch (iM_DevM.currentDMXChColour)
                    {
                        case "red":
                            sender2.blue = false;
                            sender2.green = false;
                            sender2.red = !sender2.red;
                            break;
                        case "blue":
                            sender2.red = false;
                            sender2.green = false;
                            sender2.blue = !sender2.blue;
                            break;
                        case "green":
                            sender2.red = false;
                            sender2.blue = false;
                            sender2.green = !sender2.green;
                            break;
                    }

                    break;


                case LightType.RGBX:
                    var sender3 = param as ChSelModel;
                    switch (iM_DevM.currentDMXChColour)
                    {
                        case "red":
                            sender3.blue = false;
                            sender3.green = false;
                            sender3.xCh = false;
                            sender3.red = !sender3.red;
                            break;
                        case "blue":
                            sender3.red = false;
                            sender3.green = false;
                            sender3.xCh = false;
                            sender3.blue = !sender3.blue;
                            break;
                        case "green":
                            sender3.red = false;
                            sender3.blue = false;
                            sender3.xCh = false;
                            sender3.green = !sender3.green;
                            break;
                        case "xCh":
                            sender3.red = false;
                            sender3.blue = false;
                            sender3.green = false;
                            sender3.xCh = !sender3.xCh;
                            break;
                    }

                    break;
            }
        });

        public Command AddSelectedToPolicy_Command => new Command(() =>
        {

            if (iM_DevM.currentDeviceList != null)
            {
                for (int i = 0; i < iM_DevM.currentDeviceList.Count; i++)
                {
                    if (iM_DevM.currentDeviceList[i].selected)
                    {
                        if (!iM_DevM.policyItemsList.Contains(iM_DevM.currentDeviceList[i]))
                        {

                            iM_DevM.policyItemsList.Add(iM_DevM.currentDeviceList[i]);
                        }
                    }
                }
            }
            else if (iM_DevM.currentSelectedLightGroup != null)
            {
                for (int i = 0; i < iM_DevM.currentSelectedLightGroup.groupLights.Count; i++)
                {
                    if (iM_DevM.currentSelectedLightGroup.groupLights[i].selected)
                    {
                        if (!iM_DevM.policyItemsList.Contains(iM_DevM.currentSelectedLightGroup.groupLights[i]))
                        {

                            iM_DevM.policyItemsList.Add(iM_DevM.currentSelectedLightGroup.groupLights[i]);
                        }
                    }
                }
            }
        });



        public Command AddPhaseToNewLogical_Command => new Command(() =>
        {
            if (iM_DevM.NewLogical.type == LogicalTypeEnum.TASK)
            {
                ((iM_TaskModel)iM_DevM.NewLogical).Phases.Add(new TaskPhase());
            }
        });

        public Command TriggerPolicy_Command => new Command((param) =>
        {
            var sender = param as iM_newPolicyM;

            iMComs.TriggerPolicy(sender.ID, sender.trigger ? 0 : 1);
            sender.trigger = !sender.trigger;

        });

        public Command ShowPanelCV_Command => new Command((param) =>
        {
            iM_DevM.createLPanel = new ContentView();
            switch (param)
            {
                case "showUni":
                    iM_DevM.createLPanel.Content = iM_DevM.selectUniCV;
                    break;
                case "showDmx":
                    switch (iM_DevM.newLight.type)
                    {
                        case LightType.PRIMITIVE:
                            iM_DevM.createLPanel.Content = iM_DevM.selectChPrimCV;
                            break;
                        case LightType.CCT:
                            iM_DevM.createLPanel.Content = iM_DevM.selectChCCTCV;
                            break;
                        case LightType.RGB:
                            iM_DevM.createLPanel.Content = iM_DevM.selectChRGBCV;
                            break;
                        case LightType.RGBX:
                            iM_DevM.createLPanel.Content = iM_DevM.selectChRGBXCV;
                            break;
                        default:
                            iM_DevM.createLPanel.Content = null;
                            break;
                    }
                    break;
                case "showGroups":
                    iM_DevM.createLPanel.Content = iM_DevM.lightGroupCV;
                    break;
            }
        });

        public Command ShowScenes_Command => new Command((param) =>
        {

            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;
            iM_DevM.SceneList = iMComs.ListScenes();
            for (int i = 0; i < iM_DevM.SceneList.Count; i++)
            {
                iMune_MQTT.GetInstance().SubscribeEncryptedTopic("scene/status/" + iM_DevM.SceneList[i].Id);
            }
            iM_DevM.upperContent = new ContentView();
            iM_DevM.upperContent.Content = new iM_LogicalList { BindingContext = this };

        });

        public Command ShowTASK_GROUPs_Command => new Command((param) =>
        {

            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;
            iM_DevM.TaskGroupList = iMComs.ListTaskGroups();
            for (int i = 0; i < iM_DevM.TaskGroupList.Count; i++)
            {

                iM_DevM.TaskGroupList[i].Status = iMComs.GetInitialTaskGroupStatus(iM_DevM.TaskGroupList[i].Id);
                iMune_MQTT.GetInstance().SubscribeEncryptedTopic("taskgroup/status/" + iM_DevM.TaskGroupList[i].Id);
            }
            iM_DevM.upperContent = new ContentView();
            iM_DevM.upperContent.Content = new iM_LogicalList { BindingContext = this };
        });

        public Command ChangeDuration_Command => new Command(async (param) =>
        {
            var sender = (Duration)param;
            DurationPop DurPop = new DurationPop();
            var vm = (DurationPopViewModel)DurPop.BindingContext;
            vm.Duration = sender;


            await PopupNavigation.Instance.PushAsync(DurPop);
        });

        public Command ShowLightsNewLogical_Command => new Command((param) =>
        {

            // iM_DevM.currentDeviceList = iM_DevM.lightList; 
            iM_DevM.currentLogicalDevSideTglBtn = (CustomToggleBtn)param;
            iM_DevM.CrNewLogicalSplitCV_UP = new ContentView();
            iM_DevM.CrNewLogicalSplitCV_UP.Content = new LightListCV();
        });

        public Command ShowGrLightsNewLogical_Command => new Command((param) =>
        {
            iM_DevM.currentLogicalDevSideTglBtn = (CustomToggleBtn)param;
            iM_DevM.CrNewLogicalSplitCV_UP = new ContentView();
            iM_DevM.CrNewLogicalSplitCV_UP.Content = new iM_GroupLightsCV();
        });
        public Command MoveItemToLogicalDown_Command => new Command(() =>
        {
            if (iM_DevM.NewLogical == null) iM_DevM.NewLogical = new iM_TaskModel();
            switch (iM_DevM.NewLogical.type)
            {
                case LogicalTypeEnum.TASK:
                    for (int i = 0; i < iM_DevM.currentDeviceList.Count; i++)
                    {
                        var item = iM_DevM.NewLogicalItemsList.Where(v => v.Light.ID == iM_DevM.currentDeviceList[i].ID).FirstOrDefault();

                        if (item == null && iM_DevM.currentDeviceList[i].selected)
                        {

                            iM_DevM.NewLogicalItemsList.Add(new NewLogicalItem
                            {
                                Type = LogicalItemEnum.LIGHT,
                                Light = (Light)iM_DevM.currentDeviceList[i]
                            }); ;
                        }
                    }
                    break;

                case LogicalTypeEnum.TASK_GROUP:
                    for (int i = 0; i < iM_DevM.TaskList.Count; i++)
                    {
                        var item = iM_DevM.NewLogicalItemsList.Where(v => v.Task.Id == iM_DevM.TaskList[i].Id).FirstOrDefault();

                        if (item == null && iM_DevM.TaskList[i].selected)
                        {

                            iM_DevM.NewLogicalItemsList.Add(new NewLogicalItem
                            {
                                Type = LogicalItemEnum.TASK,
                                Task = (iM_TaskModel)iM_DevM.TaskList[i]
                            }); ;
                        }
                    }
                    break;
                case LogicalTypeEnum.SCENE:
                    for (int i = 0; i < iM_DevM.currentDeviceList.Count; i++)
                    {
                        var item = iM_DevM.NewLogicalItemsList.Where(v => v.Light.ID == iM_DevM.currentDeviceList[i].ID).FirstOrDefault();

                        if (item == null && iM_DevM.currentDeviceList[i].selected)
                        {

                            iM_DevM.NewLogicalItemsList.Add(new NewLogicalItem
                            {
                                Type = LogicalItemEnum.LIGHT,
                                Light = (Light)iM_DevM.currentDeviceList[i]
                            });

                            ((iM_SceneModel)iM_DevM.NewLogical).Phases.Add(new ScenePhase
                            {
                                Name = ((Light)iM_DevM.currentDeviceList[i]).defName,
                                LightID = ((Light)iM_DevM.currentDeviceList[i]).ID,
                                LightType = ((Light)iM_DevM.currentDeviceList[i]).type
                            });
                        }
                    }
                    break;
            }
        });

        public Command ShowLogicalItems_Command => new Command((param) =>
        {
            iM_DevM.currentNewLogicalSideTglBtn = (CustomToggleBtn)param;
            iM_DevM.CrNewLogicalSplitCV_BOT = new ContentView();
            iM_DevM.CrNewLogicalSplitCV_BOT.Content = new iM_NewLogicalItemList();
        });

        public Command ChangeLightType_Command => new Command(async (param) =>
        {
            PickerSelectionPop pickerPop = new PickerSelectionPop();

            var vm = (PickerSelectionPopViewModel)pickerPop.BindingContext;
            vm.ObjectToChange = (TaskPhase)param;
            vm.WhatToChange = "Light Type";
            vm.ListOfItems = new ObservableCollection<string>(iM_DevM.l_typeList);

            await PopupNavigation.Instance.PushAsync(pickerPop);
        });

        public Command RemovePhase_Command => new Command((param) =>
        {
            var taskPhase = param as TaskPhase;
            ((iM_TaskModel)iM_DevM.NewLogical).Phases.Remove(taskPhase);
        });

        public Command CreateLogical_Command => new Command(() =>
        {
            iMComs.CreateLogical(iM_DevM.NewLogical, iM_DevM.NewLogicalItemsList);
            iM_DevM.NewLogical = null; 
        });

        public Command ShowTasksNewLogical_Command => new Command(() =>
        {
            iM_DevM.CrNewLogicalSplitCV_UP = new ContentView();
            iM_DevM.CrNewLogicalSplitCV_UP.Content = new iM_LogicalList();
        });

        public Command ChangeTransition_Command => new Command(async (param) =>
        {
            PickerSelectionPop pickerPop = new PickerSelectionPop();

            var vm = (PickerSelectionPopViewModel)pickerPop.BindingContext;
            vm.ObjectToChange = (TaskPhase)param;
            vm.WhatToChange = "Transition Style";
            vm.ListOfItems = new ObservableCollection<string>(Enum.GetValues(typeof(TransitionType)).Cast<TransitionType>().Select(v => v.ToString()).ToList());

            await PopupNavigation.Instance.PushAsync(pickerPop);
        });

        public Command ChangeScenePhaseTransition_Command => new Command(async (param) =>
        {
            PickerSelectionPop pickerPop = new PickerSelectionPop();

            var vm = (PickerSelectionPopViewModel)pickerPop.BindingContext;
            vm.ObjectToChange = (ScenePhase)param;
            vm.WhatToChange = "Transition Style";
            vm.ListOfItems = new ObservableCollection<string>(Enum.GetValues(typeof(TransitionType)).Cast<TransitionType>().Select(v => v.ToString()).ToList());

            await PopupNavigation.Instance.PushAsync(pickerPop);
        });


        public Command NewSceneDuration_Command => new Command(async () =>
        {
            //((iM_SceneModel)iM_DevM.NewLogical).

            //var sender = (Duration)param;
            DurationPop DurPop = new DurationPop();
            var vm = (DurationPopViewModel)DurPop.BindingContext;
            vm.Duration = ((iM_SceneModel)iM_DevM.NewLogical).Duration;
            // vm.SceneDuration = ((iM_SceneModel)iM_DevM.NewLogical).Duration;
            await PopupNavigation.Instance.PushAsync(DurPop);
        });

        public Command ShowSchedulers_Command => new Command((param) =>
        {


            iM_DevM.currentSideMenuToggleButton = (CustomToggleBtn)param;
            ScheduleInitiate();


            iM_DevM.upperContent = new ContentView();
            iM_DevM.upperContent.Content = new SceneCapture_RTCSetting();
        });

        private void ScheduleInitiate()
        {

            iM_DevM.actionLists = iMComs.GetActions();
            ScheduleObject.actionList = iM_DevM.actionLists;
            iM_DevM.ScheduleList = iMComs.GetSchedules();


            var mergedList = new ObservableCollection<Actions>();
            if (iM_DevM.ScheduleList != null)
            {
                for (int i = 0; i < iM_DevM.ScheduleList.Count; i++)
                {
                    var scheduleActions = iM_DevM.ScheduleList[i].actions;
                    for (int y = 0; y < scheduleActions.Count; y++)
                    {
                        if (scheduleActions[y].tasks != null)
                        {
                            for (int z = 0; z < scheduleActions[y].tasks.Count; z++)
                            {
                                var action = iM_DevM.actionLists.tasks.Where(w => w.id == scheduleActions[y].tasks[z].id).FirstOrDefault();
                                if (action != null)
                                {
                                    scheduleActions[y].tasks[z] = action;
                                }

                            }
                        }
                        if (scheduleActions[y].scenes != null)
                        {
                            for (int z = 0; z < scheduleActions[y].scenes.Count; z++)
                            {
                                var action = iM_DevM.actionLists.scenes.Where(w => w.id == scheduleActions[y].scenes[z].id).FirstOrDefault();
                                if (action != null)
                                {
                                    scheduleActions[y].scenes[z] = action;


                                }
                            }
                        }
                        for (int w = 0; w < scheduleActions[y].tasks.Count; w++)
                        {
                            scheduleActions[y].mergedList.Add(scheduleActions[y].tasks[w]);
                        }
                        for (int w = 0; w < scheduleActions[y].scenes.Count; w++)
                        {
                            scheduleActions[y].mergedList.Add(scheduleActions[y].scenes[w]);
                        }
                    }


                }

            }
            else
            {
                iM_DevM.ScheduleList = new ObservableCollection<SchedulesM>();
            }
        }

        public Command SelectedSchedule_Command => new Command((param) =>
        {
            if (iM_DevM.currentSchedule != null)
            {
                iM_DevM.currentSchedule.selected = false;
            }

            iM_DevM.currentSchedule = (SchedulesM)param;
            iM_DevM.currentSchedule.selected = true;
        });

        public Command EditName_Command => new Command((param) =>
        {
            var schedule = param as SchedulesM;
            schedule.editName = !schedule.editName;
        });


        public Command DeleteScheduler_Command => new Command(async (param) =>
        {


            var scheduler = (SchedulesM)param;

            bool answer = await Application.Current.MainPage.DisplayAlert("Are you sure?", "You are about to delete a Scheduler: \n" + scheduler.name, "Yes", "No");

            if (answer)
            {
                var result = iMComs.DeleteSchedule(scheduler.id); //Ssh.GetInstance().SendCommand("delete_schedule " + scheduler.id);

                if (result.Succesfull)
                {
                    iM_DevM.ScheduleList.Remove((SchedulesM)param);
                }
            }
        });
        public Command ChangedActionType_Command => new Command(async (param) =>
        {
            //var x = param;

            await PopupNavigation.Instance.PushAsync(new PopupPageActionPicker(iM_DevM.actionLists, (Actions)param));
        });

        public Command AddSource_Command => new Command((param) =>
        {

            if (iM_DevM.currentSchedule.actions != null)
            {
                for (int i = 0; i < iM_DevM.currentSchedule.actions.Count; i++)
                {
                    if (iM_DevM.currentSchedule.actions[i].control == (string)param)
                    {
                        if (iM_DevM.actionLists.mergedList.Count != 0)
                        {
                            if (iM_DevM.actionLists.tasks.Count > 0)
                            {
                                iM_DevM.currentSchedule.actions[i].mergedList.Add(new Actions { name = iM_DevM.actionLists.tasks[0].name, id = iM_DevM.actionLists.tasks[0].id, type = iM_DevM.actionLists.tasks[0].type });// new Actions { type = ControlType.TASK });
                            }
                            else if (iM_DevM.actionLists.scenes.Count > 0)
                            {
                                iM_DevM.currentSchedule.actions[i].mergedList.Add(new Actions { name = iM_DevM.actionLists.scenes[0].name, id = iM_DevM.actionLists.scenes[0].id, type = iM_DevM.actionLists.scenes[0].type });
                            }
                        }
                    }
                }
            }


        });

        public Command DeleteTimeActivationSetting_Command => new Command((param) =>
        {
            var sender = param as iM_StartEndTimesM;

            iM_DevM.currentActivDay.times.Remove(sender);
        });

        public Command AddTimesToActivationSetting_Command => new Command(() =>
        {

            iM_DevM.currentActivDay.times.Add(new iM_StartEndTimesM());
        });


        public Command DeleteRTCSource_Command => new Command((param) =>
        {

            var action = (Actions)param;



            switch (action.type)
            {
                case Enums.LogicalTypeEnum.TASK:
                    for (int i = 0; i < iM_DevM.currentSchedule.actions.Count; i++)
                    {
                        iM_DevM.currentSchedule.actions[i].tasks.Remove(action);
                    }
                    break;
                case Enums.LogicalTypeEnum.SCENE:
                    for (int i = 0; i < iM_DevM.currentSchedule.actions.Count; i++)
                    {
                        iM_DevM.currentSchedule.actions[i].scenes.Remove(action);
                    }
                    break;
            }
            for (int i = 0; i < iM_DevM.currentSchedule.actions.Count; i++)
            {
                iM_DevM.currentSchedule.actions[i].mergedList.Remove(action);
            }

            //currentSchedule.actions;


        });

        public Command EnableSheduler_Command => new Command(async (param) =>
        {

            var enab = (string)param;
            var ReturnMessage = iMComs.EnableSchedule(iM_DevM.currentSchedule.id, enab); //Ssh.GetInstance().SendCommand("set_schedule_enabled " + currentSchedule.id + " " + enab);

            if (ReturnMessage.Succesfull)
            {
                if (enab.Equals("1"))
                {
                    iM_DevM.currentSchedule.enabled = true;
                }
                else
                {
                    iM_DevM.currentSchedule.enabled = false;
                }
            }

        });

        public Command ExecuteScheduler_Command => new Command(async () =>
        {


            if (String.IsNullOrEmpty(iM_DevM.currentSchedule.id))
            {
                await App.Current.MainPage.DisplayAlert("Alert!!",
                    "Scheduler doesn't exist! Click \"SAVE CHANGES\" before attempting to use this function", "OK");
            }
            else
            {
                var ReturnMessage = iMComs.ExecuteSchedule(iM_DevM.currentSchedule.id); // Ssh.GetInstance().SendCommand("execute_schedule_now " + currentSchedule.id);
            }
        });

        public Command SaveScheduler_Command => new Command(async (param) =>
        {


            var responseString = "";


            if (String.IsNullOrEmpty(iM_DevM.currentSchedule.id))
            {
                var ReturnMessage = iMComs.CreateSchedule(iM_DevM.currentSchedule.name); // Ssh.GetInstance().SendCommand("create_schedule " + currentSchedule.name);

                if (ReturnMessage.Succesfull)
                {
                    iM_DevM.currentSchedule.id = ReturnMessage.Result.Split(':')[1];
                    iM_DevM.currentSchedule.enabled = true;
                    responseString = "Created Schedule: " + iM_DevM.currentSchedule.name + "\n";
                }
            }
            var messageToSend = "";

            var nameChanged = iMComs.SetScheduleName(iM_DevM.currentSchedule.id, iM_DevM.currentSchedule.name);// Ssh.GetInstance().SendCommand("set_schedule_name " + currentSchedule.id + " " + currentSchedule.name);

            for (int i = 0; i < iM_DevM.currentSchedule.actions.Count; i++)
            {
                iM_DevM.currentSchedule.actions[i].tasks = new ObservableCollection<Actions>();
                iM_DevM.currentSchedule.actions[i].scenes = new ObservableCollection<Actions>();
                for (int y = 0; y < iM_DevM.currentSchedule.actions[i].mergedList.Count; y++)
                {
                    switch (iM_DevM.currentSchedule.actions[i].mergedList[y].type)
                    {
                        case LogicalTypeEnum.TASK:
                            iM_DevM.currentSchedule.actions[i].tasks.Add(iM_DevM.currentSchedule.actions[i].mergedList[y]);
                            break;
                        case LogicalTypeEnum.SCENE:
                            iM_DevM.currentSchedule.actions[i].scenes.Add(iM_DevM.currentSchedule.actions[i].mergedList[y]);
                            break;
                    }
                }

                switch (iM_DevM.currentSchedule.actions[i].control)
                {
                    case "START":
                        if (iM_DevM.currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~start";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (iM_DevM.currentSchedule.actions[i].scenes.Count > 0)
                        {
                            messageToSend += " scene_control~start";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                    case "STOP":
                        if (iM_DevM.currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~stop";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (iM_DevM.currentSchedule.actions[i].scenes.Count > 0)
                        {
                            messageToSend += " scene_control~stop";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                    case "PAUSE":
                        if (iM_DevM.currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~pause";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (iM_DevM.currentSchedule.actions[i].scenes.Count > 0)
                        {
                            messageToSend += " scene_control~pause";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                    case "RESUME":
                        if (iM_DevM.currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " task_control~resume";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].tasks.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].tasks[y].id;
                            }
                        }

                        if (iM_DevM.currentSchedule.actions[i].tasks.Count > 0)
                        {
                            messageToSend += " scene_control~resume";

                            for (int y = 0; y < iM_DevM.currentSchedule.actions[i].scenes.Count; y++)
                            {
                                messageToSend += "~" + iM_DevM.currentSchedule.actions[i].scenes[y].id;
                            }
                        }
                        break;
                }

            }

            messageToSend = messageToSend.StartsWith("~") ? messageToSend.Substring(1) : messageToSend;

            var ReturnMessage2 = iMComs.SetScheduleAction(iM_DevM.currentSchedule.id, messageToSend); //Ssh.GetInstance().SendCommand("set_schedule_actions " + currentSchedule.id+ " "+ messageToSend);
            var ReturnMessage3 = iMComs.SetScheduleTime(iM_DevM.currentSchedule.id, iM_DevM.currentSchedule.scheduleTime.Time);//Ssh.GetInstance().SendCommand("set_schedule_time " + currentSchedule.id +" "+ currentSchedule.scheduleTime.Time.ToString(@"hh\:mm\:ss"));

            var days = "";

            for (int i = 0; i < iM_DevM.currentSchedule.scheduleTime.Days.Count; i++)
            {
                var day = iM_DevM.currentSchedule.scheduleTime.Days[i];
                if (day.use)
                {
                    days += " " + ((int)(day.DayName)).ToString();
                }
            }

            var ReturnMessage4 = iMComs.SetScheduleDays(iM_DevM.currentSchedule.id, days);//Ssh.GetInstance().SendCommand("set_schedule_days " + currentSchedule.id + days);
            //await PopupNavigation.Instance.PushAsync(new ValidationPopup("iMune Response", nameChanged +"\n"+ ReturnMessage2 + "\n" + ReturnMessage3 +"\n" + ReturnMessage4));

        });

        public Command CloseSchedulerPP_Command => new Command(async () =>
        {
            await PopupNavigation.Instance.PopAllAsync();
        });

        public Command SaveSchedulerPP_Command => new Command(async () =>
        {
            await PopupNavigation.Instance.PopAllAsync();
        });

        public Command AddScheduler_Command => new Command(() =>
        {

            var defaultList = new ObservableCollection<ScheduleActions>
            {
                new ScheduleActions{control = "START"},
                new ScheduleActions{control = "STOP"},
                new ScheduleActions{control = "PAUSE"},
                new ScheduleActions{control = "RESUME"},
            };
            iM_DevM.ScheduleList.Add(new SchedulesM { name = "No Name", scheduleTime = new ScheduleTime(), actions = defaultList });
        });
        #endregion
    }
}
