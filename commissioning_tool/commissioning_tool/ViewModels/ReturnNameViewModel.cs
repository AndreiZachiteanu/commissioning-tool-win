﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class ReturnNameViewModel : BaseViewModel
    {
        #region Properties
        public Command SetName_Command { get; internal set; }
        public string PlaceHolder { get; internal set; }
        #endregion
    }
}
