﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.ViewModels
{
    public class DurationPopViewModel : BaseViewModel
    {
        #region Properties
        public Duration Duration { get; set; }

        #endregion

        #region Commands
        public Command AddTime_Command => new Command((param) => 
        {
            switch (param)
            {
                case "Hours":
                    Duration.Hours += 1;
                break;
                case "Minutes":
                    Duration.Minutes += 1;
                    break;
                case "Seconds":
                    Duration.Seconds += 1;
                    break;
            }
        });

        public Command RemoveTime_Command => new Command((param) =>
        {
            
            switch (param)
            {
                case "Hours":
                    if(Duration.Hours > 0)Duration.Hours -= 1;
                    break;
                case "Minutes":
                    if (Duration.Minutes > 0) Duration.Minutes -= 1;
                    break;
                case "Seconds":
                    if (Duration.Seconds > 0) Duration.Seconds -= 1;
                    break;
            }
        });

     
        #endregion
    }
}
