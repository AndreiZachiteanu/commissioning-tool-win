﻿using commissioning_tool.Connections;
using commissioning_tool.ContentViews;
using commissioning_tool.ContentViews.HierarchyItems;
using commissioning_tool.Enums;
using commissioning_tool.Models;
using commissioning_tool.PhysicalDevices;
using commissioning_tool.Popups;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;
using Device = commissioning_tool.PhysicalDevices.Device;

namespace commissioning_tool.ViewModels
{
    public class iM_HierarchyViewModel : INotifyPropertyChanged
    {
        #region Fields
        private iMune_SSHComs IMComs = iMune_SSHComs.GetInstance();
        public UniverseList CurrentUniverse;
        #endregion

        #region Properties
        public Command Hier_SelectUniverse_Command { get; set; }
        public iM_LightGroup NewGroup { get; set; } = new iM_LightGroup();
        public bool LogicalTaskExpanded { get; set; }
        public Command SelectChCommand { get; set; }
        public iM_TaskModel Hier_CurrentLogicalTask { get; set; }
        public bool Hier_LogicalTaskExpanded { get; set; }
        public bool Hier_SensorPropsExpanded { get; set; }
        public Sensor Hier_CurrentSensor { get; set; }
        public ContentView Hier_CreatePanel { get; set; } = new ContentView();
        public string Hier_CreatePanelOp { get;  set; }
        public bool Hier_ShowLightGroup { get; set; }
        public iM_LightGroup Hier_CurrentLightGroup { get; set; }
        public iM_LightGroup SelectedGroup { get; set; } = new iM_LightGroup();
        public Hier_World World { get; set; } = new Hier_World();
        public Hierarchy CurrentMenuItem { get; set; }
        public Hierarchy tempHier { get; set; }
        public ControlMenuItemType CreateMenuItem { get; set; } = ControlMenuItemType.DEFAULT;
        public string CurrentDMXChColour { get; set; } = "warm";
        public Light NewLight { get; set; } = new Light();
        public List<string> Light_TypeList { get; set; } = Enum.GetValues(typeof(LightType)).Cast<LightType>().Select(v => v.ToString()).ToList();
        public HierarchyMenuItem HierType { get; set; }
        public bool EditMode { get; set; }
        public string HierItemType { get; set; }
        public string HierItemType2 { get; set; }
        public ContentView OptionsContentView { get; set; } = new ContentView();
        public string Hier_OptionsType { get; set; }
        public ObservableCollection<Hierarchy> HierarchyList { get; set; } = new ObservableCollection<Hierarchy>();
        public Light CurrentZoneLight { get; set; }
        public bool Hier_LightPropsExpanded { get; set; }
        public bool Hier_PolicyPropsExpanded { get; set; }
        public iM_newPolicyM Hier_CurrentPolicy { get; set; }
        public Command SelectChColourCommand { get; set; }
        public Command CreateLightGroup_Command { get; set; }
        public iM_newPolicyM NewPolicy { get; set; } = new iM_newPolicyM();
        public Command CreatePolicy_Command { get; set; }

        public PolicySequence SelectedPolicySeq { get; set; } = new PolicySequence();
        public Command SelectSequence_Command { get; set; }

        #endregion


        #region Constructor
        public iM_HierarchyViewModel()
        {
            HierarchyList.Add(World);


            ///TODO: Make this on app start and let MQTT handle the rest
            GetAll();
            SelectChCommand = new Command((param) => SelectChMethod(param as ChSelModel));
            SelectChColourCommand = new Command((param) => SelectChColourMethod(param as string));
            Hier_SelectUniverse_Command = new Command((param) => SelectUniverseMethod(param as UniverseList));
            CreateLightGroup_Command = new Command((param) => CreateNewGroupMethod(param as iM_LightGroup));
            CreatePolicy_Command = new Command((param) => CreateNewPolicyMethod(param as iM_newPolicyM));
            SelectSequence_Command = new Command((param) => SetSelectedSequence(param as PolicySequence));
        }



        #endregion

        #region Methods
        public void GetAll()
        {
            iM_StaticImuneItems.Instance.GetLights();
            iM_StaticImuneItems.Instance.GetSensors();
            iM_StaticImuneItems.Instance.GetLightGroups();
            iM_StaticImuneItems.Instance.GetPolicies();
            iM_StaticImuneItems.Instance.GetSchedules();
            iM_StaticImuneItems.Instance.GetTasks();
            iM_StaticImuneItems.Instance.GetTaskGroups();
            iM_StaticImuneItems.Instance.GetScenes();
        }
        #endregion

        #region Commands
        public Command ChangeCreationItem_Command => new Command((param) =>
        {
            CreateMenuItem = (ControlMenuItemType)param;
        });
        public Command MoveItemDown_Command => new Command((param) => 
        {
            var dev = param as Device;
            switch (CreateMenuItem)
            {
                case ControlMenuItemType.LIGHTGROUP:
                    if (dev.devType == Enums.DeviceType.light) NewGroup.groupLights.Add((Light)dev);
                    break;
                case ControlMenuItemType.POLICY:
                    if (dev.devType == Enums.DeviceType.light || dev.devType == Enums.DeviceType.sensor) NewPolicy.NewDevices.Add(dev);
                    break;
            }
        });



        public Command IdentifyLightDMX_Command => new Command(() =>
        {
            switch (NewLight.type)
            {
                case LightType.PRIMITIVE:
                    IMComs.IdentifyLightDMX(NewLight.universe.ToString(),
                            iM_StaticStatus.ch_instance.OutChannels.Where(x => x.prim).Select(x => x.channelNo).ToList());
                    break;
                case LightType.CCT:
                    var coolCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.cool).Select(x => x.channelNo).ToList();
                    var warmCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.warm).Select(x => x.channelNo).ToList();
                    coolCh.AddRange(warmCh);

                    IMComs.IdentifyLightDMX(NewLight.universe.ToString(), coolCh);

                    break;
                case LightType.RGB:
                    var redCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).Select(x => x.channelNo).ToList();
                    var greenCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).Select(x => x.channelNo).ToList();
                    var blueCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).Select(x => x.channelNo).ToList();
                    redCh.AddRange(greenCh);
                    redCh.AddRange(blueCh);
                    IMComs.IdentifyLightDMX(NewLight.universe.ToString(), redCh);

                    break;

                case LightType.RGBX:
                    var redxCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).Select(x => x.channelNo).ToList();
                    var greenxCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).Select(x => x.channelNo).ToList();
                    var bluexCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).Select(x => x.channelNo).ToList();
                    var whitexCh = iM_StaticStatus.ch_instance.OutChannels.Where(x => x.xCh).Select(x => x.channelNo).ToList();
                    redxCh.AddRange(greenxCh);
                    redxCh.AddRange(bluexCh);
                    redxCh.AddRange(whitexCh);
                    IMComs.IdentifyLightDMX(NewLight.universe.ToString(), redxCh);

                    break;
            }

        });
        public Command CreateLight_Command => new Command(() =>
        {
            var id = "";
            switch (((Light)NewLight).type)
            {
                case LightType.PRIMITIVE:
                    var result = IMComs.CreatePRIMLight(NewLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.prim).ToList(),
                                        SelectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList());
                    id = result.id;
                    if (result.Created)
                    {
                        var added = IMComs.AddZoneItem("Light", result.id, CurrentMenuItem.Id);
                        if (added)
                        {
                            var light = NewLight;
                            light.ID = id;
                            ((Zone)CurrentMenuItem).Lights.Add(light);
                        }
                    }
                    break;
                case LightType.CCT:
                    var result1 = IMComs.CreateCCTLight(NewLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.warm).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.cool).ToList(),
                                        SelectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList());
                    id = result1.id;
                    break;

                case LightType.RGB:
                    var result2 = IMComs.CreateRGBLight(NewLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).ToList(),
                                        SelectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList());
                    id = result2.id;
                    break;
                case LightType.RGBX:
                    var result3 = IMComs.CreateRGBXLight(NewLight,
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.red).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.green).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.blue).ToList(),
                                        iM_StaticStatus.ch_instance.OutChannels.Where(x => x.xCh).ToList(),
                                        SelectedGroup,
                                        iM_StaticStatus.ch_instance.Universe.Where(x => x.selected).ToList(),
                                        NewLight.xChannelName);
                    id = result3.id;
                    break;

            }

            if (SelectedGroup.id != null && !id.Equals(""))
            {
                IMComs.AddLightToGroup(SelectedGroup.id, id);
            }
        });

        public Command CloseExpander_Command => new Command((param) =>         
        {
            //var something = param;
            //something = false;
            switch ((ControlMenuItemType)param)
            {
                case ControlMenuItemType.LOGICALTASK:
                    LogicalTaskExpanded = false;
                    break;
            }
        });






        public Command Hier_ShowPanelCV_Command => new Command((param) =>
        {
            Hier_CreatePanel = new ContentView();
            
            Hier_CreatePanelOp = (string)param;

        });

        public Command HierOption_Command => new Command((param) =>
        {
            Hier_OptionsType = (string)param;
        });
        public Command HierLightGroupClicked_Command => new Command((param) =>
        {
            var lightGroup = param as iM_LightGroup;
            Hier_ShowLightGroup = true;
            Hier_CurrentLightGroup = lightGroup;
        });

        public Command ShowLightsForLightGroup_Command => new Command(() =>
        {

            HierItemType = "Lights LG";
        });
        public Command CloseLightGroupExpander_Command => new Command((param) =>
        {
            Hier_ShowLightGroup = false;
            

        });
        public Command LogicalDevStart => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            IMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.START);

        });
        public Command LogicalDevStop => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            IMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.STOP);
        });

        public Command LogicalDevPause => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            IMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.PAUSE);
        });

        public Command LogicalDevResume => new Command((param) =>
        {
            var btn = (iM_LogicalModel)param;
            IMComs.LogicalAction(btn.type, btn.Id, LogicalActEnum.RESUME);
        });
        public Command HierLogicalTaskClicked_Command => new Command((param) =>
        {
            Hier_CurrentLogicalTask = param as iM_TaskModel;
            LogicalTaskExpanded = true;
        });

        public Command ChangeSensorName_Command => new Command(() =>
        {
            //TODO;
        });
        public Command TriggerSensor_CommandOn => new Command(() =>
        {
            //TODO;
        });
        public Command TriggerSensor_CommandOff => new Command(() =>
        {
            //TODO;
           
        });

        public Command HierItemClicked_Command => new Command((param) =>
        {
            var type = param.GetType();
            //CurrentZoneLight = null;
            //HierItemType = "null";
            switch (type.Name)
            {
                case "Light":
                    CurrentZoneLight = ((Light)param);
                    Hier_LightPropsExpanded = true;
                    break;
                case "iM_LightGroup":

                    ((Zone)CurrentMenuItem).LightGroups.Add((iM_LightGroup)param);
                    break;
                case "iM_TaskModel":
                    ((Zone)CurrentMenuItem).Tasks.Add((iM_TaskModel)param);
                    break;
                case "iM_TaskGroupModel":
                    ((Zone)CurrentMenuItem).TaskGroups.Add((iM_TaskGroupModel)param);
                    break;
                case "iM_SceneModel":
                    ((Zone)CurrentMenuItem).Scenes.Add((iM_SceneModel)param);
                    break;
                case "Sensor":
                    Hier_SensorPropsExpanded = true;
                    Hier_CurrentSensor = (Sensor)param;
                    //((Zone)CurrentMenuItem).Sensors.Add((Sensor)param);
                    break;
                case "iM_newPolicyM":
                    Hier_PolicyPropsExpanded = true;
                    Hier_CurrentPolicy = (iM_newPolicyM)param;


                    //((Zone)CurrentMenuItem).Policies.Add((iM_newPolicyM)param);
                    break;
            }
        });

        public Command IdentifyDMX_Command => new Command((param) =>
        {
            IMComs.IdentifyDMX(Convert.ToInt32(((DMXAddress)param).oldValue), (CurrentZoneLight).universe);


        });
        public Command MoveItemToHierarchy_Command => new Command((param) =>
        {
            var type = param.GetType();
            switch (type.Name)
            {
                case "Light":
                    if (HierItemType == "Lights LG")
                    {
                        Hier_CurrentLightGroup.groupLights.Add((Light)param);
                    }
                    else
                    {
                        ((Zone)CurrentMenuItem).Lights.Add((Light)param);
                    }

                    break;
                case "iM_LightGroup":
                    ((Zone)CurrentMenuItem).LightGroups.Add((iM_LightGroup)param);
                    break;
                case "iM_TaskModel":
                    ((Zone)CurrentMenuItem).Tasks.Add((iM_TaskModel)param);
                    break;
                case "iM_TaskGroupModel":
                    ((Zone)CurrentMenuItem).TaskGroups.Add((iM_TaskGroupModel)param);
                    break;
                case "iM_SceneModel":
                    ((Zone)CurrentMenuItem).Scenes.Add((iM_SceneModel)param);
                    break;
                case "Sensor":
                    ((Zone)CurrentMenuItem).Sensors.Add((Sensor)param);
                    break;
                case "iM_newPolicyM":
                    ((Zone)CurrentMenuItem).Policies.Add((iM_newPolicyM)param);
                    break;
            }
        });
        public Command HierarchyItemSelected_Command => new Command((param) =>
        {
            var hierarchyItem = param as Hierarchy;

            CurrentMenuItem = hierarchyItem;
            HierType = hierarchyItem.HierType;
            for (int i = HierarchyList.Count - 1; i >= (int)CurrentMenuItem.HierType; i--)
            {
                HierarchyList.RemoveAt(i);
            }
            HierarchyList.Add(CurrentMenuItem);
        });
        public Command OpenCreateHierarchyPopup_Command => new Command(async () =>
        {
            ReturnNamePopup pop = new ReturnNamePopup();
            var vm = pop.BindingContext as ReturnNameViewModel;
            vm.Title = "Create new " + HierItemType;
            vm.SetName_Command = CreateNewHierarchy_Command;
            await PopupNavigation.Instance.PushAsync(pop);
        });

        public Command CreateNewHierarchy_Command => new Command(async (param) =>
        {
            if (CurrentMenuItem.HierChildType == HierarchyMenuItem.ESTATE)
            {
                var result = IMComs.CreateHierarchy((string)param, HierarchyMenuItem.ESTATE);
                if (result.Created)
                {
                    switch (CurrentMenuItem.HierChildType)
                    {
                        case HierarchyMenuItem.ESTATE:

                            var newEstate = new Hier_Estates
                            {
                                Id = result.Id,
                                Name = (string)param,
                                ItemList = new HierarchyItemList { ContainerName = "Campuses" }
                            };

                            ((Hier_World)CurrentMenuItem).ItemList.ItemList.Add(newEstate);
                            CurrentMenuItem = newEstate;
                            break;
                        case HierarchyMenuItem.BUILDING:
                            var newBuilding = new Building
                            {
                                Id = result.Id,
                                Name = (string)param,
                                ItemList = new HierarchyItemList { ContainerName = "Floors" }
                            };

                            ((Campus)CurrentMenuItem).ItemList.ItemList.Add(newBuilding);
                            CurrentMenuItem = newBuilding;

                            break;
                        case HierarchyMenuItem.FLOOR:
                            var newFloor = new Floor
                            {
                                Id = result.Id,
                                Name = (string)param,
                                ItemList = new HierarchyItemList { ContainerName = "Zones" }
                            };
                            ((Building)CurrentMenuItem).ItemList.ItemList.Add(newFloor);
                            CurrentMenuItem = newFloor;
                            break;
                        case HierarchyMenuItem.ZONE:
                            var newZone = new Zone
                            {
                                Id = result.Id,
                                Name = (string)param
                            };
                            ((Floor)CurrentMenuItem).ItemList.ItemList.Add(newZone);
                            CurrentMenuItem = newZone;
                            break;
                        case HierarchyMenuItem.CAMPUS:
                            var newCampus = new Campus
                            {
                                Id = result.Id,
                                Name = (string)param,
                                ItemList = new HierarchyItemList { ContainerName = "Buildings" }
                            };
                            ((Hier_Estates)CurrentMenuItem).ItemList.ItemList.Add(newCampus);
                            CurrentMenuItem = newCampus;
                            break;
                    }
                }
            }
            else
            {
                var result = IMComs.CreateHierarchy((string)param, CurrentMenuItem.HierChildType, CurrentMenuItem.Id);
                switch (CurrentMenuItem.HierChildType)
                {
                    case HierarchyMenuItem.ESTATE:
                        var newEstate = new Hier_Estates
                        {
                            Id = result.Id,
                            Name = (string)param,
                            ItemList = new HierarchyItemList { ContainerName = "Campuses" }
                        };

                        ((Hier_World)CurrentMenuItem).ItemList.ItemList.Add(newEstate);
                        CurrentMenuItem = newEstate;
                        break;
                    case HierarchyMenuItem.BUILDING:
                        var newBuilding = new Building
                        {
                            Id = result.Id,
                            Name = (string)param,
                            ItemList = new HierarchyItemList { ContainerName = "Floors" }
                        };

                        ((Campus)CurrentMenuItem).ItemList.ItemList.Add(newBuilding);
                        CurrentMenuItem = newBuilding;

                        break;
                    case HierarchyMenuItem.FLOOR:
                        var newFloor = new Floor
                        {
                            Id = result.Id,
                            Name = (string)param,
                            ItemList = new HierarchyItemList { ContainerName = "Zones" }
                        };
                        ((Building)CurrentMenuItem).ItemList.ItemList.Add(newFloor);
                        CurrentMenuItem = newFloor;
                        break;
                    case HierarchyMenuItem.ZONE:
                        var newZone = new Zone
                        {
                            Id = result.Id,
                            Name = (string)param
                        };
                        ((Floor)CurrentMenuItem).ItemList.ItemList.Add(newZone);
                        CurrentMenuItem = newZone;
                        break;
                    case HierarchyMenuItem.CAMPUS:
                        var newCampus = new Campus
                        {
                            Id = result.Id,
                            Name = (string)param,
                            ItemList = new HierarchyItemList { ContainerName = "Buildings" }
                        };
                        ((Hier_Estates)CurrentMenuItem).ItemList.ItemList.Add(newCampus);
                        CurrentMenuItem = newCampus;
                        break;
                }
            }
            HierarchyList.Add(CurrentMenuItem);

            try
            {
                await PopupNavigation.Instance.PopAllAsync();

            }
            catch (Exception ex)
            {

            }
        });



        public Command EditModePopup_Command => new Command(() =>
        {
            EditMode = !EditMode;
        });

        public Command EditHierName_Command => new Command(async (param) =>
        {
            tempHier = param as Hierarchy;
            ReturnNamePopup pop = new ReturnNamePopup();
            var vm = pop.BindingContext as ReturnNameViewModel;
            vm.Title = "Change Hierarchy Name" + HierItemType;
            vm.PlaceHolder = tempHier.Name;
            vm.SetName_Command = ChangeHierName_Command;
            await PopupNavigation.Instance.PushAsync(pop);

        });

        public Command ChangeHierName_Command => new Command((param) =>
        {
            if (IMComs.ChangeHierName((string)param, tempHier.HierType, tempHier.Id))
            {
                tempHier.Name = (string)param;
            }

        });

        public Command DeleteHierarchy_Command => new Command(async (param) =>
        {
            var sender = param as Hierarchy;
            switch (sender.HierType)
            {
                case HierarchyMenuItem.ESTATE:
                    var result = await App.Current.MainPage.DisplayAlert("Deleting Estate Alert", "Are you sure you want to delete " + sender.Name, "Yes", "No");
                    if (result)
                    {
                        if (IMComs.DeleteHierarchyEntry(sender.HierType, sender.Id))
                        {
                            ((Hier_World)CurrentMenuItem).ItemList.ItemList.Remove(sender);
                        }
                    }
                    break;
                case HierarchyMenuItem.ZONE:
                    //TODO;
                    break;
                default:
                    var result2 = await App.Current.MainPage.DisplayAlert("Removing Hierarchy Alert", "Are you sure you want to remove " + sender.Name + " from " + sender.Parent.Name, "Yes", "No");
                    if (result2)
                    {
                        if (IMComs.RemoveHierarchyItem(sender.Id, sender.Parent.Id, sender.HierType, sender.Parent.HierType))
                        {

                            CurrentMenuItem.ItemList.ItemList.Remove(sender);
                        }
                    }
                    break;
            }
        });
        public Command HierarchyOption_Command => new Command(() => { });


        public Command HierarchyItemList_Command => new Command((param) =>
        {
            var light = CurrentZoneLight;
            CurrentZoneLight = null;
            try
            {
                HierItemType = param as string;
                HierItemType2 = param as string;
                //Debug.WriteLine(HierItemType);
            }
            catch (Exception ex)
            {

            }

        });

        #region Methods
        private void CreateNewPolicyMethod(iM_newPolicyM iM_newPolicyM)
        {
            throw new NotImplementedException();
        }
        public void CreateNewGroupMethod(iM_LightGroup param)
        {
            var group = param;
            var result = IMComs.CreateGroup(group.groupName);
            if (result.Create)
            {
                group.id = result.ID;
                for (int i = 0; i < group.groupLights.Count; i++)
                {
                    var added = IMComs.AddLightToGroup(result.ID, group.groupLights[i].ID);
                }
                group.NumberOfLights = group.groupLights.Count.ToString();
                var addedToHier = IMComs.AddZoneItem("LightGroup", group.id, ((Zone)CurrentMenuItem).Id);
                if (addedToHier)
                {
                    ((Zone)CurrentMenuItem).LightGroups.Add(group);
                }
            }


        }

        public void SelectChColourMethod(string param)
        {
            CurrentDMXChColour = (string)param;
        }

        public void SelectChMethod(ChSelModel param)
        {

            switch (NewLight.type)
            {


                case LightType.PRIMITIVE:

                    //ClearTheRest("prim");


                    var sender = param as ChSelModel;
                    sender.prim = !sender.prim;
                    break;
                case LightType.CCT:
                    var sender1 = param as ChSelModel;
                    switch (CurrentDMXChColour)
                    {
                        case "warm":
                            sender1.cool = false;
                            sender1.warm = !sender1.warm;
                            break;
                        case "cool":
                            sender1.warm = false;
                            sender1.cool = !sender1.cool;
                            break;
                    }

                    break;

                case LightType.RGB:
                    var sender2 = param as ChSelModel;
                    switch (CurrentDMXChColour)
                    {
                        case "red":
                            sender2.blue = false;
                            sender2.green = false;
                            sender2.red = !sender2.red;
                            break;
                        case "blue":
                            sender2.red = false;
                            sender2.green = false;
                            sender2.blue = !sender2.blue;
                            break;
                        case "green":
                            sender2.red = false;
                            sender2.blue = false;
                            sender2.green = !sender2.green;
                            break;
                    }

                    break;


                case LightType.RGBX:
                    var sender3 = param as ChSelModel;
                    switch (CurrentDMXChColour)
                    {
                        case "red":
                            sender3.blue = false;
                            sender3.green = false;
                            sender3.xCh = false;
                            sender3.red = !sender3.red;
                            break;
                        case "blue":
                            sender3.red = false;
                            sender3.green = false;
                            sender3.xCh = false;
                            sender3.blue = !sender3.blue;
                            break;
                        case "green":
                            sender3.red = false;
                            sender3.blue = false;
                            sender3.xCh = false;
                            sender3.green = !sender3.green;
                            break;
                        case "xCh":
                            sender3.red = false;
                            sender3.blue = false;
                            sender3.green = false;
                            sender3.xCh = !sender3.xCh;
                            break;
                    }

                    break;
            }
        }

        public void SelectUniverseMethod(UniverseList param)
        {
            var uni = param;
            if (CurrentUniverse != null)
            {
                CurrentUniverse.selected = false;
            }
            uni.selected = true;
            CurrentUniverse = uni;
            NewLight.universe = uni.universe;
        }

        private void SetSelectedSequence(PolicySequence policySequence)
        {
            SelectedPolicySeq = new PolicySequence();
            SelectedPolicySeq = policySequence;
            SelectedPolicySeq.Selected = true;
        }


        #endregion





        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {

            var changed = PropertyChanged;
            if (propertyName == "Hier_OptionsType" || propertyName == "HierItemType")
            {
                Debug.WriteLine("Hier Option " + Hier_OptionsType + " - Hier Item " + HierItemType);
            }

            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
