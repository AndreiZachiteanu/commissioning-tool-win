﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Enums
{
    public enum PolicySequenceType
    {
        OnSequence,
        OffSequence,
        InterimSequence,
        PolicyParameter,
        PolicyActivation
    }
}
