﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Enums
{
    public enum ControlMenuItemType
    {
        LIGHT,
        LIGHTGROUP,
        SCHEDULE,
        SENSOR,
        POLICY,
        LOGICALTASK,
        LOGICALTASKGROUP,
        LOGICALSCENE,
        DEFAULT
    }
}
