﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Enums
{
    public enum PolicyType
    {
        TIMER,
        SWITCHED,
        TOGGLE,
        PRESENCE,
        ABSENCE,
        SCENESELECT,
        STEPDIM,
        RAISEDIM,
        DLH,
        EMERGENCY,
        FOLLOWME
    }
}
