﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Enums
{
    public enum ThemeEnum
    {
        BlueWhite,
        Dark,
        Medium,
        Small,
        Large
    }
}
