﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Enums
{
    public enum HierarchyMenuItem
    {
        WORLD = 0,
        ESTATE = 1,
        CAMPUS = 2,
        BUILDING = 3,
        FLOOR = 4,
        ZONE = 5,
        DEVICES = 6
    }
}
