﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Enums
{
    public enum LightType
    {
        PRIMITIVE,
        CCT,
        RGB,
        RGBX
        
    }
}
