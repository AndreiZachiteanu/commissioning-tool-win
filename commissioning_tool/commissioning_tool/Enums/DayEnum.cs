﻿using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Enums
{
    public enum DayEnum
    {
        MONDAY = 1,
        TUESDAY = 2,
        WEDNESDAY = 3,
        THURSDAY = 4,
        FRIDAY = 5,
        SATURDAY = 6,
        SUNDAY = 7
    }
}
