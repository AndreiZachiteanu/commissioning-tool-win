﻿using commissioning_tool.Enums;
using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SequenceButton : Frame
    {
        public SequenceButton()
        {
            InitializeComponent();
            var type = ThisSequence.Type;
            var selectedPolType = SelectedPolicyType;

            ButtonIsEnabled = iM_StaticStatus.CheckIfEnabled(type, selectedPolType);
            if (ButtonIsEnabled && SelectedPolicySeq != null)
            {
                
                SelectedPolicySeq.Selected = true;
            }
            //ToggledColor = (Color)((ResourceDictionary)Application.Current.Resources.MergedDictionaries.ToList()[0].MergedDictionaries.ToList()[0])["Color_bk08"];
            // NotToggledColor = (Color)((ResourceDictionary)Application.Current.Resources.MergedDictionaries.ToList()[0].MergedDictionaries.ToList()[0])["Color_bk01"];
        }



        #region Fields
        //public static Color ToggledColor;
        //public static Color NotToggledColor;
        #endregion

        #region Properties


        #endregion

        #region Methods

        /*
        private static void SetThisSequence(BindableObject bindable, object oldValue, object newValue)
        {
            //ThisSequence = (PolicySequence)newValue;
            bindable.SetValue(ThisSequenceProperty, new PolicySequence)
        }
        */
        private static void ToggledChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var thisSeq = bindable.GetValue(ThisSequenceProperty) as PolicySequence;
            var selSeq = bindable.GetValue(SelectedPolicySeqProperty) as PolicySequence;
            if (selSeq != null)
            {
                if (thisSeq.Type.ToString() == selSeq.Type.ToString())
                {
                    bindable.SetValue(Background_ColorProperty, (Color)newValue);
                }
            }
        }
        private static void SelectedSequenceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var thisSequence = bindable.GetValue(ThisSequenceProperty) as PolicySequence;
            if (newValue != null)
            {
                if (thisSequence.Type.ToString() == ((PolicySequence)newValue).Type.ToString())
                {
                    var color = (Color)bindable.GetValue(ToggledColorProperty);
                    bindable.SetValue(Background_ColorProperty, color);
                    thisSequence.Selected = true;
                    bindable.SetValue(SelectedPolicySeqProperty, thisSequence);
                }
                else
                {
                    var color = (Color)bindable.GetValue(NotToggledColorProperty);
                    bindable.SetValue(Background_ColorProperty, color);
                }
            }
        }


        private static void CheckIfEnabled(BindableObject bindable, object oldValue, object newValue)
        {

            var thisSeq = bindable.GetValue(ThisSequenceProperty) as PolicySequence;
            var type = thisSeq.Type;

            var selectedType = (PolicyType)bindable.GetValue(SelectedPolicyTypeProperty);

            bool isEnabled = iM_StaticStatus.CheckIfEnabled(type, selectedType);
            bindable.SetValue(ButtonIsEnabledProperty, isEnabled);
            if (!isEnabled)
            {
                var thisSequence = bindable.GetValue(ThisSequenceProperty) as PolicySequence;
                thisSequence.Used = isEnabled;
                thisSequence.Selected = isEnabled;
                bindable.SetValue(ThisSequenceProperty, thisSequence);
            }
            var selectedSequence = bindable.GetValue(SelectedPolicySeqProperty) as PolicySequence;
            var selSeqType = selectedSequence.Type;
            if(type == selSeqType && !isEnabled)
            {
                selectedSequence.Selected = false;
                bindable.SetValue(Background_ColorProperty, (Color)bindable.GetValue(NotToggledColorProperty));
                bindable.SetValue(SelectedPolicySeqProperty, selectedSequence);
            }
        }


        private static void SetPolicyType(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.SetValue(ThisSequenceProperty, new PolicySequence { Type = (PolicySequenceType)newValue }) ;
        }

        #endregion

        #region BindableProperties

        public static readonly BindableProperty ThisSequenceProperty = BindableProperty.Create(
                                    propertyName: nameof(ThisSequence),
                                    returnType: typeof(PolicySequence),
                                    declaringType: typeof(SequenceButton),
                                    defaultValue: new PolicySequence(),
                                    defaultBindingMode: BindingMode.TwoWay
    );

        public PolicySequence ThisSequence
        {

            get => (PolicySequence)GetValue(ThisSequenceProperty);
            set => SetValue(ThisSequenceProperty, value);
        }


        public static readonly BindableProperty Background_ColorProperty = BindableProperty.Create(
                                    propertyName: nameof(Background_Color),
                                    returnType: typeof(Color),
                                    declaringType: typeof(SequenceButton),
                                    defaultValue: null,
                                    defaultBindingMode: BindingMode.TwoWay);


        public Color Background_Color
        {
            get => (Color)GetValue(Background_ColorProperty);
            set => SetValue(Background_ColorProperty, value);
        }

        public static readonly BindableProperty ToggledColorProperty = BindableProperty.Create(
                            propertyName: nameof(ToggledColor),
                            returnType: typeof(Color),
                            declaringType: typeof(SequenceButton),
                            defaultValue: null,
                            defaultBindingMode: BindingMode.TwoWay,
                            propertyChanged:ToggledChanged);



        

        public Color ToggledColor
        {
            get => (Color)GetValue(ToggledColorProperty);
            set => SetValue(ToggledColorProperty, value);
        }

        public static readonly BindableProperty NotToggledColorProperty = BindableProperty.Create(
                            propertyName: nameof(NotToggledColor),
                            returnType: typeof(Color),
                            declaringType: typeof(SequenceButton),
                            defaultValue: null,
                            defaultBindingMode: BindingMode.TwoWay,
                            propertyChanged:NotToggledChanged);

        private static void NotToggledChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var thisSeq = bindable.GetValue(ThisSequenceProperty) as PolicySequence;
            var selSeq = bindable.GetValue(SelectedPolicySeqProperty) as PolicySequence;
            if (selSeq != null)
            {
                if (thisSeq.Type.ToString() != selSeq.Type.ToString())
                {
                    bindable.SetValue(Background_ColorProperty, (Color)newValue);
                }
            }
        }

        public Color NotToggledColor
        {
            get => (Color)GetValue(NotToggledColorProperty);
            set => SetValue(NotToggledColorProperty, value);
        }


        public static readonly BindableProperty TextProperty = BindableProperty.Create(
                                    propertyName: nameof(Text),
                                    returnType: typeof(string),
                                    declaringType: typeof(SequenceButton),
                                    defaultValue: null,
                                    defaultBindingMode: BindingMode.TwoWay);


        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public static readonly BindableProperty SelectedPolicySeqProperty = BindableProperty.Create(
                                    propertyName: nameof(SelectedPolicySeq),
                                    returnType: typeof(PolicySequence),
                                    declaringType: typeof(SequenceButton),
                                    defaultValue: null,
                                    defaultBindingMode: BindingMode.TwoWay,
                                    propertyChanged: SelectedSequenceChanged);


        public PolicySequence SelectedPolicySeq
        {
            get => (PolicySequence)GetValue(SelectedPolicySeqProperty);
            set => SetValue(SelectedPolicySeqProperty, value);
        }


        public static readonly BindableProperty SelectSequence_CommandProperty = BindableProperty.Create(
                                                propertyName: nameof(SelectSequence_Command),
                                                returnType: typeof(Command),
                                                declaringType: typeof(SequenceButton),
                                                defaultValue: null,
                                                defaultBindingMode: BindingMode.TwoWay
            );
        public Command SelectSequence_Command
        {
            get => (Command)GetValue(SelectSequence_CommandProperty);
            set => SetValue(SelectSequence_CommandProperty, value);
        }

        public static readonly BindableProperty SelectedPolicyTypeProperty = BindableProperty.Create(
                                    propertyName: nameof(SelectedPolicyType),
                                    returnType: typeof(PolicyType),
                                    declaringType: typeof(SequenceButton),
                                    defaultValue: null,
                                    defaultBindingMode: BindingMode.TwoWay,
                                    propertyChanged:CheckIfEnabled);



        public PolicyType SelectedPolicyType
        {
            get => (PolicyType)GetValue(SelectedPolicyTypeProperty);
            set => SetValue(SelectedPolicyTypeProperty, value);
        }

        public static readonly BindableProperty ThisPolicyTypeProperty = BindableProperty.Create(
                            propertyName: nameof(ThisPolicyType),
                            returnType: typeof(PolicySequenceType),
                            declaringType: typeof(SequenceButton),
                            defaultValue: null,
                            defaultBindingMode: BindingMode.TwoWay,
                            propertyChanged: SetPolicyType);


        public PolicySequenceType ThisPolicyType
        {
            get => (PolicySequenceType)GetValue(ThisPolicyTypeProperty);
            set => SetValue(ThisPolicyTypeProperty, value);
        }
        public static readonly BindableProperty ButtonIsEnabledProperty = BindableProperty.Create(
                propertyName: nameof(ButtonIsEnabled),
                returnType: typeof(bool),
                declaringType: typeof(SequenceButton),
                defaultValue: false,
                defaultBindingMode: BindingMode.TwoWay);
        public bool ButtonIsEnabled
        {
            get => (bool)GetValue(ButtonIsEnabledProperty);
            set => SetValue(ButtonIsEnabledProperty, value);
        }
        #endregion
    }
}