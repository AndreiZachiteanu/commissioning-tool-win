﻿using commissioning_tool.Enums;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.CustomControls
{
    public class DirectLightSKCanvas : SKCanvasView, INotifyPropertyChanged
    {
        #region Attributes
        private double height;
        private double width;
        #endregion

        #region Properties
        public static readonly BindableProperty TextProperty = BindableProperty.Create(
                                                    propertyName: nameof(Text),
                                                    returnType: typeof(string),
                                                    declaringType: typeof(DirectLightSKCanvas),
                                                    defaultValue: null,
                                                    defaultBindingMode: BindingMode.TwoWay,
                                                    propertyChanged: PropertyChanged_SK
                                                );

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public static readonly BindableProperty LightTypeProperty = BindableProperty.Create(
                                            propertyName: nameof(LightType),
                                            returnType: typeof(LightType),
                                            declaringType: typeof(DirectLightSKCanvas),
                                            defaultValue: null,
                                            defaultBindingMode: BindingMode.TwoWay                                            
                                        );

        public LightType LightType
        {
            get => (LightType)GetValue(LightTypeProperty);
            set => SetValue(LightTypeProperty, value);
        }



        public static readonly BindableProperty RedLevelProperty = BindableProperty.Create(
                                                            propertyName: nameof(RedLevel),
                                                            returnType: typeof(int),
                                                            declaringType: typeof(DirectLightSKCanvas),
                                                            defaultValue: null,
                                                            defaultBindingMode: BindingMode.TwoWay,
                                                            propertyChanged: PropertyChanged_SK
                                                        );

        public int RedLevel
        {
            get => (int)GetValue(RedLevelProperty);
            set => SetValue(RedLevelProperty, value);
        }


        public static readonly BindableProperty GreenLevelProperty = BindableProperty.Create(
                                                    propertyName: nameof(GreenLevel),
                                                    returnType: typeof(int),
                                                    declaringType: typeof(DirectLightSKCanvas),
                                                    defaultValue: null,
                                                    defaultBindingMode: BindingMode.TwoWay,
                                                    propertyChanged: PropertyChanged_SK
                                                );

        public int GreenLevel
        {
            get => (int)GetValue(GreenLevelProperty);
            set => SetValue(GreenLevelProperty, value);
        }

        public static readonly BindableProperty BlueLevelProperty = BindableProperty.Create(
                                            propertyName: nameof(BlueLevel),
                                            returnType: typeof(int),
                                            declaringType: typeof(DirectLightSKCanvas),
                                            defaultValue: null,
                                            defaultBindingMode: BindingMode.TwoWay,
                                            propertyChanged: PropertyChanged_SK
                                        );

        public int BlueLevel
        {
            get => (int)GetValue(BlueLevelProperty);
            set => SetValue(BlueLevelProperty, value);
        }


        public static readonly BindableProperty CCTValueProperty = BindableProperty.Create(
                                    propertyName: nameof(CCTValue),
                                    returnType: typeof(int),
                                    declaringType: typeof(DirectLightSKCanvas),
                                    defaultValue: null,
                                    defaultBindingMode: BindingMode.TwoWay,
                                    propertyChanged: PropertyChanged_SK
                                );

        public int CCTValue
        {
            get => (int)GetValue(CCTValueProperty);
            set => SetValue(CCTValueProperty, value);
        }
        public static readonly BindableProperty XValueProperty = BindableProperty.Create(
                            propertyName: nameof(XValue),
                            returnType: typeof(int),
                            declaringType: typeof(DirectLightSKCanvas),
                            defaultValue: null,
                            defaultBindingMode: BindingMode.TwoWay,
                            propertyChanged: PropertyChanged_SK
                        );

        public int XValue
        {
            get => (int)GetValue(XValueProperty);
            set => SetValue(XValueProperty, value);
        }

        public static readonly BindableProperty IDProperty = BindableProperty.Create(
                            propertyName: nameof(ID),
                            returnType: typeof(string),
                            declaringType: typeof(DirectLightSKCanvas),
                            defaultValue: null,
                            defaultBindingMode: BindingMode.TwoWay                            
                        );

        public string ID
        {
            get => (string)GetValue(IDProperty);
            set => SetValue(IDProperty, value);
        }

        public static readonly BindableProperty IntensityProperty = BindableProperty.Create(
                                                            propertyName: nameof(Intensity),
                                                            returnType: typeof(int),
                                                            declaringType: typeof(DirectLightSKCanvas),
                                                            defaultValue: null,
                                                            defaultBindingMode: BindingMode.TwoWay,
                                                            propertyChanged: PropertyChanged_SK
                                                        );

        private static void PropertyChanged_SK(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (DirectLightSKCanvas)bindable;
            control.InvalidateSurface();
        }

        public int Intensity
        {
            get => (int)GetValue(IntensityProperty);
            set => SetValue(IntensityProperty, value);
        }
        #endregion


        #region Constructors
        public DirectLightSKCanvas()
        {
            PaintSurface += Slider_PaintSurface;

        }

        #endregion


        #region Methods
        private void Slider_PaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;

            Color x = new Color();
            
            width = info.Width;
            height = info.Height;
            canvas.Clear();

            SKColor colour = new SKColor();
            switch (LightType)
            {
                case LightType.CCT:
                    colour = CreateCCTColour();
                    using (SKPaint paint = new SKPaint())
                    {
                        paint.Color = colour;
                        paint.Style = SKPaintStyle.Fill;


                        canvas.DrawRect(new SKRect(0, 0, (float)(width * ((double)Intensity / 100)), (float)height), paint);


                        if (!String.IsNullOrEmpty(Text))
                        {
                            using (SKPaint textPaint = new SKPaint())
                            {
                                textPaint.TextSize = 15;
                                textPaint.IsAntialias = true;
                                textPaint.FakeBoldText = true;
                                textPaint.Color = SKColors.Black;
                                textPaint.Style = SKPaintStyle.Stroke;
                                //textPaint.BlendMode = SKBlendMode.Darken;
                                canvas.DrawText(Text + "%", new SKPoint((float)width / 2 - 11, (float)((height / 2) + 8)), textPaint);
                            }

                            using (SKPaint textPaint = new SKPaint())
                            {
                                textPaint.TextSize = 14;
                                textPaint.IsAntialias = true;
                                textPaint.FakeBoldText = true;
                                textPaint.Color = SKColors.White;

                                //textPaint.BlendMode = SKBlendMode.Difference;
                                canvas.DrawText(Text + "%", new SKPoint((float)width / 2 - 10, (float)((height / 2) + 7)), textPaint);
                            }
                        }
                    }
                    break;
                case LightType.PRIMITIVE:

                    RedLevel = Intensity;
                    BlueLevel = Intensity;
                    GreenLevel = Intensity;
                    colour = CreateColour();
                    using (SKPaint paint = new SKPaint())
                    {
                        paint.Color = colour;
                        paint.Style = SKPaintStyle.Fill;


                        canvas.DrawRect(new SKRect(0, 0, (float)(width * ((double)Intensity / 255)), (float)height), paint);


                        if (!String.IsNullOrEmpty(Text))
                        {
                            using (SKPaint textPaint = new SKPaint())
                            {
                                textPaint.TextSize = 14;
                                textPaint.IsAntialias = true;
                                textPaint.FakeBoldText = true;
                                textPaint.Color = SKColors.Black;
                                textPaint.Style = SKPaintStyle.Stroke;
                                //textPaint.BlendMode = SKBlendMode.Darken;
                                canvas.DrawText(Text + "%", new SKPoint((float)width / 2 - 10, (float)((height / 2) + 8)), textPaint);
                            }

                            using (SKPaint textPaint = new SKPaint())
                            {
                                textPaint.TextSize = 14;
                                textPaint.IsAntialias = true;
                                textPaint.FakeBoldText = true;
                                textPaint.Color = SKColors.Red;

                                //textPaint.BlendMode = SKBlendMode.Difference;
                                canvas.DrawText(Text + "%", new SKPoint((float)width / 2 - 10, (float)((height / 2) + 7)), textPaint);
                            }
                        }
                    }
                    break;
                case LightType.RGB:
                    colour = CreateColour();
                    using (SKPaint paint = new SKPaint())
                    {
                        paint.Color = colour;
                        paint.Style = SKPaintStyle.Fill;


                        canvas.DrawRect(new SKRect(0, 0, (float)(width), (float)height), paint);

                        if (!String.IsNullOrEmpty(Text))
                        {
                            using (SKPaint textPaint = new SKPaint())
                            {
                                textPaint.TextSize = 14;
                                textPaint.IsAntialias = true;
                                textPaint.FakeBoldText = true;
                                textPaint.Color = SKColors.White;

                                textPaint.BlendMode = SKBlendMode.Difference;
                                canvas.DrawText(Text, new SKPoint((float)width / 2 - 10, (float)((height / 2) + 7)), textPaint);
                            }
                        }
                    }
                    break;


                case LightType.RGBX:
                    colour = CreateColour();
                    using (SKPaint paint = new SKPaint())
                    {
                        paint.Color = colour;
                        paint.Style = SKPaintStyle.Fill;


                        canvas.DrawRect(new SKRect(0, 0, (float)(width), (float)height / 2), paint);


                        var skColor = new SKColor((byte)255, (byte)255, (byte)255);
                        float tempH;
                        float tempS;
                        float tempV;
                        skColor.ToHsv(out tempH, out tempS, out tempV);

                        var xChannel = SKColor.FromHsv(tempH, tempS, (int)(XValue / 2.55));

                        SKPaint xChPaint = new SKPaint();
                        xChPaint.Color = xChannel;
                        xChPaint.Style = SKPaintStyle.Fill;

                        canvas.DrawRect(new SKRect(0, (float)height / 2, (float)(width * ((double)XValue)), (float)height), xChPaint);

                        if (!String.IsNullOrEmpty(Text))
                        {
                            using (SKPaint textPaint = new SKPaint())
                            {
                                textPaint.TextSize = 14;
                                textPaint.IsAntialias = true;
                                textPaint.FakeBoldText = true;
                                textPaint.StrokeWidth = 2;
                                textPaint.StrokeJoin = SKStrokeJoin.Bevel;
                                textPaint.Style = SKPaintStyle.Stroke;
                                textPaint.Color = SKColors.White;

                                textPaint.BlendMode = SKBlendMode.Difference;
                                canvas.DrawText(Text, new SKPoint(20, (float)((height / 2) + 7)), textPaint);
                            }
                        }

                    }



                    break;
            }



        }

        private SKColor CreateCCTColour()
        {
            var redAmount = 255 - CCTValue;
            var blueAmount = 255 - redAmount;



            var skColor = new SKColor((byte)((int)(redAmount)), (byte)127, (byte)((int)(blueAmount)));
            float tempH;
            float tempS;
            float tempV;
            skColor.ToHsv(out tempH, out tempS, out tempV);

            return SKColor.FromHsv(tempH, tempS, (int)(Intensity));
        }

        private SKColor CreateColour()
        {
            var skColor = new SKColor((byte)RedLevel, (byte)GreenLevel, (byte)BlueLevel);
            float tempH;
            float tempS;
            float tempV;
            skColor.ToHsv(out tempH, out tempS, out tempV);

            //var returnColor = SKColor.FromHsv(tempH, tempS, (int)(Intensity / 2.55));
            return skColor;// returnColor;
        }
        #endregion


    }
}
