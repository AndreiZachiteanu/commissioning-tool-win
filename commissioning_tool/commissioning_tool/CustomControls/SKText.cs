﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.CustomControls
{
    public class SKText : SKCanvasView, INotifyPropertyChanged
    {

        public static readonly BindableProperty TextColorProperty = BindableProperty.Create(
                                            propertyName: nameof(TextColor),
                                            returnType: typeof(Color),
                                            declaringType: typeof(SKText),
                                            defaultValue: null,
                                            defaultBindingMode: BindingMode.TwoWay,
                                            propertyChanged: PropertyChanged_SK
                                        );

        public Color TextColor
        {
            get => (Color)GetValue(TextColorProperty);
            set => SetValue(TextColorProperty, value);
        }

        public static readonly BindableProperty TextBackgroundColorProperty = BindableProperty.Create(
                                    propertyName: nameof(TextBackgroundColor),
                                    returnType: typeof(Color),
                                    declaringType: typeof(SKText),
                                    defaultValue: Color.Black,
                                    defaultBindingMode: BindingMode.TwoWay,
                                    propertyChanged: PropertyChanged_SK
                                );

        public Color TextBackgroundColor
        {
            get => (Color)GetValue(TextBackgroundColorProperty);
            set => SetValue(TextBackgroundColorProperty, value);
        }

        public static readonly BindableProperty TextSizeProperty = BindableProperty.Create(
                                    propertyName: nameof(TextSize),
                                    returnType: typeof(int),
                                    declaringType: typeof(SKText),
                                    defaultValue: null,
                                    defaultBindingMode: BindingMode.TwoWay,
                                    propertyChanged: PropertyChanged_SK
                                );

        public int TextSize
        {
            get => (int)GetValue(TextSizeProperty);
            set => SetValue(TextSizeProperty, value);
        }

        public static readonly BindableProperty TextProperty = BindableProperty.Create(
                            propertyName: nameof(Text),
                            returnType: typeof(string),
                            declaringType: typeof(SKText),
                            defaultValue: null,
                            defaultBindingMode: BindingMode.TwoWay,
                            propertyChanged: PropertyChanged_SK
                        );
        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }


        public static readonly BindableProperty PrefixTextProperty = BindableProperty.Create(
                    propertyName: nameof(PrefixText),
                    returnType: typeof(string),
                    declaringType: typeof(SKText),
                    defaultValue: null,
                    defaultBindingMode: BindingMode.TwoWay,
                    propertyChanged: PropertyChanged_SK
                );

        public string PrefixText
        {
            get => (string)GetValue(PrefixTextProperty);
            set => SetValue(PrefixTextProperty, value);
        }



        public static readonly BindableProperty SufixTextProperty = BindableProperty.Create(
                    propertyName: nameof(SufixText),
                    returnType: typeof(string),
                    declaringType: typeof(SKText),
                    defaultValue: null,
                    defaultBindingMode: BindingMode.TwoWay,
                    propertyChanged: PropertyChanged_SK
                );

        public string SufixText
        {
            get => (string)GetValue(SufixTextProperty);
            set => SetValue(SufixTextProperty, value);
        }


        public SKText()
        {
            PaintSurface += SKText_PaintSurface;
        }

        private static void PropertyChanged_SK(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (SKText)bindable;
            control.InvalidateSurface();
        }


        private void SKText_PaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;

            

            var width = info.Width;
            var height = info.Height;
            canvas.Clear();


            using (SKPaint paint = new SKPaint())
            {
                paint.Color = TextBackgroundColor.ToSKColor();
                paint.FakeBoldText = true;
                paint.TextSize = TextSize;

                canvas.DrawText(PrefixText + " " + Text + SufixText, new SKPoint(1, TextSize + 3), paint);

            }
            using (SKPaint paint = new SKPaint())
            {
                paint.Color = TextColor.ToSKColor();
                paint.TextSize = TextSize;
                paint.FakeBoldText = true;
                canvas.DrawText(PrefixText+ " " + Text + SufixText, new SKPoint(0, TextSize + 3), paint);

            }



        }
    }
}
