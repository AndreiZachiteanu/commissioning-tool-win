﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.CustomControls
{
    public class CustomToggleBtn : Button
    {
        #region Properties
        public string Identify
        {
            get { return (string)GetValue(IdentifyProperty); }
            set { SetValue(IdentifyProperty, value); }
        }

        public static readonly BindableProperty IdentifyProperty =
        BindableProperty.Create("Identify",
        typeof(string),
        typeof(CustomToggleLO),
        defaultBindingMode: BindingMode.TwoWay);

        #endregion
    }
}
