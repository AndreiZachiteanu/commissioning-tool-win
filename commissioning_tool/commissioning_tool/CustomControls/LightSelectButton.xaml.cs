﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightSelectButton : Button
    {
        public LightSelectButton()
        {
            InitializeComponent();
            CornerRadius = 5;
            if(ThisLightType == SelectedLightType)
            {
                ThisButtonSelected = true;
            }
        }

        #region BindableProperties
        public static readonly BindableProperty TextLabelProperty =
                BindableProperty.Create(nameof(TextLabel),
                typeof(string),
                typeof(LightSelectButton),
                defaultBindingMode: BindingMode.TwoWay);

        public string TextLabel
        {
            get { return (string)GetValue(TextLabelProperty); }
            set { SetValue(TextProperty, value); }
        }


        public static readonly BindableProperty ThisLightTypeProperty =
                                            BindableProperty.Create(nameof(ThisLightType),
                                            typeof(LightType),
                                            typeof(LightSelectButton),
                                            defaultBindingMode: BindingMode.TwoWay);

        public LightType ThisLightType
        {
            get { return (LightType)GetValue(ThisLightTypeProperty); }
            set { SetValue(ThisLightTypeProperty, value); }
        }

        public static readonly BindableProperty SelectedLightTypeProperty =
                                    BindableProperty.Create(nameof(SelectedLightType),
                                    typeof(LightType),
                                    typeof(LightSelectButton),
                                    defaultBindingMode: BindingMode.TwoWay,
                                    propertyChanged: SelectedLightChanged);



        public LightType SelectedLightType
        {
            get { return (LightType)GetValue(SelectedLightTypeProperty); }
            set { SetValue(ThisLightTypeProperty, value); }
        }


        public static readonly BindableProperty ThisButtonSelectedProperty =
                                    BindableProperty.Create(nameof(ThisButtonSelected),
                                    typeof(bool),
                                    typeof(LightSelectButton),
                                    defaultValue: false,
                                    defaultBindingMode: BindingMode.TwoWay,
                                    propertyChanged: SelectedLightChanged);



        public bool ThisButtonSelected
        {
            get { return (bool)GetValue(ThisButtonSelectedProperty); }
            set { SetValue(ThisLightTypeProperty, value); }
        }


        public static readonly BindableProperty ToggledColorProperty =
                            BindableProperty.Create(nameof(ToggledColor),
                            typeof(Color),
                            typeof(LightSelectButton),
                            defaultBindingMode: BindingMode.TwoWay,
                            propertyChanged: SelectedLightChanged);



        public Color ToggledColor
        {
            get { return (Color)GetValue(ToggledColorProperty); }
            set { SetValue(ToggledColorProperty, value); }
        }

        public static readonly BindableProperty UntoggledColorProperty =
                    BindableProperty.Create(nameof(UntoggledColor),
                    typeof(Color),
                    typeof(LightSelectButton),
                    defaultBindingMode: BindingMode.TwoWay,
                    propertyChanged: SelectedLightChanged);




        public Color UntoggledColor
        {
            get { return (Color)GetValue(UntoggledColorProperty); }
            set { SetValue(UntoggledColorProperty, value); }
        }


        public static readonly BindableProperty ToggledTextColorProperty =
                    BindableProperty.Create(nameof(ToggledTextColor),
                    typeof(Color),
                    typeof(LightSelectButton),
                    defaultBindingMode: BindingMode.TwoWay,
                    propertyChanged: SelectedLightChanged);



        public Color ToggledTextColor
        {
            get { return (Color)GetValue(ToggledTextColorProperty); }
            set { SetValue(ToggledTextColorProperty, value); }
        }

        public static readonly BindableProperty UntoggledTextColorProperty =
            BindableProperty.Create(nameof(UntoggledTextColor),
            typeof(Color),
            typeof(LightSelectButton),
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: SelectedLightChanged);



        public Color UntoggledTextColor
        {
            get { return (Color)GetValue(UntoggledTextColorProperty); }
            set { SetValue(UntoggledTextColorProperty, value); }
        }

        #endregion

        #region Methods
        private static void SelectedLightChanged(BindableObject bindable, object oldValue, object newValue)
        {

            var thisType = (LightType)bindable.GetValue(ThisLightTypeProperty);
            var selected = (LightType)bindable.GetValue(SelectedLightTypeProperty);
            if (thisType.ToString() == selected.ToString())
            {
                bindable.SetValue(ThisButtonSelectedProperty, true);
                bindable.SetValue(BackgroundColorProperty, bindable.GetValue(ToggledColorProperty));
                bindable.SetValue(TextColorProperty, bindable.GetValue(ToggledTextColorProperty));
            }
            else
            {
                bindable.SetValue(ThisButtonSelectedProperty, false);
                bindable.SetValue(BackgroundColorProperty, bindable.GetValue(UntoggledColorProperty));
                bindable.SetValue(TextColorProperty, bindable.GetValue(UntoggledTextColorProperty));
            }
        }
        #endregion
    }
}