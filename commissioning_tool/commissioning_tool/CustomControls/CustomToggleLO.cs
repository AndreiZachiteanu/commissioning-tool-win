﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.CustomControls
{
    public class CustomToggleLO : StackLayout
    {

        #region BindableProperties
        public static readonly BindableProperty cIDProperty =
                BindableProperty.Create("cID",
                typeof(string),
                typeof(CustomToggleLO),
                defaultBindingMode: BindingMode.TwoWay);





        public string cID
        {
            get { return (string)GetValue(cIDProperty); }
            set { SetValue(cIDProperty, value); }
        }



        #endregion




        #region Constructor
        public CustomToggleLO()
        {
            Orientation = StackOrientation.Horizontal;
            Margin = new Thickness(3,3,0,3);
            Padding = 3;
        }

        #endregion
    }
}
