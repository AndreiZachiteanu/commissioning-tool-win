﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DaySelectionBtn : Button
    {
        public DaySelectionBtn()
        {
            InitializeComponent();

        }

        #region BindableProperties
        public static readonly BindableProperty ThisDayProperty =
                        BindableProperty.Create(nameof(ThisDay),
                        typeof(iM_ActivationDayM),
                        typeof(DaySelectionBtn),
                        defaultValue:new iM_ActivationDayM(),
                        defaultBindingMode: BindingMode.TwoWay);

        public iM_ActivationDayM ThisDay
        {
            get { return (iM_ActivationDayM)GetValue(ThisDayProperty); }
            set { SetValue(ThisDayProperty, value); }
        }


        public static readonly BindableProperty SelectedDayProperty =
                BindableProperty.Create(nameof(SelectedDay),
                typeof(iM_ActivationDayM),
                typeof(DaySelectionBtn),
                defaultBindingMode: BindingMode.TwoWay,
                propertyChanged:SelectedDayChanged);



        public iM_ActivationDayM SelectedDay
        {
            get { return (iM_ActivationDayM)GetValue(SelectedDayProperty); }
            set { SetValue(SelectedDayProperty, value); }
        }


        public static readonly BindableProperty ToggledColorProperty =
                            BindableProperty.Create(nameof(ToggledColor),
                            typeof(Color),
                            typeof(LightSelectButton),
                            defaultBindingMode: BindingMode.TwoWay,
                            propertyChanged: SelectedDayChanged);
        public Color ToggledColor
        {
            get { return (Color)GetValue(ToggledColorProperty); }
            set { SetValue(ToggledColorProperty, value); }
        }

        public static readonly BindableProperty UntoggledColorProperty =
                    BindableProperty.Create(nameof(UntoggledColor),
                    typeof(Color),
                    typeof(LightSelectButton),
                    defaultBindingMode: BindingMode.TwoWay,
                    propertyChanged: SelectedDayChanged);




        public Color UntoggledColor
        {
            get { return (Color)GetValue(UntoggledColorProperty); }
            set { SetValue(UntoggledColorProperty, value); }
        }


        public static readonly BindableProperty ToggledTextColorProperty =
                    BindableProperty.Create(nameof(ToggledTextColor),
                    typeof(Color),
                    typeof(LightSelectButton),
                    defaultBindingMode: BindingMode.TwoWay,
                    propertyChanged: SelectedDayChanged);



        public Color ToggledTextColor
        {
            get { return (Color)GetValue(ToggledTextColorProperty); }
            set { SetValue(ToggledTextColorProperty, value); }
        }

        public static readonly BindableProperty UntoggledTextColorProperty =
            BindableProperty.Create(nameof(UntoggledTextColor),
            typeof(Color),
            typeof(LightSelectButton),
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: SelectedDayChanged);



        public Color UntoggledTextColor
        {
            get { return (Color)GetValue(UntoggledTextColorProperty); }
            set { SetValue(UntoggledTextColorProperty, value); }
        }
        #endregion

        #region Methods
        private static void SelectedDayChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var thisDay = bindable.GetValue(ThisDayProperty) as iM_ActivationDayM;
            var selectedDay = bindable.GetValue(SelectedDayProperty) as iM_ActivationDayM;
            if (thisDay == null)
            {
                bindable.SetValue(BackgroundColorProperty, (Color)bindable.GetValue(UntoggledColorProperty));
                bindable.SetValue(TextColorProperty, (Color)bindable.GetValue(UntoggledTextColorProperty));
            }
            else
            {
                if (thisDay.DayName == selectedDay.DayName)
                {
                    bindable.SetValue(BackgroundColorProperty, (Color)bindable.GetValue(ToggledColorProperty));
                    bindable.SetValue(TextColorProperty, (Color)bindable.GetValue(ToggledTextColorProperty));
                }
                else
                {
                    bindable.SetValue(BackgroundColorProperty, (Color)bindable.GetValue(UntoggledColorProperty));
                    bindable.SetValue(TextColorProperty, (Color)bindable.GetValue(UntoggledTextColorProperty));
                }
            }
        }
        #endregion
    }
}