﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace commissioning_tool.CustomControls
{
    public class CustomSlider : Slider
    {
        public CustomSlider()
        {
            MinColor = Color.Default;
            MaxColor = Color.Default;
        }

        public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(
            propertyName: nameof(CornerRadius),
            returnType: typeof(double),
            declaringType: typeof(CustomSlider),
            defaultValue: null,
            defaultBindingMode: BindingMode.TwoWay
        );

        public static readonly BindableProperty MinColorProperty =
        BindableProperty.Create("MinColor",
            typeof(Color),
            typeof(CustomSlider),
            defaultBindingMode: BindingMode.TwoWay);


        public Color MinColor
        {
            get { return (Color)GetValue(MinColorProperty); }
            set { SetValue(MinColorProperty, value); }
        }


        public static readonly BindableProperty MaxColorProperty =
        BindableProperty.Create("MaxColor",
            typeof(Color),
            typeof(CustomSlider),
            defaultBindingMode: BindingMode.TwoWay);


        public static BindableProperty ValueChangedCommandProperty = BindableProperty.Create(
                    propertyName: "ValueChangedCommand",
                    returnType: typeof(ICommand),
                    declaringType: typeof(CustomSlider),
                    defaultValue: null);

        public Color MaxColor
        {
            get { return (Color)GetValue(MaxColorProperty); }
            set { SetValue(MaxColorProperty, value); }
        }

        public static readonly BindableProperty CenterColorProperty =
            BindableProperty.Create("CenterColor",
            typeof(Color),
            typeof(CustomSlider),
            defaultBindingMode: BindingMode.TwoWay);


        public Color CenterColor
        {
            get { return (Color)GetValue(CenterColorProperty); }
            set { SetValue(CenterColorProperty, value); }
        }

        public static readonly BindableProperty UseCenterColorProperty =
                BindableProperty.Create("UseCenterColor",
                typeof(bool),
                typeof(CustomSlider),
                defaultBindingMode: BindingMode.TwoWay);


        public bool UseCenterColor
        {
            get { return (bool)GetValue(UseCenterColorProperty); }
            set { SetValue(UseCenterColorProperty, value); }
        }


        public double CornerRadius
        {
            get => (double)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

    }

}
