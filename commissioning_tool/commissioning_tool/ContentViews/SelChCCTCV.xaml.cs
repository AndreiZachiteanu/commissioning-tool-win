﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelChCCTCV : ContentView
    {
        public SelChCCTCV()
        {
            InitializeComponent();
        }

        #region BindableProperties
        public static readonly BindableProperty SelectChCommandProperty =
                        BindableProperty.Create("SelectChCommand",
                        typeof(ICommand),
                        typeof(SelChPrimCV),
                        defaultBindingMode: BindingMode.TwoWay);

        public ICommand SelectChCommand
        {
            get { return (ICommand)GetValue(SelectChCommandProperty); }
            set { SetValue(SelectChCommandProperty, value); }
        }

        public static readonly BindableProperty SelectChannelColourCommandProperty =
        BindableProperty.Create("SelectChannelColourCommand",
        typeof(ICommand),
        typeof(SelChPrimCV),
        defaultBindingMode: BindingMode.TwoWay);

        public ICommand SelectChannelColourCommand
        {
            get { return (ICommand)GetValue(SelectChannelColourCommandProperty); }
            set { SetValue(SelectChannelColourCommandProperty, value); }
        }


        public static BindableProperty DMXChColourProperty =
                            BindableProperty.Create("DMXChColour",
                            typeof(string),
                            typeof(SelChPrimCV),
                            defaultBindingMode: BindingMode.TwoWay);

        public string DMXChColour
        {
            get { return (string)GetValue(DMXChColourProperty); }
            set { SetValue(DMXChColourProperty, value); }
        }
        #endregion

    }
}