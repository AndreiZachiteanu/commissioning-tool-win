﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SensorCV : ContentView
    {
        public SensorCV()
        {
            InitializeComponent();
        }


        public static readonly BindableProperty TapCommandProperty = BindableProperty.Create(
                                                                              propertyName: "TapCommand",
                                                                              returnType: typeof(ICommand),
                                                                              declaringType: typeof(SensorCV),
                                                                              defaultValue: null);

        public ICommand TapCommand
        {
            get
            {
                return (ICommand)base.GetValue(TapCommandProperty);
            }
            set
            {

                base.SetValue(TapCommandProperty, value);

            }
        }

        public static readonly BindableProperty ActCommandProperty = BindableProperty.Create(
                                                                              propertyName: "ActCommand",
                                                                              returnType: typeof(ICommand),
                                                                              declaringType: typeof(SensorCV),
                                                                              defaultValue: null);

        public ICommand ActCommand
        {
            get
            {
                return (ICommand)base.GetValue(ActCommandProperty);
            }
            set
            {

                base.SetValue(ActCommandProperty, value);

            }
        }

        public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create(
                                                                                      propertyName: "TapCommandParameter",
                                                                                      returnType: typeof(Object),
                                                                                      declaringType: typeof(SensorCV),
                                                                                      defaultValue: null);

        public Object TapCommandParameter
        {
            get
            {
                return (Object)base.GetValue(TapCommandParameterProperty);
            }
            set
            {

                base.SetValue(TapCommandParameterProperty, value);

            }
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            TapCommand.Execute(TapCommandParameter);
        }

        private void ActGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            ActCommand.Execute(TapCommandParameter);
        }
    }


}