﻿using commissioning_tool.PhysicalDevices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.HierarchyItems
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Hier_LG_Lights_CV : ContentView
    {
        public Hier_LG_Lights_CV()
        {
            InitializeComponent();
        }


        #region Properties
        public static readonly BindableProperty LightListProperty = BindableProperty.Create(
                                            propertyName: nameof(LightList),
                                            returnType: typeof(ObservableCollection<Light>),
                                            declaringType: typeof(Hier_LG_Lights_CV),
                                            defaultValue: null,
                                            defaultBindingMode: BindingMode.TwoWay
                                        );

        public ObservableCollection<Light> LightList
        {
            get => (ObservableCollection<Light>)GetValue(LightListProperty);
            set => SetValue(LightListProperty, value);
        }

        public static readonly BindableProperty TitleProperty = BindableProperty.Create(
                                            propertyName: nameof(Title),
                                            returnType: typeof(string),
                                            declaringType: typeof(Hier_LG_Lights_CV),
                                            defaultValue: null,
                                            defaultBindingMode: BindingMode.TwoWay
                                );

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }


        #endregion



    }
}