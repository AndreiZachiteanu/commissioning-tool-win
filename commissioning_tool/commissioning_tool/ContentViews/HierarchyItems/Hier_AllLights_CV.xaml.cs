﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.HierarchyItems
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Hier_AllLights_CV : ContentView
    {
        public Hier_AllLights_CV()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty MoveUpCommandProperty = BindableProperty.Create(
                                                                      propertyName: "MoveUpCommand",
                                                                      returnType: typeof(ICommand),
                                                                      declaringType: typeof(Hier_AllLights_CV),
                                                                      defaultValue: null);

        public ICommand MoveUpCommand
        {
            get
            {
                return (ICommand)base.GetValue(MoveUpCommandProperty);
            }
            set
            {
                base.SetValue(MoveUpCommandProperty, value);
            }
        }

    }
}