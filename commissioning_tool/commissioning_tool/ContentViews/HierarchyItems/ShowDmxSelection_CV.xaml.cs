﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.HierarchyItems
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShowDmxSelection_CV : ContentView
    {
        public ShowDmxSelection_CV()
        {
            InitializeComponent();
        }

        public static BindableProperty LightTypeProperty =
                    BindableProperty.Create("LightType",
                    typeof(LightType),
                    typeof(SelChPrimCV),
                    defaultBindingMode: BindingMode.TwoWay,
                    propertyChanged: PropChanged)
            ;

        private static void PropChanged(BindableObject bindable, object oldValue, object newValue)
        {
            Debug.WriteLine("Something has changed");
        }


        public LightType LightType
        {
            get { return (LightType)GetValue(LightTypeProperty); }
            set { SetValue(LightTypeProperty, value); }
        }


        public static BindableProperty SelectChannelCommandProperty =
                        BindableProperty.Create("SelectChannelCommand",
                        typeof(ICommand),
                        typeof(SelChPrimCV),
                        defaultBindingMode: BindingMode.TwoWay);

        public ICommand SelectChannelCommand
        {
            get { return (ICommand)GetValue(SelectChannelCommandProperty); }
            set { SetValue(SelectChannelCommandProperty, value); }
        }

        public static BindableProperty SelectChColourCommandProperty =
                                BindableProperty.Create("SelectChColourCommand",
                                typeof(ICommand),
                                typeof(SelChPrimCV),
                                defaultBindingMode: BindingMode.TwoWay);

        public ICommand SelectChColourCommand
        {
            get { return (ICommand)GetValue(SelectChColourCommandProperty); }
            set { SetValue(SelectChColourCommandProperty, value); }
        }

        public static BindableProperty ChColourProperty =
                        BindableProperty.Create("ChColour",
                        typeof(string),
                        typeof(SelChPrimCV),
                        defaultBindingMode: BindingMode.TwoWay);

        public string ChColour
        {
            get { return (string)GetValue(ChColourProperty); }
            set { SetValue(ChColourProperty, value); }
        }
    }
}