﻿using commissioning_tool.Enums;
using commissioning_tool.Models;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.HierarchyItems
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Hier_CreateLight_CV : ContentView
    {
        public Hier_CreateLight_CV()
        {
            InitializeComponent();
        }

        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < iM_StaticStatus.CH_Instance.OutChannels.Count; i++) 
            {
                iM_StaticStatus.CH_Instance.OutChannels[i].blue = false;
                iM_StaticStatus.CH_Instance.OutChannels[i].red = false;
                iM_StaticStatus.CH_Instance.OutChannels[i].green = false;
                iM_StaticStatus.CH_Instance.OutChannels[i].warm = false;
                iM_StaticStatus.CH_Instance.OutChannels[i].cool = false;
                iM_StaticStatus.CH_Instance.OutChannels[i].prim = false;
            }
        }
    }
}