﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelUniCV : ContentView
    {
        public SelUniCV()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty SelectUniverseCommandProperty =
                BindableProperty.Create("SelectUniverseCommand",
                typeof(ICommand),
                typeof(SelChPrimCV),
                defaultBindingMode: BindingMode.TwoWay);

        public ICommand SelectUniverseCommand
        {
            get { return (ICommand)GetValue(SelectUniverseCommandProperty); }
            set { SetValue(SelectUniverseCommandProperty, value); }
        }
    }
}