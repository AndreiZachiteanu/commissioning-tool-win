﻿using commissioning_tool.Models;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SceneCapture_RTCSetting : ContentView
    {

        public SceneCapture_RTCSetting()
        {
            InitializeComponent();
            
        }

        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vm = this.BindingContext as PopupPageRTCSetViewModel;
            var item = sender as Picker;
            var bind = item.BindingContext as Actions;
            var selectedItem = item.SelectedItem as Actions;

            var tempAction = new Actions();

            var firstInt = 0;
            var secondInt = 0;
            bool use = false;
            for(int i = 0; i< vm.currentSchedule.actions.Count; i++)
            {
                var x = vm.currentSchedule.actions[i];
                
                if (x.mergedList.Contains(bind))
                {
                    firstInt = i;
                    secondInt = x.tasks.IndexOf(bind);
                    use = true;
                    // tempAction = x.mergedList[];// = selectedItem;
                }


            }
            if (use)
            {
                vm.currentSchedule.actions[firstInt].tasks[secondInt] = selectedItem;
            }

            //tempAction = selectedItem;
        }

        private void Grid_Unfocused(object sender, FocusEventArgs e)
        {
            var schedGrid = sender as Entry;
            var sched = schedGrid.BindingContext as SchedulesM;
            sched.editName = false;
        }
    }
}