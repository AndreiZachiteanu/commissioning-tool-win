﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VSContent : ContentView
    {
        public VSContent()
        {
            InitializeComponent();
        }
        public View VerticalContent
        {
            get => (View)GetValue(ContentProperty);
            set => SetValue(ContentProperty, Verticalize(value));
        }

        public double ContentRotation { get; set; } = -90;

        private View Verticalize(View toBeRotated)
        {
            if (toBeRotated == null)
                return null;

            toBeRotated.Rotation = ContentRotation;
            var result = new RelativeLayout();

            result.Children.Add(toBeRotated,
            xConstraint: Constraint.RelativeToParent((parent) =>
            {
                return parent.X - ((parent.Height - parent.Width) / 2);
            }),
            yConstraint: Constraint.RelativeToParent((parent) =>
            {
                return (parent.Height / 2) - (parent.Width / 2);
            }),
            widthConstraint: Constraint.RelativeToParent((parent) =>
            {
                return parent.Height;
            }),
            heightConstraint: Constraint.RelativeToParent((parent) =>
            {
                return parent.Width;
            }));

            return result;
        }
    }
}