﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class iM_TaskButtonUI : ContentView
    {
        public iM_TaskButtonUI()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty TapCommandProperty = BindableProperty.Create(
                                                                              propertyName: "TapCommand",
                                                                              returnType: typeof(ICommand),
                                                                              declaringType: typeof(LightCV),
                                                                              defaultValue: null);

        public ICommand TapCommand
        {
            get
            {
                return (ICommand)base.GetValue(TapCommandProperty);
            }
            set
            {

                base.SetValue(TapCommandProperty, value);

            }
        }

        public static readonly BindableProperty StartCommandProperty = BindableProperty.Create(
                                                                              propertyName: "StartCommand",
                                                                              returnType: typeof(ICommand),
                                                                              declaringType: typeof(LightCV),
                                                                              defaultValue: null);

        public ICommand StartCommand
        {
            get
            {
                return (ICommand)base.GetValue(StartCommandProperty);
            }
            set
            {

                base.SetValue(StartCommandProperty, value);

            }
        }

        public static readonly BindableProperty StopCommandProperty = BindableProperty.Create(
                                                                      propertyName: "StopCommand",
                                                                      returnType: typeof(ICommand),
                                                                      declaringType: typeof(LightCV),
                                                                      defaultValue: null);

        public ICommand StopCommand
        {
            get
            {
                return (ICommand)base.GetValue(StopCommandProperty);
            }
            set
            {

                base.SetValue(StopCommandProperty, value);

            }
        }


        public static readonly BindableProperty PauseCommandProperty = BindableProperty.Create(
                                                              propertyName: "PauseCommand",
                                                              returnType: typeof(ICommand),
                                                              declaringType: typeof(LightCV),
                                                              defaultValue: null);

        public ICommand PauseCommand
        {
            get
            {
                return (ICommand)base.GetValue(PauseCommandProperty);
            }
            set
            {

                base.SetValue(PauseCommandProperty, value);

            }
        }

        public static readonly BindableProperty ResumeCommandProperty = BindableProperty.Create(
                                                      propertyName: "ResumeCommand",
                                                      returnType: typeof(ICommand),
                                                      declaringType: typeof(LightCV),
                                                      defaultValue: null);

        public ICommand ResumeCommand
        {
            get
            {
                return (ICommand)base.GetValue(ResumeCommandProperty);
            }
            set
            {

                base.SetValue(ResumeCommandProperty, value);

            }
        }




        public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create(
                                                                                      propertyName: "TapCommandParameter",
                                                                                      returnType: typeof(Object),
                                                                                      declaringType: typeof(LightCV),
                                                                                      defaultValue: null);

        public Object TapCommandParameter
        {
            get
            {
                return (Object)base.GetValue(TapCommandParameterProperty);
            }
            set
            {

                base.SetValue(TapCommandParameterProperty, value);

            }
        }



        /*
        public static readonly BindableProperty StartCommandParameterProperty = BindableProperty.Create(
                                                                              propertyName: "StartCommandParameter",
                                                                              returnType: typeof(Object),
                                                                              declaringType: typeof(LightCV),
                                                                              defaultValue: null);
        public Object StartCommandParameter
        {
            get
            {
                return (Object)base.GetValue(StartCommandParameterProperty);
            }
            set
            {
                base.SetValue(StartCommandParameterProperty, value);
            }
        }

        public static readonly BindableProperty StopCommandParameterProperty = BindableProperty.Create(
                                                                      propertyName: "StopCommandParameter",
                                                                      returnType: typeof(Object),
                                                                      declaringType: typeof(LightCV),
                                                                      defaultValue: null);
        public Object StopCommandParameter
        {
            get
            {
                return (Object)base.GetValue(StopCommandParameterProperty);
            }
            set
            {
                base.SetValue(StopCommandParameterProperty, value);
            }
        }


        public static readonly BindableProperty PauseCommandParameterProperty = BindableProperty.Create(
                                                                      propertyName: "PauseCommandParameter",
                                                                      returnType: typeof(Object),
                                                                      declaringType: typeof(LightCV),
                                                                      defaultValue: null);
        public Object PauseCommandParameter
        {
            get
            {
                return (Object)base.GetValue(PauseCommandParameterProperty);
            }
            set
            {
                base.SetValue(PauseCommandParameterProperty, value);
            }
        }

        public static readonly BindableProperty ResumeCommandParameterProperty = BindableProperty.Create(
                                                              propertyName: "ResumeCommandParameter",
                                                              returnType: typeof(Object),
                                                              declaringType: typeof(LightCV),
                                                              defaultValue: null);
        public Object ResumeCommandParameter
        {
            get
            {
                return (Object)base.GetValue(ResumeCommandParameterProperty);
            }
            set
            {
                base.SetValue(ResumeCommandParameterProperty, value);
            }
        }
        */

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            TapCommand.Execute(TapCommandParameter);
        }

        private void StartLogical_Tapped(object sender, EventArgs e)
        {
            StartCommand.Execute(TapCommandParameter);
        }

        private void StopLogical_Tapped(object sender, EventArgs e)
        {
            StopCommand.Execute(TapCommandParameter);
        }

        private void PauseLogical_Tapped(object sender, EventArgs e)
        {
            PauseCommand.Execute(TapCommandParameter);
        }

        private void ResumeLogical_Tapped(object sender, EventArgs e)
        {
            ResumeCommand.Execute(TapCommandParameter);
        }
    }
}