﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreatePolicyItems : ContentView
    {
        public CreatePolicyItems()
        {
            InitializeComponent();
        }

        #region BindableProperties
        public static readonly BindableProperty NewPolicyProperty =
                        BindableProperty.Create("NewPolicy",
                        typeof(iM_newPolicyM),
                        typeof(CreatePolicy),
                        defaultBindingMode: BindingMode.TwoWay);

        public iM_newPolicyM NewPolicy
        {
            get { return (iM_newPolicyM)GetValue(NewPolicyProperty); }
            set { SetValue(NewPolicyProperty, value); }
        }
        #endregion
    }
}