﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightValuesRGBX : ContentView
    {
        public LightValuesRGBX()
        {
            InitializeComponent();
        }

        #region BindableProperties
        public static readonly BindableProperty LightValuesProperty =
                BindableProperty.Create(nameof(LightValues),
                typeof(LightValues),
                typeof(LightValuesRGBX),
                defaultBindingMode: BindingMode.TwoWay);

        public LightValues LightValues
        {
            get { return (LightValues)GetValue(LightValuesProperty); }
            set { SetValue(LightValuesProperty, value); }
        }
        #endregion
    }
}