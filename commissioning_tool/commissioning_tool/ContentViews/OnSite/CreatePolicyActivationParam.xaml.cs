﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreatePolicyActivationParam : ContentView
    {
        public CreatePolicyActivationParam()
        {
            InitializeComponent();
            SelectedDay = new iM_ActivationDayM();
            ActivationDaySel_Command = new Command((param) => 
            {
                SelectedDay = param as iM_ActivationDayM;
            });
            AddTimesToActivationPol_Command = new Command(() => 
            {
                SelectedDay.times.Add(new iM_StartEndTimesM());
            });
            DeleteTimeActivationPol_Command = new Command((param) => 
            {
                SelectedDay.times.Remove(param as iM_StartEndTimesM);
            });
        }
        #region Properties
        public Command ActivationDaySel_Command { get; set; }
        public iM_ActivationDayM SelectedDay { get; set; }
        public Command AddTimesToActivationPol_Command { get; set; }
        public Command DeleteTimeActivationPol_Command { get; set; }
        #endregion

        #region BindableProperties
        public static readonly BindableProperty NewPolicyProperty =
                        BindableProperty.Create("NewPolicy",
                        typeof(iM_newPolicyM),
                        typeof(CreatePolicyActivationParam),
                        defaultBindingMode: BindingMode.TwoWay);

        public iM_newPolicyM NewPolicy
        {
            get { return (iM_newPolicyM)GetValue(NewPolicyProperty); }
            set { SetValue(NewPolicyProperty, value); }
        }

        
        #endregion
    }
}