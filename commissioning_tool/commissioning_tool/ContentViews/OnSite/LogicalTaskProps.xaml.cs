﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LogicalTaskProps : ContentView
    {
        public LogicalTaskProps()
        {
            InitializeComponent();
        }

        #region BindableProperties

        public static readonly BindableProperty PropertiesProperty = BindableProperty.Create(
                                                              propertyName: "Properties",
                                                              returnType: typeof(iM_TaskModel),
                                                              declaringType: typeof(LogicalTaskProps),
                                                              defaultValue: null);

        public iM_TaskModel Properties
        {
            get
            {
                return (iM_TaskModel)base.GetValue(PropertiesProperty);
            }
            set
            {
                base.SetValue(PropertiesProperty, value);
            }
        }
        public static readonly BindableProperty PlayProperty = BindableProperty.Create(
                                              propertyName: "Play",
                                              returnType: typeof(ICommand),
                                              declaringType: typeof(LogicalTaskProps),
                                              defaultValue: null);

        public ICommand Play
        {
            get
            {
                return (ICommand)base.GetValue(PlayProperty);
            }
            set
            {
                base.SetValue(PlayProperty, value);
            }
        }

        public static readonly BindableProperty StopProperty = BindableProperty.Create(
                                              propertyName: "Stop",
                                              returnType: typeof(ICommand),
                                              declaringType: typeof(LogicalTaskProps),
                                              defaultValue: null);

        public ICommand Stop
        {
            get
            {
                return (ICommand)base.GetValue(StopProperty);
            }
            set
            {
                base.SetValue(StopProperty, value);
            }
        }

        public static readonly BindableProperty ResumeProperty = BindableProperty.Create(
                                      propertyName: "Resume",
                                      returnType: typeof(ICommand),
                                      declaringType: typeof(LogicalTaskProps),
                                      defaultValue: null);

        public ICommand Resume
        {
            get
            {
                return (ICommand)base.GetValue(ResumeProperty);
            }
            set
            {
                base.SetValue(ResumeProperty, value);
            }
        }

        public static readonly BindableProperty PauseProperty = BindableProperty.Create(
                                      propertyName: "Pause",
                                      returnType: typeof(ICommand),
                                      declaringType: typeof(LogicalTaskProps),
                                      defaultValue: null);

        public ICommand Pause
        {
            get
            {
                return (ICommand)base.GetValue(PauseProperty);
            }
            set
            {
                base.SetValue(PauseProperty, value);
            }
        }
        #endregion
    }
}