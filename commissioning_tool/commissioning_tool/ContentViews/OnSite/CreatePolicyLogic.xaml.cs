﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreatePolicyLogic : ContentView
    {
        public CreatePolicyLogic()
        {
            InitializeComponent();
        }
        #region BindableProperties
        public static readonly BindableProperty NewPolicyProperty =
                        BindableProperty.Create("NewPolicy",
                        typeof(iM_newPolicyM),
                        typeof(CreatePolicy),
                        defaultBindingMode: BindingMode.TwoWay);

        public iM_newPolicyM NewPolicy
        {
            get { return (iM_newPolicyM)GetValue(NewPolicyProperty); }
            set { SetValue(NewPolicyProperty, value); }
        }

        public static readonly BindableProperty SelectedPolicySeqProperty =
        BindableProperty.Create("SelectedPolicySeq",
        typeof(PolicySequence),
        typeof(CreatePolicy),
        defaultBindingMode: BindingMode.TwoWay);

        public PolicySequence SelectedPolicySeq
        {
            get { return (PolicySequence)GetValue(SelectedPolicySeqProperty); }
            set { SetValue(SelectedPolicySeqProperty, value); }
        }

        #endregion
    }
}