﻿using commissioning_tool.Enums;
using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightValuesContent : ContentView
    {
        public LightValuesContent()
        {
            InitializeComponent();
            SelectType_CommandMethod = new Command((param) =>
            {
                var sender = (LightType)param;
                SelectedLightType = sender;
            });
        }
        /*
        private void SetSelectedLightType(LightType lightType)
        {
            SelectedLightType = lightType;
        }
        */

        #region BindableProperties
        public static readonly BindableProperty LightValuesProperty =
                BindableProperty.Create(nameof(LightValues),
                typeof(LightValues),
                typeof(LightValuesContent),
                defaultBindingMode: BindingMode.TwoWay);

        public LightValues LightValues
        {
            get { return (LightValues)GetValue(LightValuesProperty); }
            set { SetValue(LightValuesProperty, value); }
        }

        public static readonly BindableProperty SelectedLightTypeProperty =
                            BindableProperty.Create(nameof(SelectedLightType),
                            typeof(LightType),
                            typeof(LightValuesContent),
                            defaultBindingMode: BindingMode.TwoWay);

        public LightType SelectedLightType
        {
            get { return (LightType)GetValue(SelectedLightTypeProperty); }
            set { SetValue(SelectedLightTypeProperty, value); }
        }

        public static readonly BindableProperty SelectLightType_CommandProperty =
                    BindableProperty.Create(nameof(SelectLightType_Command),
                    typeof(Command),
                    typeof(LightValuesContent),
                    defaultBindingMode: BindingMode.TwoWay);

        public Command SelectLightType_Command
        {
            get { return (Command)GetValue(SelectLightType_CommandProperty); }
            set { SetValue(SelectLightType_CommandProperty, value); }
        }

        public Command SelectType_CommandMethod { get; set; }
        #endregion
    }
}