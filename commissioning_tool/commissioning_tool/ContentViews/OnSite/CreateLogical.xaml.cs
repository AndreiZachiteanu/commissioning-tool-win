﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateLogical : ContentView
    {
        public CreateLogical()
        {
            InitializeComponent();
        }

        #region BindableProperties

        public static readonly BindableProperty NewLogicalProperty =
                                BindableProperty.Create("NewLogical",
                                typeof(iM_LogicalModel),
                                typeof(CreateLogical),
                                defaultBindingMode: BindingMode.TwoWay);

        public iM_LogicalModel NewLogical
        {
            get { return (iM_LogicalModel)GetValue(NewLogicalProperty); }
            set { SetValue(NewLogicalProperty, value); }
        }

        #endregion
    }
}