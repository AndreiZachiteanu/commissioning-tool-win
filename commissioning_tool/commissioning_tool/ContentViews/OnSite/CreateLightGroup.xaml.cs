﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateLightGroup : ContentView
    {
        public CreateLightGroup()
        {
            InitializeComponent();
            NewGroup = new iM_LightGroup();
        }

        #region BindableProperties
        public static readonly BindableProperty NewGroupProperty = BindableProperty.Create(
                                                        propertyName: nameof(NewGroup),
                                            returnType: typeof(iM_LightGroup),
                                            declaringType: typeof(CreateLightGroup),
                                            defaultValue: null,
                                            defaultBindingMode: BindingMode.TwoWay
            );

        public iM_LightGroup NewGroup
        {
            get => (iM_LightGroup)GetValue(NewGroupProperty);
            set => SetValue(NewGroupProperty, value);
        }

        public static readonly BindableProperty CreateCommandProperty = BindableProperty.Create(
                                                propertyName: nameof(CreateCommand),
                                    returnType: typeof(ICommand),
                                    declaringType: typeof(CreateLightGroup),
                                    defaultValue: null,
                                    defaultBindingMode: BindingMode.TwoWay
        );

        public ICommand CreateCommand
        {
            get => (ICommand)GetValue(CreateCommandProperty);
            set => SetValue(CreateCommandProperty, value);
        }
        #endregion
    }
}