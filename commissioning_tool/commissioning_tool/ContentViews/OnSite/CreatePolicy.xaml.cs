﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews.OnSite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreatePolicy : ContentView
    {
        public CreatePolicy()
        {
            InitializeComponent();
            ChangeView = new Command((param) => SetView(param as string));
        }
        
        public ICommand ChangeView { get; set; } 

        private void SetView(string v)
        {
            if (v.Equals("logic")) 
            {
                PolicyLogicView = true;
            }
            else
            {
                PolicyLogicView = false;
            }
        }

        #region BindableProperties
        public static readonly BindableProperty NewPolicyProperty =
                        BindableProperty.Create("NewPolicy",
                        typeof(iM_newPolicyM),
                        typeof(CreatePolicy),
                        defaultBindingMode: BindingMode.TwoWay);

        public iM_newPolicyM NewPolicy
        {
            get { return (iM_newPolicyM)GetValue(NewPolicyProperty); }
            set { SetValue(NewPolicyProperty, value); }
        }


        public static readonly BindableProperty CreateCommandProperty =
                BindableProperty.Create("CreateCommand",
                typeof(ICommand),
                typeof(CreatePolicy),
                defaultBindingMode: BindingMode.TwoWay);

        public ICommand CreateCommand
        {
            get { return (ICommand)GetValue(CreateCommandProperty); }
            set { SetValue(CreateCommandProperty, value); }
        }

        public static readonly BindableProperty PolicyLogicViewProperty =
                BindableProperty.Create("PolicyLogicView",
                typeof(bool),
                typeof(CreatePolicy),
                defaultBindingMode: BindingMode.TwoWay);

        public bool PolicyLogicView
        {
            get { return (bool)GetValue(PolicyLogicViewProperty); }
            set { SetValue(PolicyLogicViewProperty, value); }
        }

        #endregion
    }
}