﻿using commissioning_tool.Connections;
using commissioning_tool.CustomControls;
using commissioning_tool.PhysicalDevices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrimLiCV : ContentView
    {
        public PrimLiCV()
        {
            InitializeComponent();
        }
    }
}