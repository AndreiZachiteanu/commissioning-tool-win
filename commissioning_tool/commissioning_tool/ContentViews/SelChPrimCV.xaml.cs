﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.ContentViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelChPrimCV : ContentView
    {
        #region Constructor
        public SelChPrimCV()
        {
            InitializeComponent();
        }
        #endregion

        #region BindableProperties
        public static readonly BindableProperty SelectChCommandProperty =
                        BindableProperty.Create("SelectChCommand",
                        typeof(ICommand),
                        typeof(SelChPrimCV),
                        defaultBindingMode: BindingMode.TwoWay);

        public ICommand SelectChCommand
        {
            get { return (ICommand)GetValue(SelectChCommandProperty); }
            set { SetValue(SelectChCommandProperty, value); }
        }


        #endregion
    }
}