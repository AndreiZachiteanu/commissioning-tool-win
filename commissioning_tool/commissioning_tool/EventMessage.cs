﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool
{
    public class EventMessage :EventArgs
    {
        public string Message { get; set; }
        public LogicalTypeEnum LogicalType { get; set; }
        public string Id { get; internal set; }
    }
}
