﻿using commissioning_tool.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace commissioning_tool.Converters
{


    public class TriggerJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            try
            {

                JObject jsonObject = JObject.Load(reader);

                var vals = jsonObject.Properties().ToList();

                List<TriggerJson> returnedObj = new List<TriggerJson>();



                for (int i = 0; i < vals.Count; i++)
                {
                    var triggerJson = new TriggerJson();

                    triggerJson.Name = vals[i].Name.Replace("$", "");

                    ObservableCollection<Trigger> values = new ObservableCollection<Trigger>();

                    List<int> intValues = vals[i].Value.ToObject<int[]>().ToList();

                    for (int y = 0; y < intValues.Count; y++)
                    {
                        values.Add(new Trigger { value = intValues[y] });
                    }

                    triggerJson.Trigger = values;

                    returnedObj.Add(triggerJson);

                }


                return returnedObj;
            }catch(Exception ex)
            {

            }

            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    

    public class TriggerJson : BaseViewModel
    {
        public string Name { get; set; }

        public ObservableCollection<Trigger> Trigger { get; set; }

    }

    public class Trigger : BaseViewModel
    {

        public int value { get; set; }
        public bool used { get; set; }
        public int sliderValue { get; set; }
        public int rate
        {
            get
            {
                return (int)(Convert.ToDouble(sliderValue) / 2.55);
            }
        }
    }
}
