﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Converters
{
    public class TimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var val = ((string)reader.Value);
            TimeSpan time = TimeSpan.FromSeconds(0);
            if (val != null)
            {
                time = TimeSpan.Parse(val);
            }
            return time;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
