﻿using commissioning_tool.Enums;
using commissioning_tool.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace commissioning_tool.Converters
{
    public class ActionConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            //var val = ((string)reader.Value).ToUpper(); ;
            //var x = JsonConvert.DeserializeObject < List<string>>(reader);
            JToken token = JToken.Load(reader);
            List<string> items = token.ToObject<List<string>>();

            var typeSplit = reader.Path.Split('.');
            var typeString = typeSplit[typeSplit.Length - 1];
            LogicalTypeEnum type = LogicalTypeEnum.TASK;
            switch (typeString)
            {
                case "tasks":
                    type = LogicalTypeEnum.TASK;
                    break;
                case "scenes":
                    type = LogicalTypeEnum.SCENE;
                    break;
            }
            
            //var obj = jsonObject.Children();
            var actions = new ObservableCollection<Actions>();//{ id = val};

            for(int i = 0; i < items.Count; i++)
            {
                actions.Add(new Actions { id = items[i], type = type });
            }
            return actions;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
