﻿using commissioning_tool.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace commissioning_tool.Converters
{
    public class SceneDurationJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var val = (string)reader.Value;




            //return Regex.Replace(val, @"[^0-9]", String.Empty);

            var sceneDuration = new Duration();
            if (val.EndsWith("S"))
            {
                sceneDuration.Seconds =Convert.ToInt32(Regex.Replace(val, @"[^0-9]", String.Empty));
            }
            else if (val.EndsWith("M"))
            {
                sceneDuration.Minutes = Convert.ToInt32(Regex.Replace(val, @"[^0-9]", String.Empty));
            }
            else if (val.EndsWith("H"))
            {
                sceneDuration.Hours = Convert.ToInt32(Regex.Replace(val, @"[^0-9]", String.Empty));
            }

            return sceneDuration;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
