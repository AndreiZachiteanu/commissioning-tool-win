﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using commissioning_tool.Models;

namespace commissioning_tool.Converters
{
    public class ActionListConverter : JsonConverter
    {        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            ObservableCollection<ScheduleActions> items = token.ToObject<ObservableCollection<ScheduleActions>>();





            var defaultList = new ObservableCollection<ScheduleActions>
            {
                new ScheduleActions{control = "START"},
                new ScheduleActions{control = "STOP"},
                new ScheduleActions{control = "PAUSE"},
                new ScheduleActions{control = "RESUME"},
            };

            for (int i = 0; i < defaultList.Count; i++)
            {
                var item = items.Where(w => w.control == defaultList[i].control).FirstOrDefault();
                if(item == null)
                {
                    items.Add(defaultList[i]);
                }
            }
            /*
            mergeControls(items.Where(w => w.control == "START").ToList());
            var mergingItemsStop = items.Where(w => w.control == "STOP").ToList();
            var mergingItemsResume = items.Where(w => w.control == "RESUME").ToList();
            var mergingItemsPause = items.Where(w => w.control == "PAUSE").ToList();
            */
            var returnList = new ObservableCollection<ScheduleActions>()
            {
                mergeControls(items.Where(w => w.control == "START").ToList()),
                mergeControls(items.Where(w => w.control == "STOP").ToList()),
                mergeControls(items.Where(w => w.control == "PAUSE").ToList()),
                mergeControls(items.Where(w => w.control == "RESUME").ToList())
            };

            return returnList;
        }

        private ScheduleActions mergeControls(List<ScheduleActions> scheduleActions)
        {

                if (scheduleActions.Count > 1)
                {


                    for (int i = 1; i < scheduleActions.Count; i++)
                    {
                        for (int y = 0; y < scheduleActions[i].tasks.Count; y++)
                        {
                            scheduleActions[0].tasks.Add(scheduleActions[i].tasks[y]);// = new ObservableCollection<Actions>()
                        }
                        for (int y = 0; y < scheduleActions[i].scenes.Count; y++)
                        {
                            scheduleActions[0].scenes.Add(scheduleActions[i].scenes[y]);// = new ObservableCollection<Actions>()
                        }
                    }
                }
            return scheduleActions[0];
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }    
}
