﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using commissioning_tool.Models;

namespace commissioning_tool.Converters
{
    public class DaysConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            //var val = ((string)reader.Value).ToUpper(); ;
            //var x = JsonConvert.DeserializeObject < List<string>>(reader);
            JToken token = JToken.Load(reader);
            List<string> items = token.ToObject<List<string>>();


            //var obj = jsonObject.Children();
            var acivationDay= new ActivationSetList();//{ id = val};

            for (int i = 0; i < items.Count; i++)
            {
                var day = acivationDay.Where(w => w.DayName.ToString().ToLower() == items[i].ToLower()).FirstOrDefault();
                if(day != null)
                {
                    day.use = true;
                }
            }
            return acivationDay;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
