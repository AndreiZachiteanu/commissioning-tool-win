﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Converters
{
    public class SequenceTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case "ON":
                    return SequenceType.ON;
                case "OFF":
                    return SequenceType.OFF;
                case "INTERIM":
                    return SequenceType.INTERIM;
                default:
                    return null;
            }
        }
    }
}
