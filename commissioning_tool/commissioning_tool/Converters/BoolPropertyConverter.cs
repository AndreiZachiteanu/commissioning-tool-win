﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Converters
{
    public class BoolPropertyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            bool val = false;
            if (reader.ValueType == typeof(string))
            {
                val = (string)reader.Value == null ? false : Convert.ToBoolean((string)reader.Value);
            }
            else if(reader.ValueType == typeof(bool))
            {
                val = (bool)reader.Value;
            }
            return val;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
