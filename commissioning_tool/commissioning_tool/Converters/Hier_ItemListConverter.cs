﻿using commissioning_tool.Assets;
using commissioning_tool.Models;
using commissioning_tool.PhysicalDevices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace commissioning_tool.Converters
{
    public class Hier_ItemListConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            HierarchyItemList Returnlist = new HierarchyItemList();

           // JObject jsonObject = JObject.Load(reader);

            switch (reader.Path.Split('.')[1])
            {
                case "zones":
                    Returnlist.ContainerName = "Zones";
                    Returnlist.ItemList = new ObservableCollection<Hierarchy>(token.ToObject<List<Zone>>() == null ? new List<Zone>() : token.ToObject<List<Zone>>());
                    //Returnlist.ItemList = token.ToObject<ObservableCollection<Hierarchy>>();
                    break;
                case "campuses":
                    Returnlist.ContainerName = "Campuses";
                    Returnlist.ItemList = new ObservableCollection<Hierarchy>(token.ToObject<List<Campus>>() == null ? new List<Campus>() : token.ToObject<List<Campus>>());
                    //Returnlist.ItemList = token.ToObject<ObservableCollection<Hierarchy>>();
                    break;
                case "estates":
                    Returnlist.ContainerName = "Estates";
                    Returnlist.ItemList = new ObservableCollection<Hierarchy>(token.ToObject<List<Hier_Estates>>() == null ? new List<Hier_Estates>() : token.ToObject<List<Hier_Estates>>());
                    //Returnlist.ItemList = token.ToObject<ObservableCollection<Hierarchy>>();
                    break;
                case "buildings":
                    Returnlist.ContainerName = "Buildings";
                    Returnlist.ItemList = new ObservableCollection<Hierarchy>(token.ToObject<List<Building>>() == null ? new List<Building>() : token.ToObject<List<Building>>());
                    //Returnlist.ItemList = token.ToObject<ObservableCollection<Hierarchy>>();
                    break;
                case "floors":
                    Returnlist.ContainerName = "Floors";


                    Returnlist.ItemList = new ObservableCollection<Hierarchy>(token.ToObject<List<Floor>>() == null ? new List<Floor>() : token.ToObject<List<Floor>>()) ;
                    //Returnlist.ItemList = token.ToObject<ObservableCollection<Hierarchy>>();
                    break;
                case "lights":
                    return new ObservableCollection<Light>(token.ToObject<List<Light>>() == null ? new List<Light>() : token.ToObject<List<Light>>());
                case "lightGroups":
                    return new ObservableCollection<iM_LightGroup>(token.ToObject<List<iM_LightGroup>>() == null ? new List<iM_LightGroup>() : token.ToObject<List<iM_LightGroup>>());
                case "policies":
                    return new ObservableCollection<iM_newPolicyM>(token.ToObject<List<iM_newPolicyM>>() == null ? new List<iM_newPolicyM>() : token.ToObject<List<iM_newPolicyM>>());
                case "scenes":
                    return new ObservableCollection<iM_SceneModel>(token.ToObject<List<iM_SceneModel>>() == null ? new List<iM_SceneModel>() : token.ToObject<List<iM_SceneModel>>());
                case "tasks":
                    return new ObservableCollection<iM_TaskModel>(token.ToObject<List<iM_TaskModel>>() == null ? new List<iM_TaskModel>() : token.ToObject<List<iM_TaskModel>>());
                case "taskGroups":
                    return new ObservableCollection<iM_TaskGroupModel>(token.ToObject<List<iM_TaskGroupModel>>() == null ? new List<iM_TaskGroupModel>() : token.ToObject<List<iM_TaskGroupModel>>());
                case "sensors":
                    return new ObservableCollection<Sensor>(token.ToObject<List<Sensor>>() == null ? new List<Sensor>() : token.ToObject<List<Sensor>>());
            }

                    

            return Returnlist;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
