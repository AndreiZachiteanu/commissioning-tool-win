﻿using commissioning_tool.Models;
using commissioning_tool.PhysicalDevices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
namespace commissioning_tool.Converters
{
    public class LightIdJsonToGroup : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var lights=  iM_StaticImuneItems.Instance.LightList;
            ObservableCollection<Light> groupLights = new ObservableCollection<Light>();
            JToken token = JToken.Load(reader);
            var ids = token.ToObject<List<string>>();
            for(int i = 0; i< ids.Count; i++)
            {
                var light = lights.Where(w => w.ID == ids[i]).FirstOrDefault();
                if(light != null)
                {
                    groupLights.Add((Light)light);
                }
            }

            return groupLights;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
