﻿using commissioning_tool.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace commissioning_tool.Converters
{
    public class LightValueJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jsonObject = JObject.Load(reader);

            //var obj = jsonObject.Children();//.Properties().ToList()[0];


            LightValues val = new LightValues();


            var delay = jsonObject["delay"];

            val.delay.intValue = delay != null ? Regex.Replace((string)delay, @"[^0-9]", String.Empty) : "0";
            val.duration.intValue = (string)jsonObject["duration"];
            val.transition.text = ((string)jsonObject["transition"]).ToUpper();
            var rgb = Convert.ToInt32(Regex.Replace((string)jsonObject["values"]["RGB"], @"[^0-9]", String.Empty)); 
            var sc = Regex.Replace((string)jsonObject["values"]["PRIMITIVE"], @"[^0-9]", String.Empty);

            var tempVals = ((string)jsonObject["values"]["CCT"]).Split('_');

            var rgbxSplit = ((string)jsonObject["values"]["RGBX"]).Split('_');
            var rgbxVal =Convert.ToInt32(rgbxSplit[1]);
            var rgbxVal_x = rgbxSplit[2];
            var wCh = Convert.ToDouble(tempVals[1]);
            var cCh = Convert.ToDouble(tempVals[2]);

            var divider = 255 / (wCh + cCh);

            val.SCIntensity = Convert.ToInt32(sc);

            val.CCTTemperature = 255 - (int)(wCh * divider);
            val.CCTIntensity = (int)(divider * 25);           

            val.RValue = (rgb >> 16) & 0xFF;
            val.GValue = (rgb >> 8) & 0xFF;
            val.BValue = (rgb) & 0xFF;


            val.RValueX = (rgbxVal >> 16) & 0xFF;
            val.GValueX = (rgbxVal >> 8) & 0xFF;
            val.BValueX = (rgbxVal) & 0xFF;

            val.xValue = Convert.ToInt32(rgbxVal_x);
            return val;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
