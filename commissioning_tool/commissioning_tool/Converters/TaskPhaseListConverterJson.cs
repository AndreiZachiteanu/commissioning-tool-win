﻿using commissioning_tool.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace commissioning_tool.Converters
{
    public class TaskPhaseListConverterJson : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            //JToken jToken = reader.;
            // JObject jsonObject = JObject.Load(reader);

            //var item = jsonObject.Properties().ToList();
            //var vals = new ObservableCollection<TaskPhaseList>((IEnumerable<TaskPhaseList>)item);
            if (reader.TokenType == JsonToken.StartArray)
            {
                JToken token = JToken.Load(reader);
                ObservableCollection<TaskPhase> items = token.ToObject<ObservableCollection<TaskPhase>>();
                int i = 0;
                foreach(var item in items)
                {
                    item.Name = "Phase " + i++;
                }
                //myCustomType = new MyCustomType(items);
            }

            return new ObservableCollection<TaskPhase>();
            //return vals;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
