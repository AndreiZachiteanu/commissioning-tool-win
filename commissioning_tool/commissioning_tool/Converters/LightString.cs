﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Converters
{
    public class LightString : IValueConverter
    {
        public static string LightTypeToString(LightType type)
        {
            return type.ToString();
        }

        public static LightType LightStringToType(string type)
        {

            switch (type)
            {
                case "PRIM":
                    return LightType.PRIMITIVE;
                case "PRIMITIVE":
                    return LightType.PRIMITIVE;
                case "CCT":
                    return LightType.CCT;
                case "RGB":
                    return LightType.RGB;
                case "RGBX":
                    return LightType.RGBX;
                default:
                    return LightType.PRIMITIVE;
            }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();


        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case "PRIMITIVE":
                    return LightType.PRIMITIVE;
                case "CCT":
                    return LightType.CCT;
                case "RGB":
                    return LightType.RGB;
                case "RGBX":
                    return LightType.RGBX;
                default:
                    return LightType.PRIMITIVE;
            }
        }
    }
}
