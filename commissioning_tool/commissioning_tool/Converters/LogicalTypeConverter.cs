﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Converters
{
    public class LogicalTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            switch (value)
            {
                case "TASK":
                    return LogicalTypeEnum.TASK;
                case "TASK GROUP":
                    return LogicalTypeEnum.TASK_GROUP;
                case "SCENE":
                    return LogicalTypeEnum.SCENE;
                default:

                    return LogicalTypeEnum.TASK;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {


            switch (value)
            {
                case LogicalTypeEnum.TASK:
                    return "TASK";
                case LogicalTypeEnum.TASK_GROUP:
                    return "TASK GROUP";
                case LogicalTypeEnum.SCENE:
                    return "SCENE";
                default:

                    return "TASK";
            }
        }
    }
}
