﻿using commissioning_tool.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Converters
{
    public class PolicyTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value)
            {
                case "TIMER":
                    return PolicyType.TIMER;
                case "SWITCHED":
                    return PolicyType.SWITCHED;
                case "TOGGLE":
                    return PolicyType.TOGGLE;
                case "PRESENCE":
                    return PolicyType.PRESENCE;
                case "ABSENCE":
                    return PolicyType.ABSENCE;
                case "SCENESELECT":
                    return PolicyType.SCENESELECT;
                case "STEPDIM":
                    return PolicyType.STEPDIM;
                case "RAISEDIM":
                    return PolicyType.RAISEDIM;
                case "DLH":
                    return PolicyType.DLH;
                case "EMERGENCY":
                    return PolicyType.EMERGENCY;
                case "FOLLOWME":
                    return PolicyType.FOLLOWME;
                default:
                    return null;
            }
        }
    }
}
