﻿using commissioning_tool.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Converters
{
    public class ActionTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var val = ((string)reader.Value).ToUpper(); ;

            var actionType = Enum.Parse(typeof(LogicalTypeEnum), val.Split('_')[0]);
            return actionType;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
