﻿using commissioning_tool.Enums;
using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace commissioning_tool.Converters
{
    public static class LightValueTypeConverter
    {
        public static (LightType Type, LightValues LightValues) ConvertStringToLight(string s)
        {
            s = s.ToLower();
            LightType type = LightType.PRIMITIVE;
            LightValues lightValues = new LightValues();
            if (s.Split(' ').Length > 1)
            {

                s = s.Split(' ')[0];
            }

            if (s.StartsWith("prim"))
            {
                
                lightValues.SCIntensity = (int)(Convert.ToDouble(Regex.Replace(s, @"[^0-9]", String.Empty)));
            }
            else if (s.StartsWith("cct"))
            {
                type = LightType.CCT;
                var tempVals = (s).Split('_');
                var wCh = Convert.ToDouble(tempVals[1]);
                var cCh = Convert.ToDouble(tempVals[2]);

                var wcCh = (wCh + cCh);
                var intensity = 0.00;

                if(wcCh > 0)
                {
                    intensity =  wcCh  / 2.55;
                }

                lightValues.CCTTemperature = (int)((cCh / (wCh + cCh))  * 255);
                lightValues.CCTIntensity = (int)Math.Ceiling(intensity);
            }
            else if (s.StartsWith("rgb") && s.Split('_').Length == 3)
            {
                type = LightType.RGBX;
                var rgbxSplit = (s).Split('_');
                var rgbxVal = Convert.ToInt32(rgbxSplit[1]);
                var rgbxVal_x = rgbxSplit[2];
                lightValues.RGBXIntensity = 255;
                lightValues.RValueX = (rgbxVal >> 16) & 0xFF;
                lightValues.GValueX = (rgbxVal >> 8) & 0xFF;
                lightValues.BValueX = (rgbxVal) & 0xFF;

                lightValues.xValue = Convert.ToInt32(rgbxVal_x);
            }
            else if (s.StartsWith("rgb"))
            {
                type = LightType.RGB;
                var rgb = Convert.ToInt32(Regex.Replace(s, @"[^0-9]", String.Empty));
                lightValues.RGBIntensity = 255;
                lightValues.RValue = (rgb >> 16) & 0xFF;
                lightValues.GValue = (rgb >> 8) & 0xFF;
                lightValues.BValue = (rgb) & 0xFF;

            }

            return ( type, lightValues);
        }

        public static (string SCstring, string CCTstring, string RGBstring, string RGBXstring) ConvertLightValuesToImuneString(LightValues lValues, string type = null)
        {

            string prime = " prim_";
            string cct = " cct_";
            string rgb = " rgb_";
            string rgbx = " rgb_";

            LightValues light = new LightValues();
            if (type == null)
            {
                prime += lValues.SCIntensity;


                double divider = (((double)lValues.CCTIntensity) / 100) == 0 ? 1 : (((double)lValues.CCTIntensity) / 100);

                int warmCh = (int)((255 - lValues.CCTTemperature) * divider);
                int coolCh = (int)(lValues.CCTTemperature * divider);

                cct += warmCh + "_" + coolCh;

                string rgbColourON = lValues.RGB.ToHex().ToString().Replace("#", "").ToLower();
                string rgbColourONX = lValues.RGBX.ToHex().ToString().Replace("#", "").ToLower();
                rgb +=  Int32.Parse(rgbColourON, System.Globalization.NumberStyles.HexNumber).ToString();

                rgbx +=  Int32.Parse(rgbColourONX, System.Globalization.NumberStyles.HexNumber).ToString() + "_" + lValues.xValue;

            }
            else
            {
                type = type.ToLower();

                switch (type)
                {
                    case "primitive":
                        prime += lValues.SCIntensity + " ";
                        break;
                    case "prim":
                        prime += lValues.SCIntensity + " ";
                        break;
                    case "cct":

                        var _warm = (255 - lValues.CCTTemperature);// * ( lValues.CCTIntensity * 0.01);// * (lValues.CCTIntensity * 0.01);
                        var _cool = (255 - _warm);// / (lValues.CCTIntensity * 0.01))) * (lValues.CCTIntensity * 0.01);

                        var warm = _warm * (lValues.CCTIntensity * 0.01);
                        var cool = _cool * (lValues.CCTIntensity * 0.01);

                        cct += (int)warm + "_" + (int)cool + " ";
                        break;
                    case "rgb":
                        string rgbColourON = Color.FromRgb(lValues.RValue, lValues.GValue, lValues.BValue).ToHex().ToString().Replace("#", "").ToLower();
                        rgb += Int32.Parse(rgbColourON, System.Globalization.NumberStyles.HexNumber).ToString() + " "; 
                        break;
                    case "rgbx":
                        string rgbColourON1 = lValues.RGBX.ToHex().ToString().Replace("#", "").ToLower();
                        rgbx += Int32.Parse(rgbColourON1, System.Globalization.NumberStyles.HexNumber).ToString() + "_" + lValues.xValue + " ";
                        break;
                }
            }

            
            

            return (prime, cct, rgb, rgbx);
        }




    }
}
