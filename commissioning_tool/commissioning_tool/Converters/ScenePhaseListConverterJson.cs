﻿using commissioning_tool.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace commissioning_tool.Converters
{
    public class ScenePhaseListConverterJson : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            //JToken jToken = reader.;
            JObject jsonObject = JObject.Load(reader);
            var listToReturn = new ObservableCollection<ScenePhase>(); 
            var item = jsonObject.Properties().ToList();
            for(int i =0; i< item.Count; i++)
            {
                var x = item[i].Value;

                var phaseVal = JsonConvert.DeserializeObject<PhaseVal>(x.ToString());

                LightValues lightValues = new LightValues();

                lightValues.transition.text = phaseVal.transition;

                var lightTypeString = phaseVal.value.Split('_');

                if (phaseVal.value.StartsWith("prim"))
                {
                    lightValues.SCIntensity = Convert.ToInt32(Regex.Replace(phaseVal.value, @"[^0-9]", String.Empty));
                }
                else if (phaseVal.value.StartsWith("cct"))
                {
                    var tempVals = (phaseVal.value).Split('_');
                    var wCh = Convert.ToDouble(tempVals[1]);
                    var cCh = Convert.ToDouble(tempVals[2]);

                    var divider = 255 / (wCh + cCh);





                    lightValues.CCTTemperature = 255 - (int)(wCh * divider);
                    lightValues.CCTIntensity = (int)(divider * 25);
                }
                else if (phaseVal.value.StartsWith("rgb") && lightTypeString.Length == 3)
                {

                    var rgbxSplit = (phaseVal.value).Split('_');
                    var rgbxVal = Convert.ToInt32(rgbxSplit[1]);
                    var rgbxVal_x = rgbxSplit[2];

                    lightValues.RValueX = (rgbxVal >> 16) & 0xFF;
                    lightValues.GValueX = (rgbxVal >> 8) & 0xFF;
                    lightValues.BValueX = (rgbxVal) & 0xFF;

                    lightValues.xValue = Convert.ToInt32(rgbxVal_x);
                }
                else if (phaseVal.value.StartsWith("rgb"))
                {
                    var rgb = Convert.ToInt32(Regex.Replace(phaseVal.value, @"[^0-9]", String.Empty));
                    lightValues.RValue = (rgb >> 16) & 0xFF;
                    lightValues.GValue = (rgb >> 8) & 0xFF;
                    lightValues.BValue = (rgb) & 0xFF;

                }







                listToReturn.Add(new ScenePhase
                {
                    LightID = item[i].Name,
                    LightType = LightString.LightStringToType(lightTypeString[0]),
                    LightValue = lightValues

                }) ;

            }

            return listToReturn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }


        class PhaseVal 
        {
            [JsonProperty("value")]
            public string value { get; set; }

            [JsonProperty("transition")]
            public string transition { get; set; }
        }
    }
}
