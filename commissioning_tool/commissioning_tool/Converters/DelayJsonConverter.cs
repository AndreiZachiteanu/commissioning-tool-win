﻿using commissioning_tool.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace commissioning_tool.Converters
{
    public class DelayJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            iM_NPolicyParameter val = new iM_NPolicyParameter();
            if (reader.ValueType == typeof(string))
            {
                val.text = (string)reader.Value == null ? "0" : Regex.Replace((string)reader.Value, @"[^0-9]", String.Empty);


            }
            return val;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
