﻿using commissioning_tool.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Converters
{
    public class LightDMXConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            var addresses = token.ToObject<List<DMXJsonModel>>();

            LightDMXAddress dmxAddresses = new LightDMXAddress();

            switch (addresses.Count)
            {
                case 1:
                    for(int i = 0; i<addresses[0].Addresses.Length; i++)
                    {
                        dmxAddresses.primAddresses.Add(new DMXAddress { oldValue = addresses[0].Addresses[i].ToString() });
                    }
                    break;
                case 2:
                    for (int i = 0; i < addresses[0].Addresses.Length; i++)
                    {
                        dmxAddresses.warmAddresses.Add(new DMXAddress { oldValue = addresses[0].Addresses[i].ToString() });
                    }
                    for (int i = 0; i < addresses[1].Addresses.Length; i++)
                    {
                        dmxAddresses.coolAddresses.Add(new DMXAddress { oldValue = addresses[1].Addresses[i].ToString() });
                    }
                    break;
                case 3:
                    for (int i = 0; i < addresses[0].Addresses.Length; i++)
                    {
                        dmxAddresses.redAddresses.Add(new DMXAddress { oldValue = addresses[0].Addresses[i].ToString() });
                    }
                    for (int i = 0; i < addresses[1].Addresses.Length; i++)
                    {
                        dmxAddresses.greenAddresses.Add(new DMXAddress { oldValue = addresses[1].Addresses[i].ToString() });
                    }
                    for (int i = 0; i < addresses[2].Addresses.Length; i++)
                    {
                        dmxAddresses.blueAddresses.Add(new DMXAddress { oldValue = addresses[2].Addresses[i].ToString() });
                    }
                    break;
                case 4:
                    for (int i = 0; i < addresses[0].Addresses.Length; i++)
                    {
                        dmxAddresses.redAddresses.Add(new DMXAddress { oldValue = addresses[0].Addresses[i].ToString() });
                    }
                    for (int i = 0; i < addresses[1].Addresses.Length; i++)
                    {
                        dmxAddresses.greenAddresses.Add(new DMXAddress { oldValue = addresses[1].Addresses[i].ToString() });
                    }
                    for (int i = 0; i < addresses[2].Addresses.Length; i++)
                    {
                        dmxAddresses.blueAddresses.Add(new DMXAddress { oldValue = addresses[2].Addresses[i].ToString() });
                    }
                    for (int i = 0; i < addresses[3].Addresses.Length; i++)
                    {
                        dmxAddresses.whiteAddresses.Add(new DMXAddress { oldValue = addresses[3].Addresses[i].ToString() });
                    }
                    break;
            }


            return dmxAddresses;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
