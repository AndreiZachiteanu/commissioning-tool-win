﻿using commissioning_tool.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace commissioning_tool.Converters
{
    public class MultiParamJsonConverter :JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            iM_NPolicyParameter val = new iM_NPolicyParameter();
            
            if (reader.TokenType == JsonToken.StartObject)
            {
                JObject item = JObject.Load(reader);
                val.Requires = (string)item["requires"];
                val.Delay = Regex.Replace((string)item["delay"], @"[^0-9]", String.Empty); ;

                return val;
            }
            /*
            if (reader.TokenType != JsonToken.StartObject)
            {


                Object so = reader.Value;

                string s = " ";
                var x = reader.Read();
                s = " ";
                var y = reader.Value;
                var w = reader.Value.ToString();
                s = " ";

                var xm = reader.Read();

                s = " ";
                
                JObject jsonObject = JObject.Load(reader);
                if (jsonObject != null)
                {
                    val.intValue = Regex.Replace((string)jsonObject["requires"], @"[^0-9]", String.Empty);
                    val.MultiModeTimeout = Regex.Replace((string)jsonObject["delay"], @"[^0-9]", String.Empty);
                }
                
            }
            */
            /*
            if ((string)reader.Value != null)
            {
                
                JObject jsonObject = JObject.Load(reader);
                if (jsonObject != null)
                {
                    val.intValue = Regex.Replace((string)jsonObject["requires"], @"[^0-9]", String.Empty);
                    val.MultiModeTimeout = Regex.Replace((string)jsonObject["delay"], @"[^0-9]", String.Empty);
                }
                

            }
            */
            // return val;
            return val;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}

