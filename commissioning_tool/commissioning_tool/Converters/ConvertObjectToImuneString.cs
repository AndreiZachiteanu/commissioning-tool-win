﻿using commissioning_tool.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace commissioning_tool.Converters
{
    public static class ConvertObjectToImuneString
    {
        public static string TaskPhase(TaskPhase phase, bool duration = false)
        {
            var returnMessage = "";
            switch (phase.LightType.ToLower())
            {
                case "primitive":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(phase.LightValues, phase.LightType).SCstring.Trim();
                    break;
                case "cct":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(phase.LightValues, phase.LightType).CCTstring.Trim();
                    break;
                case "rgb":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(phase.LightValues, phase.LightType).RGBstring.Trim();
                    break;
                case "rgbx":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(phase.LightValues, phase.LightType).RGBXstring.Trim();
                    break;
            }

            if (duration)
            {
                returnMessage += " abs_" + (TimeSpan.FromHours(phase.Duration.Hours) + TimeSpan.FromMinutes(phase.Duration.Minutes) + TimeSpan.FromSeconds(phase.Duration.Seconds)).TotalMilliseconds;
            }

            returnMessage += " " + phase.TransitionStyle.ToLower();

            return returnMessage;
        }


        public static string ScenePhase(LightValues lightValue, Enums.LightType lightType)
        {
            var returnMessage = "";
            switch (lightType.ToString().ToLower())
            {
                case "primitive":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(lightValue, lightType.ToString()).SCstring.Trim();
                    break;
                case "cct":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(lightValue, lightType.ToString()).CCTstring.Trim();
                    break;
                case "rgb":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(lightValue, lightType.ToString()).RGBstring.Trim();
                    break;
                case "rgbx":
                    returnMessage += LightValueTypeConverter.ConvertLightValuesToImuneString(lightValue, lightType.ToString()).RGBXstring.Trim();
                    break;
            }


            return returnMessage;
        }
    }
}
