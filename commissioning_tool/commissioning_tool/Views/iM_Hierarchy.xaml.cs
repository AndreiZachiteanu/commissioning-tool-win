﻿using commissioning_tool.Connections;
using commissioning_tool.Models;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class iM_Hierarchy : ContentPage
    {
        #region Fields
        private iMune_SSHComs iMComs = iMune_SSHComs.GetInstance();
        
        #endregion


        #region Constructor
        public iM_Hierarchy()
        {
            InitializeComponent();
            var vm = BindingContext as iM_HierarchyViewModel;
            vm.World.ItemList.ContainerName = "Estates";
            vm.World.ItemList.ItemList = iMComs.GetHierarchy_Estates();
            //TranslateToCurrentMenu(World);
            vm.CurrentMenuItem = vm.World;
        }
        #endregion
    }
}