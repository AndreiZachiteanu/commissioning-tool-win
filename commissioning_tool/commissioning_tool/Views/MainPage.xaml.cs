﻿using commissioning_tool.Assets.Dictionary;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public static Assembly Assembly
        {
            get
            {
                return typeof(MainPage).GetTypeInfo().Assembly;
            }

        }
        public MainPage()
        {
            LocalizationResourceManager.Current.Init(AppResources.ResourceManager);
            LocalizationResourceManager.Current.SetCulture(CultureInfo.GetCultureInfo("en"));
            InitializeComponent();


        }

        private void PreCom_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LoginPage());
        }

        private void OnSite_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new iM_LoginPage());
        }
    }
}