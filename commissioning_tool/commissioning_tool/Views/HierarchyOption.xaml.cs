﻿using commissioning_tool.Connections;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HierarchyOption : ContentPage
    {


        #region Constructor
        public HierarchyOption()
        {
            InitializeComponent();

            var vm = BindingContext as HierarchyOptionViewModel;
            vm.Navigation = Navigation;
            

        }
        #endregion
    }
}