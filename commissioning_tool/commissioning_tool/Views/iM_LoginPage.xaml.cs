﻿using commissioning_tool.Connections;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class iM_LoginPage : ContentPage
    {
        private iM_LoginVM vm;

        public iM_LoginPage()
        {
            InitializeComponent();
            vm = BindingContext as iM_LoginVM;
            vm.imLoginM.Navigation = Navigation;
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
            
        }
    }
}