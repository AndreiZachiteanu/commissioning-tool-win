﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using commissioning_tool.ViewModels;

namespace commissioning_tool.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            var vm = BindingContext as LoginVM;
            vm.loginM.Navigation = Navigation;
        }
    }
}
