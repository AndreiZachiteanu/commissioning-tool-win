﻿using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProjectsPage : ContentPage
    {
        public ProjectsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasBackButton(this, false);
            var vm = BindingContext as ProjectsVM;
            vm.projectsM.Navigation = Navigation;
        }
    }
}