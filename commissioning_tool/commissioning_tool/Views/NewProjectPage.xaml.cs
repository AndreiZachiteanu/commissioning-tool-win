﻿using commissioning_tool.Objects;
using commissioning_tool.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewProjectPage : ContentPage
    {
        public NewProjectPage()
        {
            InitializeComponent();
            var vm = BindingContext as NewProjectVM;
            vm.newProjectM.Navigation = Navigation;
        }

        private void DeviceTapped(object sender, ItemTappedEventArgs e)
        {
            var sen = sender;
            var eve = e.Item as DeviceM;
            eve.usingDev = !eve.usingDev;
        }
    }
}