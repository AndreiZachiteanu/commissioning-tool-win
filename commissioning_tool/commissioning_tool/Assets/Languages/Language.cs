﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.Assets.Languages
{
    public class Language
    {
        #region Properties
        public string Name { get; set; }

        public ImageSource Image { get; set; }
        public string LanCode { get; internal set; }
        public bool SelectedLanguage { get; internal set; }
        #endregion
    }
}
