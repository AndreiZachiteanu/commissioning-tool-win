﻿using commissioning_tool.Assets.Dictionary;
using commissioning_tool.Popups;
using commissioning_tool.ViewModels;
using commissioning_tool.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Forms;

namespace commissioning_tool.Assets.Languages
{
    public static class LanguageList
    {
        #region Properties
        public static ObservableCollection<Language> Languages { get; set; } = new ObservableCollection<Language>
        {
            new Language
            {
                Name = "English",
                Image = ImageSource.FromResource("en", MainPage.Assembly),
                LanCode = "en",
                SelectedLanguage = true
            } ,
            new Language 
            {
                Name = "Rom\u00E2n\u0103",
                Image = ImageSource.FromResource("ro", MainPage.Assembly),
                LanCode ="ro"
            }
        

        };

        public static Language CurrentLanguage { get; set; } = new Language();
        #endregion

        #region Commands
        public static Command ChangeLanguage_Command => new Command(async (param) => {

            CurrentLanguage.SelectedLanguage = false;
            CurrentLanguage = (Language)param;
            CurrentLanguage.SelectedLanguage = true;

            await PopupNavigation.Instance.PopAsync();
            LocalizationResourceManager.Current.SetCulture(CultureInfo.GetCultureInfo(CurrentLanguage.LanCode));
           
        });

        public static Command OpenSettingsPop_Command => new Command(async () =>
        {
            SettingsPop c_picker = new SettingsPop();
            //c_picker.BindingContext = this;
            await PopupNavigation.Instance.PushAsync(c_picker);
        });
        #endregion
    }
}
