﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Assets.Languages
{
    [ContentProperty(nameof(Source))]
    public class FlagPointer : IMarkupExtension
    {
        public string Source { get; set; }




        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Source == null)
                return null;

            return ImageSource.FromResource(Source, typeof(FlagPointer).GetType().Assembly);
        }
    }
}
