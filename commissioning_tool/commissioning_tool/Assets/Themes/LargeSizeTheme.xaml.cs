﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace commissioning_tool.Assets.Themes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LargeSizeTheme : ResourceDictionary
    {
        public LargeSizeTheme()
        {
            InitializeComponent();
        }
    }
}