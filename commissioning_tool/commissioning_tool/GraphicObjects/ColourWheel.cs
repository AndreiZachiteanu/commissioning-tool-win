﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;

namespace commissioning_tool.GraphicObjects
{
    public class ColourWheel : SKCanvasView
    {
        #region Attributes
        private int width;
        private int height;
        private SKBitmap bitmap;
        #endregion

        #region Parameters
        public int RedLevel
        {
            get { return (int)GetValue(RedLevelProperty); }
            set { SetValue(RedLevelProperty, value); }
        }
        public int GreenLevel
        {
            get { return (int)GetValue(GreenLevelProperty); }
            set { SetValue(GreenLevelProperty, value); }
        }
        public int BlueLevel
        {
            get { return (int)GetValue(BlueLevelProperty); }
            set { SetValue(BlueLevelProperty, value); }
        }
        #endregion


        #region BindableProperties
        public static readonly BindableProperty RedLevelProperty =
                                BindableProperty.Create("RedLevel",
                                typeof(int),
                                typeof(ColourWheel),
                                defaultBindingMode: BindingMode.TwoWay);

        public static readonly BindableProperty GreenLevelProperty =
                                BindableProperty.Create("GreenLevel",
                                typeof(int),
                                typeof(ColourWheel),
                                defaultBindingMode: BindingMode.TwoWay);

        public static readonly BindableProperty BlueLevelProperty =
                                BindableProperty.Create("BlueLevel",
                                typeof(int),
                                typeof(ColourWheel),
                                defaultBindingMode: BindingMode.TwoWay);
        #endregion



        #region Constructors
        public ColourWheel()
        {
            PaintSurface += ColourWheel_PaintSurface;

            EnableTouchEvents = true;

            Touch += ColourWheel_Touch;
        }

        #endregion

        #region Methods
        private void ColourWheel_Touch(object sender, SKTouchEventArgs args)
        {
            
            switch (args.ActionType)
            {
                case SKTouchAction.Moved:
                    if (args.InContact)
                     {
                        try
                        {
                            if (args.Location.X <= width && args.Location.Y <= height && args.Location.X >= 0 && args.Location.Y >= 0)
                            {
                                SKColor color1 = bitmap.GetPixel(Convert.ToInt32(args.Location.X), Convert.ToInt32(args.Location.Y));
                                if (color1.Alpha != 0)
                                {
                                    RedLevel = color1.Red;
                                    GreenLevel = color1.Green;
                                    BlueLevel = color1.Blue;

                                }
                                else
                                {
                                    args.Handled = false;
                                    args = (SKTouchEventArgs)SKTouchEventArgs.Empty;
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("error 404");
                        }
                    }
                    break;

                case SKTouchAction.Pressed:
                    try
                    {

                        

                        if (args.Location.X <= width && args.Location.Y <= height && args.Location.X >= 0 && args.Location.Y >= 0)
                        {

                            SKColor color1 = bitmap.GetPixel(Convert.ToInt32(args.Location.X), Convert.ToInt32(args.Location.Y));
                            if (color1.Alpha != 0)
                            {
                                RedLevel = color1.Red;
                                GreenLevel = color1.Green;
                                BlueLevel = color1.Blue;
                            }
                            else
                            {
                                args.Handled = false;
                                args = (SKTouchEventArgs)SKTouchEventArgs.Empty;

                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("error bzzz");
                    }
                    break;
            }
            args.Handled = true;
            //((SKCanvasView)sender).InvalidateSurface();
        }

        private void ColourWheel_PaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;
            

            width = info.Width;
            height = info.Height;
            canvas.Clear();
            using (SKPaint paint = new SKPaint())
            {
                using (SKPaint paints = new SKPaint())
                {
                    paints.IsAntialias = true;
                    var size = info.Width < info.Height ? info.Width : info.Height;


                    bitmap = new SKBitmap(size, size);
                    int strokeWidth = (int)(size * .45);
                    float radius = (float)strokeWidth / 2;
                    // Define an array of rainbow colors
                    SKColor[] colors = new SKColor[8];

                    for (int i = 0; i < colors.Length; i++)
                    {
                        colors[i] = SKColor.FromHsl(i * 360f / 7, 100, 50);
                    }

                    //int size = 300;

                    //var bitmap = new SKBitmap(size, size);
                    SKPoint center = new SKPoint(size / 2, size / 2);
                    paint.MaskFilter = SKMaskFilter.CreateBlur(SKBlurStyle.Normal, 0.5f);
                    // Create sweep gradient based on center of canvas
                    paint.Shader = SKShader.CreateSweepGradient(center, colors, null);
                    //paint.Shader = SKShader.CreateRadialGradient(new SKPoint(0,0), 20, new SKColor[] { SKColors.White, SKColors.Gray }, null, SKShaderTileMode.Clamp );

                    // Draw a circle with a wide line

                    paint.Style = SKPaintStyle.Stroke;
                    paint.StrokeWidth = strokeWidth;


                        
                    canvas.DrawCircle(center, radius, paint);

                    paints.Shader = SKShader.CreateRadialGradient(
                    new SKPoint(center.X, center.Y),
                    (int)(size * .4),
                    new SKColor[] { SKColors.White, SKColors.Transparent },
                    null,
                    SKShaderTileMode.Clamp);

                    canvas.DrawCircle(center, size, paints);

                    using (SKCanvas canvas2 = new SKCanvas(bitmap))
                    {

                        canvas2.Clear();
                        canvas2.DrawCircle(center, radius, paint);

                        paints.Shader = SKShader.CreateRadialGradient(
                        new SKPoint(center.X, center.Y),
                        (int)(size * .4),
                        new SKColor[] { SKColors.White, SKColors.Transparent },
                        null,
                        SKShaderTileMode.Clamp);

                        canvas2.DrawCircle(center, size, paints);
                    }
                    
                }
            }
        }
        #endregion
    }
}
