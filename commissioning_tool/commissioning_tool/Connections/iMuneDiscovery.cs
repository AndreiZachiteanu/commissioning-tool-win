﻿using commissioning_tool.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace commissioning_tool.Connections
{
    public class iMuneDiscovery : UdpClient
    {
#region Attributes
        private int port;
        private ObservableCollection<iMuneM> iMuneList;
        private ObservableCollection<IPAddress> DHCP_Addresses { get; set; }
        #endregion

        #region Constructor
        public iMuneDiscovery(int port, ObservableCollection<iMuneM> iMuneList) : base(port)
        {
            this.port = port;
            this.iMuneList = iMuneList;
            StartListening();
            EnableBroadcast = true;
        }
        #endregion

        #region Methods
        private void StartListening()
        {
            //IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);

            try
            {
               
                // byte[] bytes = this.Receive(ref groupEP);

                // Debug.WriteLine($"Received broadcast from {groupEP} :");
                //Debug.WriteLine($" {Encoding.ASCII.GetString(bytes, 0, bytes.Length)}");
                StateM state = new StateM();
                state.workSocket = Client;
                BeginReceive(Received, state);

            }
            catch (SocketException e)
            {
                Console.WriteLine(e);
            }
        }


        private void Received(IAsyncResult ar)
        {
            try
            {
                IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                StateM state = (StateM)ar.AsyncState;

                var message = EndReceive(ar, ref remoteEndPoint);

                if (message.Length > 0)
                {
                    //state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, messageLength));
                    var messageString = (Encoding.UTF8.GetString(message));
                    if (messageString.StartsWith("iMuneSrv;"))
                    {
                        var splitMessage = messageString.Split(';');

                        if (splitMessage.Length == 6)
                        {
                            //Debug.WriteLine(messageString);
                            var iMune = new iMuneM()
                            {
                                name = splitMessage[3],
                                IP = remoteEndPoint.Address.ToString(),
                                Version = splitMessage[4]
                            };


                            var im = (from item in iMuneList where item.IP.Equals(iMune.IP) select item).FirstOrDefault();
                            if (im == null)
                            {
                                for (int i = 0; i < DHCP_Addresses.Count; i++)
                                {
                                    if (iMune.IP == DHCP_Addresses[i].ToString())
                                    {
                                        iMune.isDHCP = true;
                                    }
                                }
                                Xamarin.Essentials.MainThread.BeginInvokeOnMainThread(() =>
                                {
                                    iMuneList.Add(iMune);
                                });
                            }
                        }
                    }
                }

                //Debug.WriteLine(remoteEndPoint.);
                StartListening();

            }catch(Exception ex) { }
            //foreach(IPAddress ip in DHCP_Addresses)
            //{
            //   // Debug.WriteLine(ip.ToString());
            //}
        }


        public void LookforImune() 
        {
            //iMuneList = new ObservableCollection<iMuneM>();
            System.Threading.Tasks.Task.Run(() =>
            {

                NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();
                var callMessage = Encoding.ASCII.GetBytes("iMuneSrv;");
                foreach (NetworkInterface Interface in Interfaces)
                {
                    if (Interface.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                    {
                        if (DHCP_Addresses == null)
                        {
                            DHCP_Addresses = new ObservableCollection<IPAddress>(Interface.GetIPProperties().DhcpServerAddresses.ToList());
                        }
                        else
                        {
                            foreach (var address in Interface.GetIPProperties().DhcpServerAddresses)
                            {
                                var ad = (from item in DHCP_Addresses where item.Address.ToString() == address.Address.ToString() select item).FirstOrDefault();

                                if (ad == null)
                                {
                                    DHCP_Addresses.Add(address);
                                }
                            }

                            //DHCP_Addresses = new ObservableCollection<IPAddress>(DHCP_Addresses.Concat());
                            //foreach (var address in Interface.GetIPProperties().DhcpServerAddresses)
                            //{
                            //    if (!DHCP_Addresses.Contains(address)) DHCP_Addresses.Add(address);
                            //}
                        }
                        UnicastIPAddressInformationCollection UnicastIPInfoCol = Interface.GetIPProperties().UnicastAddresses;
                        foreach (UnicastIPAddressInformation UnicatIPInfo in UnicastIPInfoCol)
                        {
                            if (UnicatIPInfo.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                try
                                {
                                    //UdpClient socket = new UdpClient(22000);
                                    EnableBroadcast = true;
                                    Send(callMessage, callMessage.Length, new IPEndPoint(GetBroadcastAddress(UnicatIPInfo.Address, UnicatIPInfo.IPv4Mask), 11000));
                                    //socket.Close();
                                }catch(Exception ex) { Debug.WriteLine("socket down - IP: " + UnicatIPInfo.Address.AddressFamily); }
                            }
                        }
                    }
                }


            });


        }
        private static IPAddress GetBroadcastAddress(IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
            }
            return new IPAddress(broadcastAddress);
        }
        #endregion
    }
}
