﻿using commissioning_tool.Converters;
using commissioning_tool.Enums;
using commissioning_tool.Models;
using commissioning_tool.PhysicalDevices;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Xamarin.Essentials;
using DeviceType = commissioning_tool.Enums.DeviceType;

namespace commissioning_tool.Connections
{
    public class iMune_SSHComs
    {
        #region Fields
        private static iMune_SSHComs instance;
        private Ssh sClient;

        internal ObservableCollection<Hierarchy> GetHierarchy_Estates()
        {
            var hierarchyReturn = sClient.sendCommand("hierarchy_list_estates");
            ObservableCollection<Hierarchy> hierarchy = new ObservableCollection<Hierarchy>();
            if (hierarchyReturn.Succesfull)
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                hierarchy = new ObservableCollection<Hierarchy>(JsonConvert.DeserializeObject<List<Hier_Estates>>(hierarchyReturn.Result, settings));
            }
            else
            {
                App.Current.MainPage.DisplayAlert("iMune Response", "No response from iMune", "OK");
            }
            return hierarchy;
        }
        #endregion

        #region Properties

        #endregion


        #region Constructors
        private iMune_SSHComs()
        {
            sClient = Ssh.GetInstance();
        }
        #endregion

        #region Methods
        public static iMune_SSHComs GetInstance()
        {
            if (instance == null)
            {
                instance = new iMune_SSHComs();
            }
            return instance;
        }

        public Object GetLights(string id = null)
        {

            if (!sClient.IsConnected()) sClient.Reconnect();
            if (id == null)
            {
                var result = sClient.sendCommand("get_lights all");
                if (result.Succesfull)
                {
                    ObservableCollection<Device> lightList = CreateUILightList(result.Result.Split('\n'));

                    return lightList;
                }
                else
                {
                    return new ObservableCollection<Device>();
                }
            }
            else
            {
                var lightStr = sClient.sendCommand("get_lights " + id);//.Split('\n');
                if (lightStr.Succesfull)
                {
                    return lightStr.Result.Split('\n');
                }
                else
                {
                    return new string[] { "" };
                }
            }
        }

        public (bool Created, string Id) CreateHierarchy(string name, HierarchyMenuItem hierChildType, string id = "")
        {
            bool succesfull = false;
            //string newHierId = "";
            string returnedId = "";
            if(hierChildType == HierarchyMenuItem.ESTATE)
            {
                var result = sClient.sendCommand("create_hierarchy_entry " + hierChildType + " " + name);
                succesfull = result.Succesfull;
                if (succesfull)
                {
                    returnedId = result.Result.Substring(result.Result.IndexOf("ID:") + 3);
                }
            }
            else
            {
                var hierarchy = sClient.sendCommand("create_hierarchy_entry " + hierChildType + " " + name);
                if (hierarchy.Succesfull)
                {
                    returnedId = hierarchy.Result.Substring(hierarchy.Result.IndexOf("ID:") + 3);


                    switch (hierChildType)
                    {
                        case HierarchyMenuItem.CAMPUS:
                            var result = sClient.sendCommand("add_" + HierarchyMenuItem.ESTATE + "_item " + id + " " + returnedId);
                            succesfull = result.Succesfull;
                            break;
                        case HierarchyMenuItem.BUILDING:
                            var result2 = sClient.sendCommand("add_" + HierarchyMenuItem.CAMPUS + "_item " + id + " " + returnedId);
                            succesfull = result2.Succesfull;
                            break;
                        case HierarchyMenuItem.FLOOR:
                            var result3 = sClient.sendCommand("add_" + HierarchyMenuItem.BUILDING + "_item " + id + " " + returnedId);
                            succesfull = result3.Succesfull;
                            break;
                        case HierarchyMenuItem.ZONE:
                            var result4 = sClient.sendCommand("add_" + HierarchyMenuItem.FLOOR + "_item " + id + " zone " + returnedId);
                            succesfull = result4.Succesfull;
                            break;
                    }


                    if (succesfull)
                    {
                        //returnedId = result.Result.Substring(result.Result.IndexOf("ID:") + 3);
                    }
                }

            }

            return (succesfull, returnedId);      
        }

        internal ScheduleActions GetActions()
        {
            var groups = ProcessAction(LogicalTypeEnum.TASK_GROUP, sClient.sendCommand("list_task_groups"));
            var tasks = ProcessAction(LogicalTypeEnum.TASK, sClient.sendCommand("list_tasks"));
            var scenes = ProcessAction(LogicalTypeEnum.SCENE, sClient.sendCommand("list_scenes"));

            var obsActionlist = new ScheduleActions();
            obsActionlist.tasks = (ObservableCollection<Actions>)tasks;
            obsActionlist.scenes = (ObservableCollection<Actions>)scenes;


            return obsActionlist;
        }


        private object ProcessAction(LogicalTypeEnum type, (string Result, bool Succesfull) p)
        {
            if (p.Succesfull)
            {
                var listSplit = p.Result.Split(',');



                var actionList = new ObservableCollection<Actions>();
                for (int i = 0; i < listSplit.Length; i++)
                {

                    string[] actionSplit;

                    if (type != LogicalTypeEnum.TASK)
                    {
                        actionSplit = listSplit[i].Split('_');


                    }
                    else
                    {
                        actionSplit = listSplit[i].Split(':');
                    }
                    actionList.Add(new Actions
                    {
                        name = actionSplit[1],
                        id = actionSplit[0],
                        type = type
                    });
                }


                return actionList;
            }
            return null;
        }

        internal bool AddZoneItem(string hierItemType, string itemId, string hierarchyID)
        {
            switch (hierItemType) 
            {
                case "Light":
                    var result = sClient.sendCommand("add_zone_item " + hierarchyID + " light " + itemId);

                    return result.Succesfull;
                case "LightGroup":
                    var result2 = sClient.sendCommand("add_zone_item " + hierarchyID + " lightgroup " + itemId);
                    return result2.Succesfull;
            }
            return false;
        }

        internal bool GetCommissioningMode()
        {
            try
            {
                return JsonConvert.DeserializeObject<iM_CommissioningDetails>(sClient.sendCommand("get_commissioning_details").Result).Commissioning;
            }catch(Exception ex)
            {
                App.Current.MainPage.DisplayAlert("Error!", "Json Error! Commissioning Mode", "Ok");
                return false;
            }
        }

        internal ObservableCollection<iM_newPolicyM> GetPolicies()
        {
            var policies = sClient.sendCommand("list_policies");
            if (policies.Succesfull)
            {
                return JsonConvert.DeserializeObject<ObservableCollection<iM_newPolicyM>>(policies.Result);

            }
            else
            {
                return new ObservableCollection<iM_newPolicyM>();
            }
        }

        public void ChangeLightLevel(Light light, bool onOff = false)
        {
            //if (!sClient.IsConnected()) sClient.Reconnect();
            string result = "";
            //string rgbColourON = light.RGB.ToHex().ToString().Replace("#", "").ToLower();
            // sClient.sendCommand("")
            switch (light.type)
            {
                case LightType.PRIMITIVE:
                    result = sClient.sendCommand("set_light_value"  
                        + LightValueTypeConverter.ConvertLightValuesToImuneString(new LightValues { SCIntensity = light.intensity }, "prim").SCstring 
                        + light.ID 
                        + " Zet").Result;
                    break;
                case LightType.CCT:
                    LightValues lightValue = new LightValues();
                    lightValue.CCTIntensity = light.CCTIntensity;
                    lightValue.CCTTemperature = light.CCTTemperature;
                    var cct = LightValueTypeConverter.ConvertLightValuesToImuneString(lightValue, "cct").CCTstring;                    
                    result = sClient.sendCommand("set_light_value" + cct + light.ID + " Zet").Result;
                        break;
                case LightType.RGB:
                    //var rgb = " rgb_" + Int32.Parse(rgbColourON, System.Globalization.NumberStyles.HexNumber).ToString();
                    var sendingMessage = ("set_light_value"
                         + LightValueTypeConverter.ConvertLightValuesToImuneString(new LightValues
                         {
                             RValue = light.RValue,
                             BValue = light.BValue,
                             GValue = light.GValue
                         }, "rgb").RGBstring
                         + light.ID
                         + " Zet");
                        result = sClient.sendCommand(sendingMessage).Result;                    
                    break;
                case LightType.RGBX:
                    //var rgbx = " rgb_" + Int32.Parse(rgbColourON, System.Globalization.NumberStyles.HexNumber).ToString() + "_" + light.xValue;
                    var sendingMessage2 = ("set_light_value"
                        + LightValueTypeConverter.ConvertLightValuesToImuneString(new LightValues
                        {
                            RValueX = light.RValue,
                            BValueX = light.BValue,
                            GValueX = light.GValue,
                            xValue = light.xValue

                        }, "rgbx").RGBXstring
                        + light.ID
                        + " Zet");
                    result = sClient.sendCommand(sendingMessage2).Result;
                    break;

            }
        }

        internal bool RemoveHierarchyItem(string deleteWhatId, string deleteFromId, HierarchyMenuItem deleteWhat, HierarchyMenuItem deleteFrom, bool specified = false)
        {

            if (specified)
            {
                return false;
            }
            else
            {
                var x = sClient.sendCommand("remove_" + deleteFrom + "_item " + deleteFromId + " " + deleteWhatId);
                return x.Succesfull;
            }
            return false;
        }

        internal bool DeleteHierarchyEntry(HierarchyMenuItem hierType, string id)
        {
            return sClient.sendCommand("delete_hierarchy_entry " + hierType + " " + id).Succesfull;
        }

        internal bool ChangeHierName(string name, HierarchyMenuItem hierType, string id)
        {
            return sClient.sendCommand("rename_hierarchy_entry " + hierType + " " + id + " " + name).Succesfull;
        }

        internal (string Result, bool Succesfull) DeleteSchedule(string id)
        {
            return sClient.sendCommand("delete_schedule " + id);
        }

        internal void IdentifySensor(string param, bool phase)
        {
            MainThread.BeginInvokeOnMainThread(() =>
            {
                var result = sClient.sendCommand("identify_sensor_new " + param + (phase ? " 1" : " 0"));
            });
        }

        internal ObservableCollection<Device> GetSensors()
        {
            if (!sClient.IsConnected()) sClient.Reconnect();
            var result = sClient.sendCommand("get_sensors");


            if (result.Succesfull)
            {
                //var from = result.IndexOf("OK - \n");
                //var from = "OK - \n";
                var sensors = result.Result;// result.Substring(from.Length);
                //var x = new ObservableCollection<Device>(JsonConvert.DeserializeObject<ObservableCollection<Sensor>>(sensors));
                if (!String.IsNullOrEmpty(sensors))
                {

                    return new ObservableCollection<Device>(JsonConvert.DeserializeObject<ObservableCollection<Sensor>>(sensors));
                }
                else
                {
                    return null;
                }
                //JObject some = new JObject(@""+something);

            }
            return new ObservableCollection<Device>();

        }

        internal bool ChangeCommissioningMode(bool iMuneCommissioningMode)
        {
            return sClient.sendCommand("enable_commissioning_mode " + iMuneCommissioningMode).Succesfull;
        }

        internal bool DeleteGroup(iM_LightGroup param)
        {
            return sClient.sendCommand("delete_light_group " + param.id).Succesfull;
        }

        internal void TriggerSensor(string iD, string param, int state)
        {
            string x = "";
            sClient.sendCommand("debug_sensor_trigger " + iD + "_" + param + " " + state);
        }

        internal (bool Create, string ID) CreateGroup(string param)
        {
            var result = sClient.sendCommand("create_light_group " + param);
            bool created = result.Succesfull;
            var id = result.Result.Substring(result.Result.IndexOf("ID:") + 3).Trim();
            return (created, id);
        }

        internal (string Result, bool Succesfull) EnableSchedule(string id, string enab)
        {
            return sClient.sendCommand("set_schedule_enabled " + id + " " + enab);
        }

        public ObservableCollection<iM_LightGroup> GetGroups()
        {
            ObservableCollection<iM_LightGroup> list = new ObservableCollection<iM_LightGroup>();
            var result = sClient.sendCommand("get_light_groups");
            if (result.Succesfull)
            {
                var resultArray = result.Result.Split('\n');

                for (int i = 0; i < resultArray.Length; i++)
                {
                    var group = resultArray[i].Split('_');
                    if (group.Length > 1)
                    {

                        var groupLights_result = sClient.sendCommand("get_light_group_lights " + group[0]);//.Split('\n');
                        if (groupLights_result.Succesfull)
                        {

                            var groupLightResSplit = groupLights_result.Result.Split('\n');
                            ObservableCollection<Light> groupLights = new ObservableCollection<Light>();
                            for (int y = 0; y < groupLightResSplit.Length; y++)
                            {
                                var id = groupLightResSplit[y].Split('_')[0];
                                if (id.Length > 2)
                                {
                                    groupLights.Add(CreateUILight(id));
                                }

                            }

                            list.Add(new iM_LightGroup()
                            {
                                id = group[0],
                                groupLights = groupLights,
                                groupName = group[1]
                            }); ;
                        }
                        else
                        {
                            list.Add(new iM_LightGroup()
                            {
                                id = group[0],
                                groupLights = new ObservableCollection<Light>(),
                                groupName = group[1]
                            });
                        }
                    }

                }
            }
            return list;
        }

        internal (string Result, bool Succesfull) SetScheduleName(string id, string name)
        {
            return sClient.sendCommand("set_schedule_name " + id + " " + name);
        }

        internal (string Result, bool Succesfull) CreateSchedule(string name)
        {
            return sClient.sendCommand("create_schedule " + name);
        }

        internal (string Result, bool Succesfull) ExecuteSchedule(string id)
        {
            return sClient.sendCommand("execute_schedule_now " + id);
        }

        public bool ChangeLightDMXAddresses(Light currentDevice)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();
             

            switch (currentDevice.type)
            {
                case LightType.PRIMITIVE:
                    var primChannel = " 1_";
                    var listPrimChannels = new List<string>();

                    primChannel += AddDMXAddressesToString(currentDevice.dmxAddress.primAddresses);

                    return sClient.sendCommand("set_light_dmx_addresses " + currentDevice.ID + primChannel).Succesfull;
                case LightType.CCT:
                    string warmChannel = " 1_";


                    warmChannel += AddDMXAddressesToString(currentDevice.dmxAddress.warmAddresses);

                    string coolChannel = " 2_";


                    coolChannel += AddDMXAddressesToString(currentDevice.dmxAddress.coolAddresses);

                    return sClient.sendCommand("set_light_dmx_addresses " + currentDevice.ID + warmChannel + coolChannel).Succesfull;
                   
                case LightType.RGB:
                    string redChannel = " 1_";

                    redChannel += AddDMXAddressesToString(currentDevice.dmxAddress.redAddresses);


                    string greenChannel = " 2_";
                    greenChannel += AddDMXAddressesToString(currentDevice.dmxAddress.greenAddresses);

                    string blueChannel = " 3_";
                    blueChannel+= AddDMXAddressesToString(currentDevice.dmxAddress.blueAddresses);


                    return sClient.sendCommand("set_light_dmx_addresses " + currentDevice.ID + redChannel + greenChannel + blueChannel).Succesfull;                  
                case LightType.RGBX:
                    string redChannelx = " 1_";

                    redChannelx += AddDMXAddressesToString(currentDevice.dmxAddress.redAddresses);

                    string greenChannelx = " 2_";
                    greenChannelx += AddDMXAddressesToString(currentDevice.dmxAddress.greenAddresses);

                    string blueChannelx = " 3_";
                    blueChannelx += AddDMXAddressesToString(currentDevice.dmxAddress.blueAddresses);

                    string xChannel1 = " 4_";
                    greenChannelx += AddDMXAddressesToString(currentDevice.dmxAddress.whiteAddresses);

                    return sClient.sendCommand("set_light_dmx_addresses " + currentDevice.ID + redChannelx + greenChannelx + blueChannelx + xChannel1).Succesfull;                    
            }

            return false;
        }

        private string AddDMXAddressesToString(ObservableCollection<DMXAddress> addresses)
        {

            var listChannels = new List<string>();
            for (int i = 0; i < addresses.Count; i++)
            {
                if (!String.IsNullOrEmpty(addresses[i].newValue))
                {
                    listChannels.Add(addresses[i].newValue);
                }
                else
                {
                    if (!addresses[i].oldValue.Equals("0"))
                    {
                        listChannels.Add(addresses[i].oldValue);
                    }
                }

                // String.IsNullOrEmpty(currentDevice.dmxAddress.primAddresses[i].newValue)? currentDevice.dmxAddress.primAddresses[i].oldValue : currentDevice.dmxAddress.primAddresses[i].newValue + ",";
            }
            return String.Join(",", listChannels);
        }

        public bool IdentifyDMX(int param, int universe)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();
            //Debug.WriteLine(param);
            return sClient.sendCommand("identify_dmx " + universe + " " + param).Succesfull;
        }

        public (bool Created, string id) CreatePRIMLight(Light light,
                                                        List<ChSelModel> selChannels,
                                                        iM_LightGroup selectedGroup,
                                                        List<UniverseList> universe)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            List<string> channelList = new List<string>();
            string channels;


            for (int i = 0; i < selChannels.Count; i++)
            {
                channelList.Add(selChannels[i].channelNo + "_" + selChannels[i].channelNo);
            }
            channels = String.Join("\\*", channelList);
            var message = "create_light " + light.type + "/" + universe[0].universe + "/0/false/" + light.defName + "/" + light.custName.Replace(' ', '_') + "/" + "defadmin*imune/" + channels;

            //result.Substring("OK -".Length).Trim();

            var result = sClient.sendCommand("create_light " + light.type + "/" + universe[0].universe + "/0/" + light.custName.Replace(' ', '_') + "/" + channels);
            var id = result.Result.Substring(result.Result.IndexOf("ID:") + 3).Trim();
            return (result.Succesfull, id);
        }

        internal (string Result, bool Succesfull) SetScheduleDays(string id, string days)
        {
            return sClient.sendCommand("set_schedule_days " + id + days);
        }

        internal (string Result, bool Succesfull) SetScheduleTime(string id, TimeSpan time)
        {
            return sClient.sendCommand("set_schedule_time " + id + " " + time.ToString(@"hh\:mm\:ss"));
        }

        internal (string Result, bool Succesfull) SetScheduleAction(string id, string messageToSend)
        {
            return sClient.sendCommand("set_schedule_actions " + id + " " + messageToSend);
        }

        public (bool Created, string id) CreateCCTLight(Light light,
                                                        List<ChSelModel> warmSelChannels,
                                                        List<ChSelModel> coolSelModels,
                                                        iM_LightGroup selectedGroup,
                                                        List<UniverseList> universe)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            List<string> warmChannelList = new List<string>();
            List<string> coolChannelList = new List<string>();
            string warmChannels; 
            string coolChannels;

            for (int i = 0; i < warmSelChannels.Count; i++)
            {
                warmChannelList.Add(warmSelChannels[i].channelNo + "_" + warmSelChannels[i].channelNo);
            }

            for (int i = 0; i < coolSelModels.Count; i++)
            {
                coolChannelList.Add(coolSelModels[i].channelNo + "_" + coolSelModels[i].channelNo);
            }

            warmChannels = String.Join("\\*", warmChannelList);
            coolChannels = String.Join("\\*", coolChannelList);
            var message =  warmChannels +"/" + coolChannels +"/warm_0/cool_0";

            //result.Substring("OK -".Length).Trim();

            var result = sClient.sendCommand("create_light " + light.type + "/" + universe[0].universe + "/0/" + light.custName.Replace(' ', '_') + "/" + message);
            var id = result.Result.Substring(result.Result.IndexOf("ID:") + 3).Trim();
            return (result.Succesfull, id);
        }
        internal (bool Created, string id) CreateRGBLight(Light light, 
                                                            List<ChSelModel> redSelModels,
                                                            List<ChSelModel> greenSelModels, 
                                                            List<ChSelModel> blueSelModels, 
                                                            iM_LightGroup selectedGroup, 
                                                            List<UniverseList> universe)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            List<string> redChannelList = new List<string>();
            List<string> greenChannelList = new List<string>();
            List<string> blueChannelList = new List<string>();
            string redChannels;
            string greenChannels;
            string blueChannels;


            for (int i = 0; i < redSelModels.Count; i++)
            {
                redChannelList.Add(redSelModels[i].channelNo + "_" + redSelModels[i].channelNo);
            }

            for (int i = 0; i < greenSelModels.Count; i++)
            {
                greenChannelList.Add(greenSelModels[i].channelNo + "_" + greenSelModels[i].channelNo);
            }

            for (int i = 0; i < blueSelModels.Count; i++)
            {
                blueChannelList.Add(blueSelModels[i].channelNo + "_" + blueSelModels[i].channelNo);
            }

            redChannels = String.Join("\\*", redChannelList);
            greenChannels = String.Join("\\*", greenChannelList);
            blueChannels = String.Join("\\*", blueChannelList);


            var message = redChannels + "/" + greenChannels + "/" + blueChannels;

            //result.Substring("OK -".Length).Trim();

            var result = sClient.sendCommand("create_light " + light.type + "/" + universe[0].universe + "/0/" + light.custName.Replace(' ', '_') + "/" + message);
            var id = result.Result.Substring(result.Result.IndexOf("ID:") + 3).Trim();
            return (result.Succesfull, id);
        }

        internal (bool Created, string id) CreateRGBXLight(Light light,
                                                            List<ChSelModel> redSelModels,
                                                            List<ChSelModel> greenSelModels,
                                                            List<ChSelModel> blueSelModels,
                                                            List<ChSelModel> xChSelModels,
                                                            iM_LightGroup selectedGroup,
                                                            List<UniverseList> universe,
                                                            string xChannelName)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            List<string> redChannelList = new List<string>();
            List<string> greenChannelList = new List<string>();
            List<string> blueChannelList = new List<string>();
            List<string> xChChannelList = new List<string>();
            string redChannels;
            string greenChannels;
            string blueChannels;
            string xChChannels;


            for (int i = 0; i < redSelModels.Count; i++)
            {
                redChannelList.Add(redSelModels[i].channelNo + "_" + redSelModels[i].channelNo);
            }

            for (int i = 0; i < greenSelModels.Count; i++)
            {
                greenChannelList.Add(greenSelModels[i].channelNo + "_" + greenSelModels[i].channelNo);
            }

            for (int i = 0; i < blueSelModels.Count; i++)
            {
                blueChannelList.Add(blueSelModels[i].channelNo + "_" + blueSelModels[i].channelNo);
            }

            for (int i = 0; i < xChSelModels.Count; i++)
            {
                xChChannelList.Add(xChSelModels[i].channelNo + "_" + xChSelModels[i].channelNo);
            }

            redChannels = String.Join("\\*", redChannelList);
            greenChannels = String.Join("\\*", greenChannelList);
            blueChannels = String.Join("\\*", blueChannelList);
            xChChannels = String.Join("\\*", xChChannelList);

            string xChName = String.IsNullOrEmpty(xChannelName) ? "X_Channel" : xChannelName;
            var message = redChannels + "/" + greenChannels + "/" + blueChannels + "/" + xChChannels+ "/xname_" + xChName;

            //result.Substring("OK -".Length).Trim();
            int universeItem = 0;
            if (universe.Count > 0) universeItem = universe[0].universe;
            var result = sClient.sendCommand("create_light " + light.type + "/" + universeItem + "/0/" + light.custName.Replace(' ', '_') + "/" + message);
            var id = result.Result.Substring(result.Result.IndexOf("ID:") + 3).Trim();
            return (result.Succesfull, id);
        }

        public bool IdentifyLightDMX(string universe, List<int> channels)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            return sClient.sendCommand("identify_dmx " + universe + " " + String.Join(",", channels)).Succesfull;
        }

        private ObservableCollection<Device> CreateUILightList(string[] lightsArray)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            ObservableCollection<Device> list = new ObservableCollection<Device>();
            for (int i = 0; i < lightsArray.Length; i++)
            {
                var getLight = CreateUILight(lightsArray[i].Split(',')[0]);
                if (getLight != null)
                {
                    list.Add(getLight);
                }
            }
            return list;
        }



        public bool AddLightToGroup(string groupID, string lightID)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            return sClient.sendCommand("add_light_group_lights " + groupID + " " + lightID).Succesfull;
        }

        private Light CreateUILight(string lightId)
        {
            if (!sClient.IsConnected()) sClient.Reconnect();

            if (!String.IsNullOrEmpty(lightId.Trim()))
            {

                var lightStr = (string[])GetLights(lightId);



                if (!String.IsNullOrEmpty(lightStr[0]) && !lightStr[0].StartsWith("FAIL") && !lightStr[0].Equals("\r") && !lightStr[0].Equals("-1-2-3-4-5-6-7-8"))
                {

                    var type = LightString.LightStringToType(lightStr[1].Split(':')[1].Trim());
                    LightDMXAddress dMXAdress = new LightDMXAddress();


                    var stringSplit = lightStr[9].Split(';');

                    List<ObservableCollection<DMXAddress>> listOfAddresses = new List<ObservableCollection<DMXAddress>>();


                    for (int i = 0; i < stringSplit.Length; i++)
                    {
                        if (stringSplit[i].Length > 1)
                        {
                            ObservableCollection<DMXAddress> addresses = new ObservableCollection<DMXAddress>();
                            var from = stringSplit[i].IndexOf("course:") + 7;
                            var to = stringSplit[i].IndexOf(",fine:");
                            if ((to - from) > 0)
                            {
                                var howDoesItLook = stringSplit[i].Substring(from, to - from);
                                var address = howDoesItLook.Split(',');
                                for (int y = 0; y < address.Length; y++)
                                {
                                    int addressNo = 0;
                                    if (Int32.TryParse(address[y], out addressNo))
                                    {
                                        var addressDMXObj = new DMXAddress {
                                            oldValue = addressNo.ToString() };
                                        addresses.Add(addressDMXObj);
                                    }

                                }
                                listOfAddresses.Add(addresses);
                            }
                            else
                            {
                                var addressDMXObj = new DMXAddress {
                                    oldValue = "0" };
                                addresses.Add(addressDMXObj);
                                listOfAddresses.Add(addresses);
                            }
                        }
                    }


                    if (listOfAddresses.Count > 0)
                    {
                        switch (type)
                        {
                            case LightType.PRIMITIVE:
                                dMXAdress.primAddresses = listOfAddresses[0];
                                break;
                            case LightType.CCT:
                                dMXAdress.warmAddresses = listOfAddresses[0];
                                dMXAdress.coolAddresses = listOfAddresses[1];
                                break;
                            case LightType.RGB:
                                dMXAdress.redAddresses = listOfAddresses[0];
                                dMXAdress.greenAddresses = listOfAddresses[1];
                                dMXAdress.blueAddresses = listOfAddresses[2];
                                break;
                            case LightType.RGBX:
                                dMXAdress.redAddresses = listOfAddresses[0];
                                dMXAdress.greenAddresses = listOfAddresses[1];
                                dMXAdress.blueAddresses = listOfAddresses[2];
                                dMXAdress.whiteAddresses = listOfAddresses[3];
                                break;
                        }
                        if(type == LightType.RGBX)
                        {
                            string s = "";
                        }


                        string xchannelName = String.IsNullOrEmpty(lightStr[10].Split(':')[1].Trim()) ? "xChannel" : lightStr[10].Split(':')[1].Trim();
                        var light = new Light()
                        {
                            //type = LightString.LightStringToType(lightStr[0].Substring(0, lightStr[0].IndexOf('-'))),

                            ID = lightStr[0].Split(':')[1].Trim(),
                            type = type,
                            subUni = Convert.ToInt32(lightStr[2].Split(':')[1]),
                            net = Convert.ToInt32(lightStr[3].Split(':')[1]),
                            universe = Convert.ToInt32(lightStr[4].Split(':')[1]),
                            is16Bit = Convert.ToBoolean(lightStr[5].Split(':')[1]),
                            defName = lightStr[6].Split(':')[1],
                            custName = lightStr[7].Split(':')[1],
                            xChannelName = xchannelName,
                            warmTemp = Convert.ToInt32(lightStr[11].Split(':')[1]),
                            coolTemp = Convert.ToInt32(lightStr[12].Split(':')[1]),
                            devType = DeviceType.light,
                            dmxAddress = dMXAdress
                        };
                        return light;
                    }
                }
                else
                {
                    // Debug.WriteLine(lightStr);
                }
            }
            return null;
        }

        internal void ChangeSensorName(string iD, string param)
        {
            sClient.sendCommand("set_sensor_name " + iD + " " + param);
        }

        internal void IdentifyLight(string iD)
        {
            sClient.sendCommand("identify_lights " + iD);
        }

        public void DeletePolicies(ObservableCollection<iM_newPolicyM> policyList)
        {
            string list = "";


            for (int i = 0; i < policyList.Count; i++)
            {
                if (policyList[i].selected)
                {
                    list += " " + policyList[i].ID;
                }
            }
            sClient.sendCommand("delete_policy" + list);

        }

        public void CreatePolicy(iM_newPolicyM newPolicy, ObservableCollection<iM_PolSeqM> polSeqToggle, ObservableCollection<Device> itemList)
        {
            var received = sClient.sendCommand("create_policy " + newPolicy.type + " " + newPolicy.name + " : " + newPolicy.description);
            string ID;
            if (received.Succesfull || !String.IsNullOrEmpty(newPolicy.ID))
            {

                if (!String.IsNullOrEmpty(newPolicy.ID))
                {
                    ID = newPolicy.ID;
                }
                else
                {
                    ID = received.Result.Substring(received.Result.LastIndexOf(":") + 1);
                }

                

                sClient.sendCommand("set_policy_enabled " + ID + " " + newPolicy.Enabled.ToString().ToLower());

                sClient.sendCommand("set_policy_priority " + ID + " " + newPolicy.priority);

                string lightList = "";

                string sensorList = "";

                for (int i = 0; i < itemList.Count; i++)
                {
                    switch (itemList[i].devType)
                    {
                        case DeviceType.light:
                            lightList += " " + itemList[i].ID;
                            break;
                        case DeviceType.sensor:

                            for (int y = 0; y < ((Sensor)itemList[i]).triggerEvents.Count; y++)
                            {
                                var trigger = ((Sensor)itemList[i]).triggerEvents[y];


                                for (int z = 0; z < trigger.Trigger.Count; z++)
                                {
                                    if (trigger.Trigger[z].used)
                                    {
                                        sensorList += " " + ((Sensor)itemList[i]).ID + "_" + trigger.Trigger[z].value;

                                        if (newPolicy.type == PolicyType.RAISEDIM || newPolicy.type == PolicyType.STEPDIM)
                                        {
                                            if (trigger.Trigger[z].sliderValue != 0)
                                                sClient.sendCommand("set_policy_sensor_rates " + ID + " " + ((Sensor)itemList[i]).ID + "_" + trigger.Trigger[z].value + ":" + trigger.Trigger[z].sliderValue);
                                        }

                                    }
                                }

                            }


                            break;
                    }
                }


                if (lightList.Length > 10) sClient.sendCommand("set_policy_lights " + ID + lightList);

                if (sensorList.Length > 3)
                {
                    var result = sClient.sendCommand("set_policy_sensors " + ID + sensorList).Result;
                }

                var onValues = LightValueTypeConverter.ConvertLightValuesToImuneString(newPolicy.OnValues);
                var offValues = LightValueTypeConverter.ConvertLightValuesToImuneString(newPolicy.OffValues);
                var interValues = LightValueTypeConverter.ConvertLightValuesToImuneString(newPolicy.InterimValues);


                for (int i = 0; i < polSeqToggle.Count; i++)
                {
                    if (polSeqToggle[i].used == true)
                    {
                        switch (polSeqToggle[i].cID)
                        {
                            case "onSeq":
                                sClient.sendCommand("add_policy_sequence " + ID + " on " + Convert.ToInt32(newPolicy.OnValues.duration.intValue) + " " + newPolicy.OnValues.transition.text +
                                onValues.SCstring +
                                onValues.CCTstring +
                                onValues.RGBstring +
                                onValues.RGBXstring);
                                break;
                            case "offSeq":
                                sClient.sendCommand("add_policy_sequence " + ID + " off " + Convert.ToInt32(newPolicy.OffValues.duration.intValue) + " " + newPolicy.OffValues.transition.text +
                                offValues.SCstring +
                                offValues.CCTstring +
                                offValues.RGBstring +
                                offValues.RGBXstring);
                                break;
                            case "interimSeq":
                                sClient.sendCommand("add_policy_sequence " + ID + " interim " + Convert.ToInt32(newPolicy.InterimValues.duration.intValue) + " " + newPolicy.InterimValues.transition.text +
                                interValues.SCstring +
                                interValues.CCTstring +
                                interValues.RGBstring +
                                interValues.RGBXstring);


                                sClient.sendCommand("set_policy_delay " + ID + " interim " + (newPolicy.InterimValues.delay.intValue == null ? "0" : newPolicy.InterimValues.delay.intValue));
                                break;
                            case "parameter":


                                if (newPolicy.IntensityCapMax.enabled)
                                {
                                    if (newPolicy.IntensityCapMax.value > 0)
                                    {
                                        sClient.sendCommand("set_policy_intensity_cap " + ID + " max " + newPolicy.IntensityCapMax.value);
                                    }
                                }


                                if (newPolicy.IntensityCapMin.enabled)
                                {
                                    if (newPolicy.IntensityCapMin.value >= 0)
                                    {
                                        sClient.sendCommand("set_policy_intensity_cap " + ID + " min " + newPolicy.IntensityCapMin.value);
                                    }
                                }


                                if (newPolicy.MultiModeOn.enabled)
                                {
                                    if (newPolicy.MultiModeOn.intValue != null)
                                    {
                                        sClient.sendCommand("add_policy_multimode " + ID + " on " + newPolicy.MultiModeOn.Requires + " " + newPolicy.MultiModeOn.Delay);
                                    }
                                }



                                if (newPolicy.MultiModeOff.enabled)
                                {
                                    if (newPolicy.MultiModeOff.intValue != null)
                                    {
                                        sClient.sendCommand("add_policy_multimode " + ID + " off " + newPolicy.MultiModeOff.Requires + " " + newPolicy.MultiModeOff.Delay);
                                    }
                                }


                                break;
                            case "activation":
                                var sendingString = "add_policy_activation " + ID + " time";
                                bool sending = false;

                                for(int x = 0; x<  newPolicy.ActivationParam.Count; x++)
                                {
                                    var day = newPolicy.ActivationParam[x];
                                    if (day.use)
                                    {
                                        sending = true;
                                        sendingString += " " + (int)day.DayName + "_";
                                       // sendingString += " " + ((int)day.DayName).ToString() + "_" + day.StartTime.ToString(@"hh\:mm\:ss");
                                        for (int y =0; y < day.times.Count; y++)
                                        {
                                            if (y != 0) sendingString += "-";
                                            sendingString += day.times[y].StartTime.ToString(@"hh\:mm\:ss") + "," + day.times[y].EndTime.ToString(@"hh\:mm\:ss");
                                        }
                                    }
                                }

                                if (sending)
                                {
                                    sClient.sendCommand(sendingString);
                                    
                                }

                                break;
                        }
                    }


                }
                if (newPolicy.Delay.enabled)
                {

                        if (!String.IsNullOrEmpty(newPolicy.Delay.text))
                        {
                            var something = sClient.sendCommand("set_policy_delay " + ID + " " + "off " + newPolicy.Delay.text);
                        }

                    
                }




            }
        }

        internal void LogicalAction(LogicalTypeEnum type, string id, LogicalActEnum Act)
        {
            sClient.sendCommand(Act.ToString().ToLower() + "_" + type.ToString() + " " + id);
        }

        internal ObservableCollection<iM_TaskModel> ListTasks()
        {
            
            var result = sClient.sendCommand("list_tasks_json");
            if (result.Succesfull)
            {
                return JsonConvert.DeserializeObject<ObservableCollection<iM_TaskModel>>(result.Result);
            }
            return new ObservableCollection<iM_TaskModel>(); 
        }

        internal void DeleteLights(ObservableCollection<Device> lightList)
        {


            var lights = lightList.Where(y => y.selected).ToList();
            var lightString = "";
            for (int i = 0; i < lights.Count; i++)
            {
                lightString += lights[i].ID + ",";
            }

            sClient.sendCommand("delete_lights " + lightString);
        }

        internal iM_newPolicyM GetPolicyDetails(string id)
        {
            var policyDetails = sClient.sendCommand("get_policy_details " + id);



            if (policyDetails.Succesfull)
            {
                return JsonConvert.DeserializeObject<iM_newPolicyM>(policyDetails.Result);
            }


            return new iM_newPolicyM();
        }

        public (LightType type, LightValues values) GetLightValue(string id)
        {

            /*
            var prime = " prim_" + ((int)(((double)lValues.SCIntensity) )).ToString().ToLower();


            double divider = (((double)lValues.CCTIntensity) / 100) == 0 ? 1 : (((double)lValues.CCTIntensity) / 100);

            int warmCh = (int)((255 - lValues.CCTTemperature) * divider);
            int coolCh = (int)(lValues.CCTTemperature * divider);

            var cct = " cct_" + warmCh + "_" + coolCh;

            string rgbColourON = lValues.RGB.ToHex().ToString().Replace("#", "").ToLower();
            string rgbColourONX = lValues.RGBX.ToHex().ToString().Replace("#", "").ToLower();
            var rgb = " rgb_" + Int32.Parse(rgbColourON, System.Globalization.NumberStyles.HexNumber).ToString();

            var rgbx = " rgbx_" + Int32.Parse(rgbColourONX, System.Globalization.NumberStyles.HexNumber).ToString()+"_"+lValues.xValue;


            return (prime, cct, rgb, rgbx);
            */

            

            return LightValueTypeConverter.ConvertStringToLight(sClient.sendCommand("get_light_value " + id).Result);
        }

        internal void TriggerPolicy(string id, int v)
        {
            sClient.sendCommand("execute_policy_now " + id + " " + v);
        }

        internal ObservableCollection<iM_SceneModel> ListScenes()
        {
            return JsonConvert.DeserializeObject<ObservableCollection<iM_SceneModel>>(sClient.sendCommand("list_scenes_json").Result);
        }

        internal ObservableCollection<iM_TaskGroupModel> ListTaskGroups()
        {
            return JsonConvert.DeserializeObject<ObservableCollection<iM_TaskGroupModel>>(sClient.sendCommand("list_task_groups_json").Result);
        }

        internal string GetInitialTaskGroupStatus(string id)
        {
            var x = sClient.sendCommand("get_task_group_status " + id).Result; 
            return sClient.sendCommand("get_task_group_status " + id).Result;
        }

        internal void CreateLogical(iM_LogicalModel newLogical, ObservableCollection<NewLogicalItem> newLogicalItemsList)
        {
            if (String.IsNullOrEmpty(newLogical.Id))
            {
                var guid = Guid.NewGuid();

                switch (newLogical.type)
                {
                    case LogicalTypeEnum.TASK:
                        var result = sClient.sendCommand("create_new_task " + guid + " " + newLogical.Name);
                        if (result.Succesfull)
                        {
                            newLogical.Id = guid.ToString();
                        }
                        break;
                    case LogicalTypeEnum.TASK_GROUP:


                        var list = String.Join(",", newLogicalItemsList.Select(v => v.Task.Id).ToList());

                        var result1 = sClient.sendCommand("save_task_group " + guid + " " + newLogical.Name + " No_Description nothing " + "TASK:"+ list);
                        if (result1.Succesfull)
                        {
                            newLogical.Id = guid.ToString();
                        }
                        break;
                    case LogicalTypeEnum.SCENE:

                        var itemList = String.Join(",", newLogicalItemsList.Select(v => v.Light.ID).ToList());

                        
                        var result2 = sClient.sendCommand("create_scene " + newLogical.Name);// + " abs_" + (TimeSpan.FromHours(duration.Hours) + TimeSpan.FromMinutes(duration.Minutes) + TimeSpan.FromSeconds(duration.Seconds)).TotalMilliseconds + " " +  itemList);
                        if (result2.Succesfull)
                        {

                            var id = result2.Result.Substring(result2.Result.IndexOf("ID:") + 3);
                            newLogical.Id = id;
                        }
                        break;
                }  
            }

            if (!String.IsNullOrEmpty(newLogical.Id))
            {
                string ids = "";


                switch (newLogical.type)
                {
                    case LogicalTypeEnum.TASK:
                        sClient.sendCommand("cr_task_set_cycled " + newLogical.Id + " " + ((iM_TaskModel)newLogical).Cycled);

                        sClient.sendCommand("cr_task_set_pingpong " + newLogical.Id + " " + ((iM_TaskModel)newLogical).Pingpong);

                        List<string> phaseList = new List<string>();

                        for(int i = 0; i < ((iM_TaskModel)newLogical).Phases.Count; i++) 
                        {
                            phaseList.Add("["+
                                ConvertObjectToImuneString.TaskPhase(
                                    ((iM_TaskModel)newLogical).Phases[i], true) 
                                     + " F]");

                        }
                        var phases = String.Join(",", phaseList);

                        sClient.sendCommand("cr_task_set_phases " + newLogical.Id + " " + phases);


                        if (newLogicalItemsList.Count > 0)
                        {
                            var dev = newLogicalItemsList[0];
                            switch (dev.Type)
                            {
                                case LogicalItemEnum.LIGHT:
                                    ids = String.Join(",", newLogicalItemsList.Select(w => w.Light.ID).ToList());
                                    sClient.sendCommand("cr_task_set_lights " + newLogical.Id + " " + ids);
                                    break;
                                case LogicalItemEnum.TASK:
                                    ids = String.Join(",", newLogicalItemsList.Select(w => w.Task.Id).ToList());
                                    break;
                            }
                        }
                        break;
                    case LogicalTypeEnum.SCENE:
                        var duration = ((iM_SceneModel)newLogical).Duration;
                        sClient.sendCommand("set_scene_duration " + newLogical.Id + " " + (TimeSpan.FromHours(duration.Hours) + TimeSpan.FromMinutes(duration.Minutes) + TimeSpan.FromSeconds(duration.Seconds)).TotalMilliseconds);
                        sClient.sendCommand("set_scene_priority " + newLogical.Id + " " + newLogical.Priority);

                        for(int i =0; i< ((iM_SceneModel)newLogical).Phases.Count; i++)
                        {
                            var light = ((iM_SceneModel)newLogical).Phases[i];
                            sClient.sendCommand("add_scene_light_phase " + newLogical.Id + " " + light.LightID + " " + light.TransitionStyle.ToLower() + " " + ConvertObjectToImuneString.ScenePhase(light.LightValue, light.LightType));
                        }                        
                        break;
                }



                
                

                
            }

        }

        public ObservableCollection<SchedulesM> GetSchedules()
        {
            var result = sClient.sendCommand("list_schedules");
            if (result.Succesfull)
            {
                return JsonConvert.DeserializeObject<ObservableCollection<SchedulesM>>(result.Result);
            }
            return new ObservableCollection<SchedulesM>();
        }
        #endregion
    }
}
