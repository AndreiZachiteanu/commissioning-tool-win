﻿using commissioning_tool.PhysicalDevices;
using commissioning_tool.ViewModels;
using MQTTnet;
using MQTTnet.Client.Receiving;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Xamarin.Essentials;
using commissioning_tool.Crypto;
using commissioning_tool.Enums;

namespace commissioning_tool.Connections
{
    public sealed class MqttMessages : BaseViewModel, IMqttApplicationMessageReceivedHandler
    {
        public event EventHandler SensorIdentified;
        public event EventHandler CommissionModeChanged;
        public event EventHandler LogicalActStateChanged;
        public event EventHandler LightValueChanged;
        private Cryptography crypto = Cryptography.Instance;
        public List<string> Messages { get; set; } = new List<string>();
        private static MqttMessages instance;

        public static MqttMessages GetInstance()
        {
            if(instance == null)
            {
                instance = new MqttMessages();
            }
            return instance;
        }
         public Task HandleApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            
            Messages.Add(eventArgs.ApplicationMessage.ToString());
            HandleMessage(eventArgs);
            return Task.FromResult(0);
        }

        private void HandleMessage(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            string decodedMessage = Encoding.ASCII.GetString(eventArgs.ApplicationMessage.Payload);

            var topicArr = eventArgs.ApplicationMessage.Topic.Split('/');
            string topic = topicArr[topicArr.Length - 1];

            var decryptedTopic = new string[topicArr.Length];

            if (topicArr[0].EndsWith("~~"))
            {
                for (int i = 0; i < topicArr.Length; i++)
                {
                    decryptedTopic[i] += crypto.decrypt(topicArr[i]);
                }
            }
            if (eventArgs.ApplicationMessage.Topic.ToString().StartsWith("sensor/status"))
            {
                EventHandler handler = SensorIdentified;
                handler?.Invoke(this, eventArgs);
            }

            else if (eventArgs.ApplicationMessage.Topic.ToString().StartsWith("imune/light"))
            {

                
                EventHandler handler = LightValueChanged;

                var returnMes = new EventMessage();
                returnMes.Id = topicArr[2];
                if (topicArr[3].Equals("value"))
                {
                    returnMes.Message = decodedMessage;
                    handler?.Invoke(this, returnMes);
                }
            }

            else if (topic.Equals("commissioning"))
            {
                EventHandler handler = CommissionModeChanged;
                var returnMes = new EventMessage();
                returnMes.Message = decodedMessage;
                handler?.Invoke(this, returnMes);
            }
            else if (!String.IsNullOrEmpty(decryptedTopic[0])) 
            {
                if (decryptedTopic[0].Equals("task"))
                {
                    EventHandler handler = LogicalActStateChanged;
                    var returnMes = new EventMessage();
                    returnMes.LogicalType = LogicalTypeEnum.TASK;
                    returnMes.Id = decryptedTopic[decryptedTopic.Length - 1];
                    returnMes.Message = crypto.decrypt(decodedMessage);
                    handler?.Invoke(this, returnMes);
                }
                else if (decryptedTopic[0].Equals("scene"))
                {
                    EventHandler handler = LogicalActStateChanged;
                    var returnMes = new EventMessage();
                    returnMes.LogicalType = LogicalTypeEnum.SCENE;
                    returnMes.Id = decryptedTopic[decryptedTopic.Length - 1];
                    returnMes.Message = crypto.decrypt(decodedMessage);
                    handler?.Invoke(this, returnMes);
                }
                else if (decryptedTopic[0].Equals("taskgroup"))
                {
                    EventHandler handler = LogicalActStateChanged;
                    var returnMes = new EventMessage();
                    returnMes.LogicalType = LogicalTypeEnum.TASK_GROUP;
                    returnMes.Id = decryptedTopic[decryptedTopic.Length - 1];
                    returnMes.Message = crypto.decrypt(decodedMessage);
                    handler?.Invoke(this, returnMes);
                }
            }
        }
    }
}
