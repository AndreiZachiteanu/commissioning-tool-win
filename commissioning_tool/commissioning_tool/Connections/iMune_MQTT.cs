﻿using commissioning_tool.Crypto;
using commissioning_tool.PhysicalDevices;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Receiving;
using MQTTnet.Extensions.ManagedClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Xamarin.Essentials.Permissions;

namespace commissioning_tool.Connections
{
    public class iMune_MQTT 
    {
        public static MqttClientService client;
        private static iMune_MQTT instance;
        private static string ipAddress;
        private Cryptography crypto = Cryptography.Instance;
        private iMune_MQTT()
        {

        }

        public bool IsConnected { get { return client.IsConnected; } }

        public static iMune_MQTT GetInstance(string ip = "")
        {
            if(instance == null)
            {
                instance = new iMune_MQTT();
            }
            if (!String.IsNullOrEmpty(ip))
            {
                if(client == null)
                {
                    client = new MqttClientService();
                    
                }
                Connect(ip); 
            }
            return instance;
        }

        public List<IMqttApplicationMessageReceivedHandler> Messages()
        {

            // return client.
            return null;
        }

        private static async void Connect(string ip)
        {
            ipAddress = ip;
            await client.Connect(ip);
        }

        public async void Subscribe(string topic)
        {
            if (client.IsConnected)
            {
                await client.Subscribe(topic);
            }

        }
        /*
        internal void IdentifySensors(ObservableCollection<Device> sensorList)
        {
            client.IdentifySensors(sensorList);
        }
        */
        internal void SubscribeEncryptedTopic(string topic)
        {
            var topicSplit = topic.Split('/');
            var topicToSend = "";
            for(int i = 0; i< topicSplit.Length; i++)
            {
                topicSplit[i] = crypto.encrypt(topicSplit[i]);
            }
            //topicSplit[0] = "F5p6jLmO_4ynmMQYt7HI4Q~~";

            topicToSend = String.Join("/", topicSplit);

            Subscribe(topicToSend);
        }
    }

}
