﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Security.Authentication;
using System.Threading.Tasks;
using commissioning_tool.PhysicalDevices;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Receiving;
using MQTTnet.Client.Subscribing;
using MQTTnet.Client.Unsubscribing;
using MQTTnet.Diagnostics.PacketInspection;
using System.Threading;
using commissioning_tool.Models;
using Xamarin.Essentials;

namespace commissioning_tool.Connections
{
    public sealed class MqttClientService : IMqttApplicationMessageReceivedHandler, IMqttPacketInspector
    {
        readonly List<IMqttApplicationMessageReceivedHandler> _applicationMessageReceivedHandlers = new List<IMqttApplicationMessageReceivedHandler>();
        readonly List<Action<ProcessMqttPacketContext>> _messageInspectors = new List<Action<ProcessMqttPacketContext>>();
        public EventHandler ReconnectTrigger;
        IMqttClient _mqttClient;
        private MqttClientOptionsBuilder clientOptionsBuilder;

        public bool IsConnected => _mqttClient?.IsConnected == true;

        public async Task<MqttClientAuthenticateResult> Connect(string ip)
        {
           

            if (_mqttClient != null)
            {
                await _mqttClient.DisconnectAsync();
                _mqttClient.Dispose();
            }

            _mqttClient = new MqttFactory().CreateMqttClient();

            // _mqttClient.UseApplicationMessageReceivedHandler(this);
            



            clientOptionsBuilder = new MqttClientOptionsBuilder()
                .WithCommunicationTimeout(TimeSpan.FromSeconds(5))
                .WithClientId("commissioning_app")
                .WithCredentials("test2", "password2")
                .WithKeepAlivePeriod(TimeSpan.FromSeconds(10));


                clientOptionsBuilder.WithTcpServer(ip, 1883);
            _mqttClient.UseApplicationMessageReceivedHandler(MqttMessages.GetInstance());
            try
            {
                var result = await _mqttClient.ConnectAsync(clientOptionsBuilder.Build());

                _mqttClient.DisconnectedHandler = new MqttClientDisconnectedHandlerDelegate(OnDisconnected);



                return result;

            }catch(Exception ex)
            {
                App.Current.MainPage.DisplayAlert("MQTT Connection", "Unable to connect", "OK" );
            }
            return null;
            }

        private async void OnDisconnected(MqttClientDisconnectedEventArgs obj)
        {   
            Reconnect();
        }

        private async void Reconnect()
        {
            MainThread.BeginInvokeOnMainThread(() =>
            {
                iM_StaticStatus.instance.AddMessage(new StatusMessage
                {
                    message = "MQTT Disconnected. Attempting to reconnect",

                });
            });
            await Task.Delay(TimeSpan.FromSeconds(5));
            if (!_mqttClient.IsConnected)
            {
                await _mqttClient.ReconnectAsync().ContinueWith(antecedent=>Reconnect());
            }
            else
            {
                MainThread.BeginInvokeOnMainThread(() =>
                {
                    iM_StaticStatus.instance.AddMessage(new StatusMessage
                    {
                        message = "MQTT Connected.",
                        received = true
                    }) ;
                });
                EventMessage returnedTrigger = new EventMessage();
                returnedTrigger.Message = "true";
                EventHandler handler = ReconnectTrigger;
                handler?.Invoke(this, returnedTrigger);
            }
        }
        /*
        internal void IdentifySensors(ObservableCollection<Device> sensorList)
        {
            MqttMessages.GetInstance().sensorList = sensorList;
        }
        */
        private void MessageReceived(MqttApplicationMessageReceivedEventArgs e)
        {
            Debug.WriteLine(e.ApplicationMessage.Topic);


        }

        public Task Disconnect()
        {
            ThrowIfNotConnected();

            return _mqttClient.DisconnectAsync();
        }

        public async Task<MqttClientSubscribeResult> Subscribe(string topic)
        {           

            ThrowIfNotConnected();

            var topicFilter = new MqttTopicFilterBuilder()
                .WithTopic(topic)
                .Build();

            var subscribeOptions = new MqttClientSubscribeOptionsBuilder()
                .WithTopicFilter(topicFilter)
                .Build();

            var subscribeResult = await _mqttClient.SubscribeAsync(subscribeOptions).ConfigureAwait(false);

            return subscribeResult;
        }



        public async Task<MqttClientUnsubscribeResult> Unsubscribe(string topic)
        {
            if (topic == null) throw new ArgumentNullException(nameof(topic));

            ThrowIfNotConnected();

            var unsubscribeResult = await _mqttClient.UnsubscribeAsync(topic);

            return unsubscribeResult;
        }

        public void RegisterApplicationMessageReceivedHandler(IMqttApplicationMessageReceivedHandler handler)
        {
            if (handler == null) throw new ArgumentNullException(nameof(handler));

            _applicationMessageReceivedHandlers.Add(handler);
        }

        public void RegisterMessageInspectorHandler(Action<ProcessMqttPacketContext> handler)
        {
            if (handler == null) throw new ArgumentNullException(nameof(handler));

            _messageInspectors.Add(handler);
        }

        public async Task HandleApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            if (eventArgs == null) throw new ArgumentNullException(nameof(eventArgs));

            foreach (var handler in _applicationMessageReceivedHandlers)
            {
                await handler.HandleApplicationMessageReceivedAsync(eventArgs);
            }
        }

        public void ProcessMqttPacket(ProcessMqttPacketContext context)
        {
            foreach (var messageInspector in _messageInspectors)
            {
                messageInspector.Invoke(context);
            }
        }

        void ThrowIfNotConnected()
        {
            if (_mqttClient == null || !_mqttClient.IsConnected)
            {
                throw new InvalidOperationException("The MQTT client is not connected.");
            }
        }

    }
}
