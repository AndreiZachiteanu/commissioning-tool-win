﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Renci.SshNet;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using commissioning_tool.Models;

namespace commissioning_tool.Connections
{
    public class Ssh
    {
        #region Fields
        private static Ssh instance;
        SshClient client;

        #endregion
        public string exception;
        private ConnectionInfo connectionInfo;
        private string ip;

        public string GetIP()
        {

            return ip;
        }
        public bool IsConnected()
        {
            
            if (client == null) 
            {
                iM_StaticStatus.instance.AddMessage(new StatusMessage {
                    //count = iM_StaticStatus.instance.StatusMessageList.Count,
                    message = "Client is null | ", 
                    received = false, 
                     });
                //iM_StaticStatus.instance.LastStatusMessage = "Client is null";
                iM_StaticStatus.instance.numberOfErrors += 1;
                return false;
            }
            return client.IsConnected;            
        }

        public void Reconnect()
        {
            iM_StaticStatus.instance.AddMessage(new StatusMessage {
                //count = iM_StaticStatus.instance.StatusMessageList.Count,
                message = "Client is reconnecting to iMune | " , 
                received = true,  });
            //iM_StaticStatus.instance.LastStatusMessage = "Client is reconnecting to iMune";
            client.Connect();

            if (client.IsConnected)
            {
                iM_StaticStatus.instance.AddMessage(new StatusMessage {
                    //count = iM_StaticStatus.instance.StatusMessageList.Count,
                    message = "Client is connected | ", 
                    received = true, 
                    });
                //iM_StaticStatus.instance.LastStatusMessage = "Client is connected";
                iM_StaticStatus.instance.numberOfErrors += 1;
            }
            else
            {
                iM_StaticStatus.instance.AddMessage(new StatusMessage {
                    //count = iM_StaticStatus.instance.StatusMessageList.Count,
                    message = "Client hasn't connected | ", 
                    received = false, 
                    });
                //iM_StaticStatus.instance.LastStatusMessage = "Client hasn't connected";
                iM_StaticStatus.instance.numberOfErrors += 1;
            }
        }

        #region Constructor

        private Ssh()
        {
            
        }
        #endregion

        public static Ssh GetInstance()
        {
            if (instance == null)
            {
                instance = new Ssh();
            }

            return instance;
        }

        /// <summary>
        /// Method to make server connection using username,Ip addr and server key
        /// </summary>
        /// <param name="ip"></param>server Ip addr
        /// <param name="username"></param> 
        /// <param name="keyToConnect"></param>private key 
        public void Connect(string ip, string username, string keyToConnect)
        {

            //iMune_MQTT.GetInstance().Connect(ip, 22, username, "imune");
            //await iMune_MQTT.GetInstance().StartAsync(new System.Threading.CancellationToken());

            String key = keyToConnect;
            this.ip = ip;
            key.Trim();
            PrivateKeyFile pkf = new PrivateKeyFile(stringToStream(key));
            AuthenticationMethod authenticationMethod = new PrivateKeyAuthenticationMethod(username, pkf);

            connectionInfo = new ConnectionInfo(ip, 22, username, authenticationMethod);
            
            connectionInfo.Timeout = TimeSpan.FromSeconds(15);
            connectionInfo.ChannelCloseTimeout = TimeSpan.FromMinutes(15);
            //connectionInfo.
            client = new SshClient(connectionInfo);
            var statusMess = iM_StaticStatus.instance;

            statusMess.AddMessage(new StatusMessage
            {
               // count = iM_StaticStatus.instance.StatusMessageList.Count,
                message = "Client is trying to connect to iMune | ",
                received = true,

            });
            //client.KeepAliveInterval = TimeSpan.FromMinutes(3);
            try
            {
                client.Connect();
            }
            catch(Renci.SshNet.Common.SshAuthenticationException exception)
            {
                statusMess.AddMessage(new StatusMessage
                {
                    //count = iM_StaticStatus.instance.StatusMessageList.Count,
                    message = "Key used is not correct | ",
                    received = false,

                });
            }

            //if (statusMess.StatusMessageList.Count > 0)
          //  {

           //     statusMess.LastStatusMessage = statusMess.StatusMessageList[0].message;
           // }
        }

        public void Disconnect()
        {
            iM_StaticStatus.instance.AddMessage(new StatusMessage {
                //count = iM_StaticStatus.instance.StatusMessageList.Count,
                message = "Disconnecting from iMune | ", 
                received = true, 
                 });
            //iM_StaticStatus.instance.LastStatusMessage = "Disconnecting from iMune";
            client.Disconnect();
        }

        /// <summary>
        /// Method to handle user command
        /// </summary>
        /// <param name="Command"></param>user command
        /// <returns></returns>Returns  strings after successful server connection 
        public (string Result, bool Succesfull) sendCommand(string Command)
        {
            string ret = Command;


            bool success = false;

            string resultMessage = "";
            try
            {
                using (var command = client.RunCommand("imsg " + Command))
                {
                    // var command = client.RunCommand();
                    //Debug.WriteLine(command.Execute());
                    //command.CommandTimeout = TimeSpan.FromSeconds(3);

                    var result = command.Result;
                    if (!result.StartsWith("FAIL"))
                    {
                        success = true;

                        ret = result;

                        if (result.StartsWith("OK -"))
                        {
                            ret = result.Substring("OK -".Length).Trim();//.Length);


                        }
                        else if(result.StartsWith("OK"))
                        {
                            ret = result.Substring("OK".Length).Trim();
                        }
                        else
                        {
                            ret = result.Trim();
                        }
                    }


                    if (!success)
                    {

                        resultMessage =". iMune: "+ result;
                        string s = "";
                    }

                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Command exception - Renci " + ex);
            }
            // client.Disconnect();
            //}

            //if (client.IsConnected) {
            //   Debug.WriteLine("is connected  "  + client.IsConnected);

            //var x = client.RunCommand("imsg " + Command);
            // Debug.WriteLine(x);

            // }
            //}
            //catch (Exception ex)
            //{
            //   Debug.WriteLine(ex);
            //  ret = "-1-2-3-4-5-6-7-8";
            //}

            var statusMess = iM_StaticStatus.instance;
            if (!success) statusMess.numberOfErrors++;
            statusMess.AddMessage(new StatusMessage {
                //count = iM_StaticStatus.instance.StatusMessageList.Count,
                message = "Sent command : " + Command  + resultMessage + ". Client Connected: " + client.IsConnected + " | ",
                received = success,
                
            });
            //statusMess.LastStatusMessage = statusMess.Las statusMess.StatusMessageList[statusMess.StatusMessageList.Count - 1].message;
            

            return (ret, success);
        }
              
        /// <summary>
        /// Method to stream server key
        /// </summary>
        /// <param name="toConvert"></param>
        /// <returns></returns>
        private Stream stringToStream(string toConvert)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            writer.Write(toConvert);
            writer.Flush();

            stream.Position = 0;
            return stream;
        }

    }
}
